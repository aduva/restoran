INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Повар', 'Аспазшы', 'Cook');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Техничка', 'Техник', 'Cleaning');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Посудница', 'Ыдыс жуу', 'Dish washer');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Посудница', 'Ыдыс жуу', 'Dish washer');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Администратор', 'Администратор', 'Manager');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Водитель', 'Шопыр бала', 'Driver');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Бухгалтер', 'Бухгалтер', 'Accountant');

INSERT INTO res_employee_type(
            id_, creator_, editor_, date_created_, date_edited_, disabled_, 
            tags_, group_, ru, kk, en)
    VALUES (1, 1, null, 'now', null, FALSE, 
            '', 1, 'Фотограф', 'Фотограф', 'Photographer');

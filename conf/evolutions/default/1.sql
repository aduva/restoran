# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table act_activity (
  id_                       bigint not null,
  actor                     bigint,
  subject                   bigint,
  target                    bigint,
  verb_                     varchar(255),
  composite_verb_           varchar(255),
  date_                     timestamp,
  constraint pk_act_activity primary key (id_))
;

create table act_object (
  id_                       bigint not null,
  display_name_             varchar(255),
  object_type_              varchar(255),
  object_id_                bigint,
  constraint pk_act_object primary key (id_))
;

create table res_banket (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  title_                    varchar(255),
  banket_type_              bigint,
  date_                     timestamp,
  people_amount_            integer,
  location_                 bigint,
  group_                    bigint,
  constraint pk_res_banket primary key (id_))
;

create table res_banket_invoice (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  banket_                   bigint,
  invoice_                  bigint,
  type_                     varchar(9),
  constraint ck_res_banket_invoice_type_ check (type_ in ('FOOD','SERVICE','EQUIPMENT')),
  constraint pk_res_banket_invoice primary key (id_))
;

create table res_banket_profile (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  banket_                   bigint,
  profile_                  bigint,
  constraint pk_res_banket_profile primary key (id_))
;

create table res_banket_supply (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  banket_                   bigint,
  supply_                   bigint,
  constraint pk_res_banket_supply primary key (id_))
;

create table res_banket_type (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  type_                     varchar(7),
  group_                    bigint,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint ck_res_banket_type_type_ check (type_ in ('WEDDING','DEFAULT')),
  constraint pk_res_banket_type primary key (id_))
;

create table core_modules (
  id_                       bigint not null,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  url_                      varchar(255),
  access_level_             integer,
  name_                     varchar(255),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint uq_core_modules_url_ unique (url_),
  constraint uq_core_modules_name_ unique (name_),
  constraint pk_core_modules primary key (id_))
;

create table res_contact (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  profile_                  bigint,
  constraint pk_res_contact primary key (id_))
;

create table res_contact_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  contact_                  bigint,
  contact_type_             varchar(9),
  value_                    varchar(255),
  main_                     boolean,
  constraint ck_res_contact_item_contact_type_ check (contact_type_ in ('MOBILE','ADDRESS','UNKNOWN','IIN','EMAIL','TELEPHONE')),
  constraint pk_res_contact_item primary key (id_))
;

create table res_employee (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  employee_type_            bigint,
  profile_                  bigint,
  salary_                   integer,
  constraint pk_res_employee primary key (id_))
;

create table res_employee_schedule (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  employee_                 bigint,
  date_                     timestamp,
  constraint pk_res_employee_schedule primary key (id_))
;

create table res_employee_type (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_res_employee_type primary key (id_))
;

create table res_food (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  food_type_                bigint,
  cost_                     float,
  price_                    float,
  weight_                   float,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_res_food primary key (id_))
;

create table res_food_ingredient (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  food_                     bigint,
  ingredient_               bigint,
  amount_                   float,
  constraint pk_res_food_ingredient primary key (id_))
;

create table res_order_food (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  order_                    bigint,
  food_                     bigint,
  food_type_                bigint,
  portions_per_place_       float,
  overall_                  float,
  parent_food_              bigint,
  price_                    float,
  to_supply_                boolean,
  invoice_item_             bigint,
  constraint pk_res_order_food primary key (id_))
;

create table res_food_order_table_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  order_                    bigint,
  order_table_item_         bigint,
  food_order_               bigint,
  places_                   integer,
  portions_                 float,
  overall_                  float,
  constraint pk_res_food_order_table_item primary key (id_))
;

create table res_food_type (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_res_food_type primary key (id_))
;

create table sec_groups (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  name_                     varchar(255),
  parent_                   bigint,
  version                   integer not null,
  constraint uq_sec_groups_name_ unique (name_),
  constraint pk_sec_groups primary key (id_))
;

create table res_invoice (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  title_                    varchar(255),
  constraint pk_res_invoice primary key (id_))
;

create table res_invoice_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  title_                    varchar(255),
  invoice_                  bigint,
  amount_                   integer,
  closed_                   boolean,
  invoice_type_             varchar(7),
  constraint ck_res_invoice_item_invoice_type_ check (invoice_type_ in ('BILL','EXPENSE','INCOME')),
  constraint pk_res_invoice_item primary key (id_))
;

create table res_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  transform_ratio_          float,
  transform_measure_type_   varchar(7),
  measure_type_             varchar(7),
  item_type_                varchar(10),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint ck_res_item_transform_measure_type_ check (transform_measure_type_ in ('LITER','KILO','UNKNOWN','PIECES','GRAMM')),
  constraint ck_res_item_measure_type_ check (measure_type_ in ('LITER','KILO','UNKNOWN','PIECES','GRAMM')),
  constraint ck_res_item_item_type_ check (item_type_ in ('INGREDIENT','UNKNOWN','HOUSEHOLD')),
  constraint pk_res_item primary key (id_))
;

create table res_location (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  people_amount_            integer,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_res_location primary key (id_))
;

create table res_location_table_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  location_                 bigint,
  table_item_               bigint,
  quantity_                 integer,
  joined_                   boolean,
  priority_                 integer,
  constraint pk_res_location_table_item primary key (id_))
;

create table sec_memberships (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  user_                     bigint,
  group_                    bigint,
  role_level_               integer,
  version                   integer not null,
  constraint pk_sec_memberships primary key (id_))
;

create table core_object_type (
  name_                     varchar(255) not null,
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_core_object_type primary key (name_))
;

create table res_order (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  banket_                   bigint,
  template_                 bigint,
  people_amount_            integer,
  unplaced_people_amount_   integer,
  overall_payment_          float,
  supply_date_              timestamp,
  finalized_                boolean,
  supply_approved_          boolean,
  supply_                   bigint,
  constraint pk_res_order primary key (id_))
;

create table res_order_table_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  order_                    bigint,
  location_table_item_      bigint,
  table_item_               bigint,
  quantity_                 integer,
  people_amount_            integer,
  constraint pk_res_order_table_item primary key (id_))
;

create table res_order_template (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint pk_res_order_template primary key (id_))
;

create table res_order_template_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  order_template_           bigint,
  food_type_                bigint,
  food_amount_              integer,
  max_portions_             integer,
  min_portions_             float,
  constraint pk_res_order_template_item primary key (id_))
;

create table res_order_template_place (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  order_template_           bigint,
  food_type_                bigint,
  location_table_item_      bigint,
  places_                   integer,
  constraint pk_res_order_template_place primary key (id_))
;

create table res_profile (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  user_                     bigint,
  firstname_                varchar(255),
  lastname_                 varchar(255),
  constraint pk_res_profile primary key (id_))
;

create table sec_permissions (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  value_                    varchar(255),
  version                   integer not null,
  constraint uq_sec_permissions_value_ unique (value_),
  constraint pk_sec_permissions primary key (id_))
;

create table sec_roles (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  level_                    integer,
  name_                     varchar(255),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  version                   integer not null,
  constraint uq_sec_roles_name_ unique (name_),
  constraint pk_sec_roles primary key (id_))
;

create table res_stock_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  item_                     bigint,
  amount_                   float,
  locked_                   float,
  constraint pk_res_stock_item primary key (id_))
;

create table res_stock_item_takeout (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  item_                     bigint,
  amount_                   float,
  constraint pk_res_stock_item_takeout primary key (id_))
;

create table res_supply (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  date_                     timestamp,
  title_                    varchar(255),
  constraint pk_res_supply primary key (id_))
;

create table res_supply_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  item_                     bigint,
  supply_                   bigint,
  amount_                   float,
  locked_                   float,
  date_                     timestamp,
  constraint pk_res_supply_item primary key (id_))
;

create table res_supply_item_result (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  item_                     bigint,
  date_                     timestamp,
  final_amount_             float,
  single_price_             integer,
  constraint pk_res_supply_item_result primary key (id_))
;

create table res_table_item (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  group_                    bigint,
  people_amount_            integer,
  type_                     varchar(7),
  ru                        varchar(255),
  kk                        varchar(255),
  en                        varchar(255),
  constraint ck_res_table_item_type_ check (type_ in ('WEDDING','DEFAULT')),
  constraint pk_res_table_item primary key (id_))
;

create table sec_users (
  id_                       bigint not null,
  creator_                  bigint,
  editor_                   bigint,
  date_created_             timestamp,
  date_edited_              timestamp,
  disabled_                 boolean,
  tags_                     varchar(255),
  firstname_                varchar(255),
  lastname_                 varchar(255),
  email_                    varchar(255),
  password_                 varchar(255),
  email_valid_              boolean,
  role_level_               integer,
  version                   integer not null,
  constraint uq_sec_users_email_ unique (email_),
  constraint pk_sec_users primary key (id_))
;

create sequence act_activity_seq;

create sequence act_object_seq;

create sequence res_banket_seq;

create sequence res_banket_invoice_seq;

create sequence res_banket_profile_seq;

create sequence res_banket_supply_seq;

create sequence res_banket_type_seq;

create sequence core_modules_seq;

create sequence res_contact_seq;

create sequence res_contact_item_seq;

create sequence res_employee_seq;

create sequence res_employee_schedule_seq;

create sequence res_employee_type_seq;

create sequence res_food_seq;

create sequence res_food_ingredient_seq;

create sequence res_order_food_seq;

create sequence res_food_order_table_item_seq;

create sequence res_food_type_seq;

create sequence sec_groups_seq;

create sequence res_invoice_seq;

create sequence res_invoice_item_seq;

create sequence res_item_seq;

create sequence res_location_seq;

create sequence res_location_table_item_seq;

create sequence sec_memberships_seq;

create sequence core_object_type_seq;

create sequence res_order_seq;

create sequence res_order_table_item_seq;

create sequence res_order_template_seq;

create sequence res_order_template_item_seq;

create sequence res_order_template_place_seq;

create sequence res_profile_seq;

create sequence sec_permissions_seq;

create sequence sec_roles_seq;

create sequence res_stock_item_seq;

create sequence res_stock_item_takeout_seq;

create sequence res_supply_seq;

create sequence res_supply_item_seq;

create sequence res_supply_item_result_seq;

create sequence res_table_item_seq;

create sequence sec_users_seq;




# --- !Downs

drop table if exists act_activity cascade;

drop table if exists act_object cascade;

drop table if exists res_banket cascade;

drop table if exists res_banket_invoice cascade;

drop table if exists res_banket_profile cascade;

drop table if exists res_banket_supply cascade;

drop table if exists res_banket_type cascade;

drop table if exists core_modules cascade;

drop table if exists res_contact cascade;

drop table if exists res_contact_item cascade;

drop table if exists res_employee cascade;

drop table if exists res_employee_schedule cascade;

drop table if exists res_employee_type cascade;

drop table if exists res_food cascade;

drop table if exists res_food_ingredient cascade;

drop table if exists res_order_food cascade;

drop table if exists res_food_order_table_item cascade;

drop table if exists res_food_type cascade;

drop table if exists sec_groups cascade;

drop table if exists res_invoice cascade;

drop table if exists res_invoice_item cascade;

drop table if exists res_item cascade;

drop table if exists res_location cascade;

drop table if exists res_location_table_item cascade;

drop table if exists sec_memberships cascade;

drop table if exists core_object_type cascade;

drop table if exists res_order cascade;

drop table if exists res_order_table_item cascade;

drop table if exists res_order_template cascade;

drop table if exists res_order_template_item cascade;

drop table if exists res_order_template_place cascade;

drop table if exists res_profile cascade;

drop table if exists sec_permissions cascade;

drop table if exists sec_roles cascade;

drop table if exists res_stock_item cascade;

drop table if exists res_stock_item_takeout cascade;

drop table if exists res_supply cascade;

drop table if exists res_supply_item cascade;

drop table if exists res_supply_item_result cascade;

drop table if exists res_table_item cascade;

drop table if exists sec_users cascade;

drop sequence if exists act_activity_seq;

drop sequence if exists act_object_seq;

drop sequence if exists res_banket_seq;

drop sequence if exists res_banket_invoice_seq;

drop sequence if exists res_banket_profile_seq;

drop sequence if exists res_banket_supply_seq;

drop sequence if exists res_banket_type_seq;

drop sequence if exists core_modules_seq;

drop sequence if exists res_contact_seq;

drop sequence if exists res_contact_item_seq;

drop sequence if exists res_employee_seq;

drop sequence if exists res_employee_schedule_seq;

drop sequence if exists res_employee_type_seq;

drop sequence if exists res_food_seq;

drop sequence if exists res_food_ingredient_seq;

drop sequence if exists res_order_food_seq;

drop sequence if exists res_food_order_table_item_seq;

drop sequence if exists res_food_type_seq;

drop sequence if exists sec_groups_seq;

drop sequence if exists res_invoice_seq;

drop sequence if exists res_invoice_item_seq;

drop sequence if exists res_item_seq;

drop sequence if exists res_location_seq;

drop sequence if exists res_location_table_item_seq;

drop sequence if exists sec_memberships_seq;

drop sequence if exists core_object_type_seq;

drop sequence if exists res_order_seq;

drop sequence if exists res_order_table_item_seq;

drop sequence if exists res_order_template_seq;

drop sequence if exists res_order_template_item_seq;

drop sequence if exists res_order_template_place_seq;

drop sequence if exists res_profile_seq;

drop sequence if exists sec_permissions_seq;

drop sequence if exists sec_roles_seq;

drop sequence if exists res_stock_item_seq;

drop sequence if exists res_stock_item_takeout_seq;

drop sequence if exists res_supply_seq;

drop sequence if exists res_supply_item_seq;

drop sequence if exists res_supply_item_result_seq;

drop sequence if exists res_table_item_seq;

drop sequence if exists sec_users_seq;


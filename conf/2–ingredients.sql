INSERT INTO res_ingredient VALUES (setval('res_ingredient_seq',1), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Говядина мякоть', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Помидоры', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Огурцы', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Перец болгарский', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Морковь', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Чеснок', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, масло', 1, 'GRAMM', 'Масло кунжутное', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, масло', 1, 'GRAMM', 'Масло оливковое', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, масло', 1, 'GRAMM', 'Масло подсолнечное', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень', 1, 'GRAMM', 'Базилик', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, соус', 1, 'GRAMM', 'Соус соевый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба', 1, 'GRAMM', 'Семга (ласось) слабосоленая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'яйца', 1, 'GRAMM', 'Яйца куриные', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Картофель', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'украшения, зелень, консервы', 1, 'GRAMM', 'Маслины без косточек', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, соус', 1, 'GRAMM', 'Майонез', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень', 1, 'GRAMM', 'Фасоль стручковая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Филе куриное (грудки)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Лук', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные', 1, 'GRAMM', 'Молоко 2,5%', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'орехи, украшения', 1, 'GRAMM', 'Орех Грецкий', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, колбасные', 1, 'GRAMM', 'Ветчина говяжья', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Язык говяжьий', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, консервы', 1, 'GRAMM', 'Кукуруза', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень, украшения', 1, 'GRAMM', 'Салатный лист', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Баклажаны', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'семена, специи', 1, 'GRAMM', 'Кунжутные семечки', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, овощи', 1, 'GRAMM', 'Перец Чили (красный, острый) свежий', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, соус, уксус', 1, 'GRAMM', 'Уксус 9%', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Печень говяжья', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, грибы', 1, 'GRAMM', 'Шампиньоны свежие', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные, хлеб', 1, 'GRAMM', 'Батон нарезной', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куриная грудка копченая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Пармезан', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, соус', 1, 'GRAMM', 'Горчица', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'фрукты, цитрусовые, украшения', 1, 'GRAMM', 'Лимон', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'заправка, соус, уксус', 1, 'GRAMM', 'Уксус бальзамический белый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, специи, консервы', 1, 'GRAMM', 'Каперсы маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Фета', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Орегано (майоран)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Чечел', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, сухофрукты', 1, 'GRAMM', 'Чернослив без косточек', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'малочные', 1, 'GRAMM', 'Сметана 40%', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'консервы', 1, 'GRAMM', 'Фасоль красная в томате', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Черный перец', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, сахар', 1, 'GRAMM', 'Сахар песок', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень, украшения, специи', 1, 'GRAMM', 'Петрушка', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень, украшения, специи', 1, 'GRAMM', 'Укроп', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'соус, заправка', 1, 'GRAMM', 'Кетчуп томатный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'консервы', 1, 'GRAMM', 'Горошек зеленый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, консервы', 1, 'GRAMM', 'Огурцы соленые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Соль', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'консервы, грибы', 1, 'GRAMM', 'Опята', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'уксус, заправка', 1, 'GRAMM', 'Винный уксус', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Гауда', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные', 1, 'GRAMM', 'Фунчоза', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, рыба', 1, 'GRAMM', 'Судак филе', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'уксус', 1, 'GRAMM', 'Уксусная эссенция 70%', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Кориандр', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'грибы, консервы', 1, 'GRAMM', 'Шампиньоны маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Паприка', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы', 1, 'GRAMM', 'Кальмары маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба', 1, 'GRAMM', 'Креветки очищенные', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные', 1, 'GRAMM', 'Сметана домашняя', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы', 1, 'GRAMM', 'Икра осетровая зернистая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Филадельфия', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'фрукты', 1, 'GRAMM', 'Ананас', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, консервы', 1, 'GRAMM', 'Корнишоны маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Лавровый лист', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы', 1, 'GRAMM', 'Печень трески', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы, икра', 1, 'GRAMM', 'Икра искусственная', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, конина', 1, 'GRAMM', 'Жая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, конина', 1, 'GRAMM', 'Казы', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, соленые', 1, 'GRAMM', 'Белорыбица', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные, украшения', 1, 'GRAMM', 'Валованы', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куриная окорочка', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куриные крылышки', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Желатин', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, мучные', 1, 'GRAMM', 'Сухари панировачные', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные', 1, 'GRAMM', 'Мука', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, соус', 1, 'GRAMM', 'Жидкий дым', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, масло', 1, 'GRAMM', 'Масло сливочное несоленое', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Специи для баранины', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Специи для курицы', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'маринады, овощи', 1, 'GRAMM', 'Капуста квашеная', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, консервы', 1, 'GRAMM', 'Помидоры соленые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'фрукты, цитрусовые', 1, 'GRAMM', 'Апельсин', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень, специи', 1, 'GRAMM', 'Мята перечная', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'зелень, овощи, украшения', 1, 'GRAMM', 'Лук зеленый (перо)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куриный рулет копченый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы, мясо', 1, 'GRAMM', 'Селдь среднесоленая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Бастурма в специях', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Буженина (баранина)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, колбасные', 1, 'GRAMM', 'Колбаса сырокопченая (сервелат)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные', 1, 'GRAMM', 'Блины', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'рыба, консервы', 1, 'GRAMM', 'Тунец консервированный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Сыр плавленный колбасный копченый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные, сыр', 1, 'GRAMM', 'Сыр мраморный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, колбасные', 1, 'GRAMM', 'Салями', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Карбонат копченый (корейка сырокопченая)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, консервы', 1, 'GRAMM', 'Черри маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные', 1, 'GRAMM', 'Творог полужирный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'орехи', 1, 'GRAMM', 'Цукаты', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'орехи', 1, 'GRAMM', 'Миндаль ядро', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мучные', 1, 'GRAMM', 'Тесто слоеное', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Шафран', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, добавки', 1, 'GRAMM', 'Сода пищевая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, рыба', 1, 'GRAMM', 'Семга (ласось) свежая', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Специи для рыбы', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'молочные', 1, 'GRAMM', 'Сливки 35%', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'алкоголь', 1, 'GRAMM', 'Вино белое сухое', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи, специи', 1, 'GRAMM', 'Имбирь свежий (корень)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Чили молотый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи, консервы', 1, 'GRAMM', 'Томатная паста', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'соки', 1, 'GRAMM', 'Сок гранатовый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, птица', 1, 'GRAMM', 'Утка (тушка 1кат)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, баранина', 1, 'GRAMM', 'Баранина задняя нога на кости', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Инжир сушеный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Зира', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, птица', 1, 'GRAMM', 'Гусь целый (обработанная тушка 1 категории)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'фрукты', 1, 'GRAMM', 'Яблоки', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'алкоголь', 1, 'GRAMM', 'Коньяк (бренди)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'уксус', 1, 'GRAMM', 'Уксус соевый', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куриная голень', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, курица', 1, 'GRAMM', 'Куры целые (тушки 1 категории)', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, рыба', 1, 'GRAMM', 'Семга (ласось, форель) тушка потрашенная с головой', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Овощная смесь мексиканская', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо, рыба', 1, 'GRAMM', 'Карп, сазан', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'орехи', 1, 'GRAMM', 'Кешью ядро', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Корица', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Мед натуральный', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'мясо', 1, 'GRAMM', 'Баранина коробка', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Капуста брокколи', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Капуста бельгийская', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'овощи', 1, 'GRAMM', 'Капуста цветная', NULL, NULL);
INSERT INTO res_ingredient VALUES (nextval('res_ingredient_seq'), 1, NULL, 'now', NULL, false, 'специи', 1, 'GRAMM', 'Красный перец молотый', NULL, NULL);

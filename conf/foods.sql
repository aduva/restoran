--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.5
-- Dumped by pg_dump version 9.1.4
-- Started on 2014-03-31 12:49:26 AQTT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2293 (class 0 OID 252601)
-- Dependencies: 168
-- Data for Name: res_food; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO res_food VALUES (4, NULL, NULL, '2014-03-28 14:31:56.627', NULL, false, NULL, NULL, 2, 0, 1000, 200, 'Анкара', NULL, NULL);
INSERT INTO res_food VALUES (6, NULL, NULL, '2014-03-28 14:31:56.627', NULL, false, NULL, NULL, 2, 0, 1000, 200, 'Трофей охотника', NULL, NULL);
INSERT INTO res_food VALUES (30, NULL, NULL, '2014-03-28 14:31:56.631', NULL, false, NULL, NULL, 1, 0, 1000, 200, 'Секрет бабушки', NULL, NULL);
INSERT INTO res_food VALUES (35, NULL, NULL, '2014-03-28 14:31:56.632', NULL, false, NULL, NULL, 1, 0, 1000, 200, 'Казан кебаб', NULL, NULL);
INSERT INTO res_food VALUES (37, NULL, NULL, '2014-03-28 14:31:56.632', NULL, false, NULL, NULL, 4, 0, 1000, 200, 'Картофель пикантный', NULL, NULL);
INSERT INTO res_food VALUES (38, NULL, NULL, '2014-03-28 14:31:56.632', NULL, false, NULL, NULL, 4, 0, 1000, 200, 'Овощной гарнир', NULL, NULL);
INSERT INTO res_food VALUES (39, NULL, NULL, '2014-03-28 14:31:56.632', NULL, false, NULL, NULL, 4, 0, 1000, 200, 'Картофель Фри', NULL, NULL);
INSERT INTO res_food VALUES (40, NULL, NULL, '2014-03-28 14:31:56.632', NULL, false, NULL, NULL, 5, 0, 1000, 200, 'Фруктовая корзина', NULL, NULL);
INSERT INTO res_food VALUES (41, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 5, 0, 1000, 200, 'Фруктовая нарезка', NULL, NULL);
INSERT INTO res_food VALUES (42, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 7, 0, 1000, 200, 'Хлебная корзина', NULL, NULL);
INSERT INTO res_food VALUES (43, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 7, 0, 1000, 200, 'Бауырсаки', NULL, NULL);
INSERT INTO res_food VALUES (44, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 8, 0, 1000, 200, 'Чайный стол', NULL, NULL);
INSERT INTO res_food VALUES (45, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 9, 0, 1000, 200, 'Канапе', NULL, NULL);
INSERT INTO res_food VALUES (46, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 9, 0, 1000, 200, 'Бельгийский Шоколадный фонтан', NULL, NULL);
INSERT INTO res_food VALUES (47, NULL, NULL, '2014-03-28 14:31:56.633', NULL, false, NULL, NULL, 9, 0, 1000, 200, 'Турецкий Шоколадный фонтан', NULL, NULL);
INSERT INTO res_food VALUES (48, NULL, NULL, '2014-03-28 14:31:56.634', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Директор', NULL, NULL);
INSERT INTO res_food VALUES (49, NULL, 4, '2014-03-28 14:31:56.634', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Балтика', NULL, NULL);
INSERT INTO res_food VALUES (50, NULL, 4, '2014-03-28 14:31:56.634', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Наурыз', NULL, NULL);
INSERT INTO res_food VALUES (51, NULL, 4, '2014-03-28 14:31:56.635', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Шопский', NULL, NULL);
INSERT INTO res_food VALUES (52, NULL, 4, '2014-03-28 14:31:56.635', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Самурай', NULL, NULL);
INSERT INTO res_food VALUES (53, NULL, 4, '2014-03-28 14:31:56.636', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Бруклин', NULL, NULL);
INSERT INTO res_food VALUES (54, NULL, 4, '2014-03-28 14:31:56.636', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Цезарь', NULL, NULL);
INSERT INTO res_food VALUES (55, NULL, NULL, '2014-03-28 14:31:56.636', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Греческий', NULL, NULL);
INSERT INTO res_food VALUES (56, NULL, NULL, '2014-03-28 14:31:56.637', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Салем', NULL, NULL);
INSERT INTO res_food VALUES (57, NULL, 4, '2014-03-28 14:31:56.637', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Коктейль из курицы', NULL, NULL);
INSERT INTO res_food VALUES (58, NULL, 4, '2014-03-28 14:31:56.637', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Восторг', NULL, NULL);
INSERT INTO res_food VALUES (59, NULL, NULL, '2014-03-28 14:31:56.638', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Оливье классическое', NULL, NULL);
INSERT INTO res_food VALUES (60, NULL, 4, '2014-03-28 14:31:56.638', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Купеческий', NULL, NULL);
INSERT INTO res_food VALUES (61, NULL, 4, '2014-03-28 14:31:56.638', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Боярский', NULL, NULL);
INSERT INTO res_food VALUES (62, NULL, 4, '2014-03-28 14:31:56.639', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Дон Кихот', NULL, NULL);
INSERT INTO res_food VALUES (63, NULL, 4, '2014-03-28 14:31:56.639', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Азия', NULL, NULL);
INSERT INTO res_food VALUES (64, NULL, 4, '2014-03-28 14:31:56.639', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Ассорти Ретро', NULL, NULL);
INSERT INTO res_food VALUES (65, NULL, NULL, '2014-03-28 14:31:56.64', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Кардинал', NULL, NULL);
INSERT INTO res_food VALUES (3, NULL, 4, '2014-03-28 14:31:56.627', '2014-03-28 16:07:22.854', false, NULL, NULL, 2, 0, 1000, 400, 'Рыбное Ассорти', NULL, NULL);
INSERT INTO res_food VALUES (2, NULL, 4, '2014-03-28 14:31:56.627', '2014-03-28 16:07:02.374', false, NULL, NULL, 2, 0, 3500, 400, 'Казахстан', NULL, NULL);
INSERT INTO res_food VALUES (1, NULL, 4, '2014-03-28 14:31:56.623', '2014-03-28 16:06:42.671', false, NULL, NULL, 2, 0, 2000, 400, 'Баклажаны по-египетский', NULL, NULL);
INSERT INTO res_food VALUES (15, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:32:15.036', false, NULL, NULL, 2, 0, 1800, 400, 'Поцелуй удачи', NULL, NULL);
INSERT INTO res_food VALUES (10, NULL, 4, '2014-03-28 14:31:56.628', '2014-03-28 16:43:33.916', false, NULL, NULL, 2, 0, 2000, 400, 'Анталия', NULL, NULL);
INSERT INTO res_food VALUES (11, NULL, 4, '2014-03-28 14:31:56.628', '2014-03-28 17:01:54.672', false, NULL, NULL, 2, 0, 1800, 400, 'Закуска На Троих', NULL, NULL);
INSERT INTO res_food VALUES (9, NULL, 4, '2014-03-28 14:31:56.628', '2014-03-30 11:05:22.8', false, NULL, NULL, 2, 0, 1200, 500, 'Кавказ', NULL, NULL);
INSERT INTO res_food VALUES (13, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:21:44.229', false, NULL, NULL, 2, 0, 2400, 352, 'Афродита', NULL, NULL);
INSERT INTO res_food VALUES (14, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:24:35.039', false, NULL, NULL, 2, 0, 2000, 500, 'Гастрономия', NULL, NULL);
INSERT INTO res_food VALUES (29, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 16:05:54.475', false, NULL, NULL, 1, 0, 2200, 200, 'Ужин аристократа', NULL, NULL);
INSERT INTO res_food VALUES (16, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:35:07.894', false, NULL, NULL, 2, 0, 1500, 400, 'Секрет', NULL, NULL);
INSERT INTO res_food VALUES (17, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:39:51.04', false, NULL, NULL, 2, 0, 1800, 400, 'Банкетное Ассорти', NULL, NULL);
INSERT INTO res_food VALUES (21, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 13:47:58.04', false, NULL, NULL, 1, 0, 1000, 250, 'Бухара', NULL, NULL);
INSERT INTO res_food VALUES (31, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 16:27:48.11', false, NULL, NULL, 1, 0, 1000, 1000, 'Рыба с секретом', NULL, NULL);
INSERT INTO res_food VALUES (28, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 14:07:43.292', false, NULL, NULL, 1, 0, 3500, 800, 'Курочка с пьяными апельсинами', NULL, NULL);
INSERT INTO res_food VALUES (22, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 13:57:36.71', false, NULL, NULL, 1, 0, 1800, 200, 'Мясо по-тайский', NULL, NULL);
INSERT INTO res_food VALUES (26, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 14:14:06.995', false, NULL, NULL, 1, 0, 60000, 10800, 'Барашек на вертеле', NULL, NULL);
INSERT INTO res_food VALUES (27, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 14:19:15.945', false, NULL, NULL, 1, 0, 1800, 300, 'Волшебная торбочка', NULL, NULL);
INSERT INTO res_food VALUES (24, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 15:02:59.817', false, NULL, NULL, 1, 0, 2500, 500, 'Барашек по-актюбинский', NULL, NULL);
INSERT INTO res_food VALUES (36, NULL, 4, '2014-03-28 14:31:56.632', '2014-03-30 17:45:00.238', false, NULL, NULL, 4, 0, 400, 150, 'Рис припущенный', NULL, NULL);
INSERT INTO res_food VALUES (32, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 16:32:15.828', false, NULL, NULL, 1, 0, 1800, 300, 'Рыба по-мельничьи', NULL, NULL);
INSERT INTO res_food VALUES (20, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 16:38:46.464', false, NULL, NULL, 1, 0, 2000, 300, 'Курица Стэй Фрай', NULL, NULL);
INSERT INTO res_food VALUES (19, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 16:45:41.608', false, NULL, NULL, 1, 0, 1800, 150, 'Семга под икорным соусом', NULL, NULL);
INSERT INTO res_food VALUES (33, NULL, 4, '2014-03-28 14:31:56.631', '2014-03-30 17:02:06.732', false, NULL, NULL, 1, 0, 1800, 300, 'Gallus с орехами', NULL, NULL);
INSERT INTO res_food VALUES (34, NULL, 4, '2014-03-28 14:31:56.632', '2014-03-30 18:02:44.621', false, NULL, NULL, 1, 0, 2000, 200, 'Salmo с орехами', NULL, NULL);
INSERT INTO res_food VALUES (66, NULL, 4, '2014-03-28 14:31:56.64', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Океан', NULL, NULL);
INSERT INTO res_food VALUES (71, 4, NULL, '2014-03-28 14:31:56.641', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Кызыл сулу', NULL, NULL);
INSERT INTO res_food VALUES (67, NULL, NULL, '2014-03-28 14:31:56.64', NULL, false, NULL, NULL, 3, 0, 1000, 200, 'Интрига', NULL, NULL);
INSERT INTO res_food VALUES (72, 4, NULL, '2014-03-28 14:31:56.641', NULL, false, NULL, NULL, 3, 0, 1200, 200, 'Гнездо глухаря', NULL, NULL);
INSERT INTO res_food VALUES (68, NULL, 4, '2014-03-28 14:31:56.64', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Турецкий гамбит', NULL, NULL);
INSERT INTO res_food VALUES (69, NULL, 4, '2014-03-28 14:31:56.641', '2014-03-26 16:02:51.61', false, NULL, NULL, 3, 0, 1200, 200, 'Бригантина', NULL, NULL);
INSERT INTO res_food VALUES (70, 4, NULL, '2014-03-28 14:31:56.641', NULL, false, NULL, NULL, 3, 0, 1200, 200, 'Санжар', NULL, NULL);
INSERT INTO res_food VALUES (5, NULL, 4, '2014-03-28 14:31:56.627', '2014-03-28 16:07:07.086', false, NULL, NULL, 2, 0, 2500, 400, 'Нептун', NULL, NULL);
INSERT INTO res_food VALUES (7, NULL, 4, '2014-03-28 14:31:56.627', '2014-03-28 16:07:45.589', false, NULL, NULL, 2, 0, 1800, 400, 'Дедушкин погребок', NULL, NULL);
INSERT INTO res_food VALUES (73, 4, 4, '2014-03-28 16:09:49.604', '2014-03-28 16:12:45.94', false, NULL, NULL, 2, 0, 2400, 400, 'Судак баттерфляй', NULL, NULL);
INSERT INTO res_food VALUES (8, NULL, 4, '2014-03-28 14:31:56.628', '2014-03-28 16:15:05.209', false, NULL, NULL, 2, 0, 1800, 300, 'Закуска по-японский', NULL, NULL);
INSERT INTO res_food VALUES (12, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-28 17:05:17.412', false, NULL, NULL, 2, 0, 2800, 450, 'Европа', NULL, NULL);
INSERT INTO res_food VALUES (18, NULL, 4, '2014-03-28 14:31:56.629', '2014-03-30 11:47:33.7', false, NULL, NULL, 1, 0, 1500, 310, 'Бешбармак', NULL, NULL);
INSERT INTO res_food VALUES (101, 4, 4, '2014-03-30 11:49:15.211', '2014-03-30 11:49:32.59', false, NULL, NULL, 1, 0, 400, 150, 'гарнир для бешбармака', NULL, NULL);
INSERT INTO res_food VALUES (121, 4, 4, '2014-03-30 14:01:42.528', '2014-03-30 14:01:59.114', false, NULL, NULL, 1, 0, 2400, 250, 'мясо по тайский большая ', NULL, NULL);
INSERT INTO res_food VALUES (25, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 14:09:38.597', false, NULL, NULL, 1, 0, 4000, 1000, 'Золотистый гусь с яблоками', NULL, NULL);
INSERT INTO res_food VALUES (122, 4, NULL, '2014-03-30 16:48:36.411', NULL, false, NULL, NULL, 1, 0, 2400, 0, 'семга под икорным большая ', NULL, NULL);
INSERT INTO res_food VALUES (23, NULL, 4, '2014-03-28 14:31:56.63', '2014-03-30 17:20:26.342', false, NULL, NULL, 1, 0, 2000, 200, 'Утка с апельсинами', NULL, NULL);
INSERT INTO res_food VALUES (123, 4, 4, '2014-03-30 17:23:09.779', '2014-03-30 17:33:23.074', false, NULL, NULL, 1, 0, 1800, 250, 'колбаски из печени', NULL, NULL);
INSERT INTO res_food VALUES (125, 4, 4, '2014-03-30 17:37:38.563', '2014-03-30 17:37:50.347', false, NULL, NULL, 1, 0, 1700, 250, 'колбаски по деревенски из курицы', NULL, NULL);
INSERT INTO res_food VALUES (126, 4, 4, '2014-03-30 17:42:04.616', '2014-03-30 17:42:21.205', false, NULL, NULL, 4, 0, 500, 200, 'картофельное пюре по домашнему', NULL, NULL);
INSERT INTO res_food VALUES (127, 4, 4, '2014-03-30 17:46:24.209', '2014-03-30 17:46:42.232', false, NULL, NULL, 4, 0, 600, 150, 'рис по итальянски', NULL, NULL);
INSERT INTO res_food VALUES (128, 4, 4, '2014-03-30 17:50:28.545', '2014-03-30 17:51:18.517', false, NULL, NULL, 4, 0, 600, 150, 'гарнир по мексикански', NULL, NULL);
INSERT INTO res_food VALUES (124, 4, 4, '2014-03-30 17:23:39.886', '2014-03-30 18:10:43.357', false, NULL, NULL, 1, 0, 2000, 250, 'колбаски по деревенски  из баранины', NULL, NULL);


-- Completed on 2014-03-31 12:49:26 AQTT

--
-- PostgreSQL database dump complete
--


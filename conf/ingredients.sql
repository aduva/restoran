--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.5
-- Dumped by pg_dump version 9.1.4
-- Started on 2014-03-31 12:50:35 AQTT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2294 (class 0 OID 252642)
-- Dependencies: 174
-- Data for Name: res_ingredient; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO res_ingredient VALUES (1, NULL, NULL, '2014-03-28 14:31:56.641', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Говядина мякоть', NULL, NULL);
INSERT INTO res_ingredient VALUES (2, NULL, NULL, '2014-03-28 14:31:56.644', NULL, false, 'мясо, колбасные', NULL, 1000, 'KILO', 'GRAMM', 'Ветчина говяжья', NULL, NULL);
INSERT INTO res_ingredient VALUES (3, NULL, NULL, '2014-03-28 14:31:56.644', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Язык говяжий', NULL, NULL);
INSERT INTO res_ingredient VALUES (4, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо, конина', NULL, 1000, 'KILO', 'GRAMM', 'Жая', NULL, NULL);
INSERT INTO res_ingredient VALUES (5, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо, конина', NULL, 1000, 'KILO', 'GRAMM', 'Казы', NULL, NULL);
INSERT INTO res_ingredient VALUES (6, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо, баранина', NULL, 1000, 'KILO', 'GRAMM', 'Баранина задняя нога на кости', NULL, NULL);
INSERT INTO res_ingredient VALUES (7, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Баранина коробка', NULL, NULL);
INSERT INTO res_ingredient VALUES (8, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Буженина (баранина)', NULL, NULL);
INSERT INTO res_ingredient VALUES (9, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо, колбасные', NULL, 1000, 'KILO', 'GRAMM', 'Колбаса сырокопченая (сервелат)', NULL, NULL);
INSERT INTO res_ingredient VALUES (10, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Карбонат копченый (корейка сырокопченая)', NULL, NULL);
INSERT INTO res_ingredient VALUES (11, NULL, NULL, '2014-03-28 14:31:56.645', NULL, false, 'мясо', NULL, 1000, 'KILO', 'GRAMM', 'Бастурма в специях', NULL, NULL);
INSERT INTO res_ingredient VALUES (12, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'мясо, колбасные', NULL, 1000, 'KILO', 'GRAMM', 'Салями', NULL, NULL);
INSERT INTO res_ingredient VALUES (13, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'мясо, рыба', NULL, 1000, 'KILO', 'GRAMM', 'Семга (ласось) свежая', NULL, NULL);
INSERT INTO res_ingredient VALUES (14, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'мясо, рыба', NULL, 1000, 'KILO', 'GRAMM', 'Семга (ласось, форель) тушка потрашенная с головой', NULL, NULL);
INSERT INTO res_ingredient VALUES (15, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'рыба', NULL, 1000, 'KILO', 'GRAMM', 'Семга (ласось) слабосоленая', NULL, NULL);
INSERT INTO res_ingredient VALUES (16, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'рыба, соленые', NULL, 1000, 'KILO', 'GRAMM', 'Белорыбица', NULL, NULL);
INSERT INTO res_ingredient VALUES (17, NULL, NULL, '2014-03-28 14:31:56.646', NULL, false, 'мясо, рыба', NULL, 1000, 'KILO', 'GRAMM', 'Судак филе', NULL, NULL);
INSERT INTO res_ingredient VALUES (18, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'мясо, рыба', NULL, 1000, 'KILO', 'GRAMM', 'Карп, сазан', NULL, NULL);
INSERT INTO res_ingredient VALUES (19, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Икра осетровая зернистая', NULL, NULL);
INSERT INTO res_ingredient VALUES (20, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Печень трески', NULL, NULL);
INSERT INTO res_ingredient VALUES (21, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы, икра', NULL, 1000, 'KILO', 'GRAMM', 'Икра искусственная', NULL, NULL);
INSERT INTO res_ingredient VALUES (22, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы, мясо', NULL, 1000, 'KILO', 'GRAMM', 'Селдь среднесоленая', NULL, NULL);
INSERT INTO res_ingredient VALUES (23, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Тунец консервированный', NULL, NULL);
INSERT INTO res_ingredient VALUES (24, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Кальмары маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (25, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'рыба', NULL, 1000, 'KILO', 'GRAMM', 'Креветки очищенные', NULL, NULL);
INSERT INTO res_ingredient VALUES (26, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Филе куриное (грудки)', NULL, NULL);
INSERT INTO res_ingredient VALUES (27, NULL, NULL, '2014-03-28 14:31:56.647', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куриная грудка копченая', NULL, NULL);
INSERT INTO res_ingredient VALUES (28, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, птица', NULL, 1000, 'KILO', 'GRAMM', 'Гусь целый (обработанная тушка 1 категории)', NULL, NULL);
INSERT INTO res_ingredient VALUES (29, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, птица', NULL, 1000, 'KILO', 'GRAMM', 'Утка (тушка 1кат)', NULL, NULL);
INSERT INTO res_ingredient VALUES (30, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куры целые (тушки 1 категории)', NULL, NULL);
INSERT INTO res_ingredient VALUES (31, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куриная голень', NULL, NULL);
INSERT INTO res_ingredient VALUES (32, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куриная окорочка', NULL, NULL);
INSERT INTO res_ingredient VALUES (33, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куриные крылышки', NULL, NULL);
INSERT INTO res_ingredient VALUES (34, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'мясо, курица', NULL, 1000, 'KILO', 'GRAMM', 'Куриный рулет копченый', NULL, NULL);
INSERT INTO res_ingredient VALUES (35, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Картофель', NULL, NULL);
INSERT INTO res_ingredient VALUES (36, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Лук', NULL, NULL);
INSERT INTO res_ingredient VALUES (37, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Морковь', NULL, NULL);
INSERT INTO res_ingredient VALUES (38, NULL, NULL, '2014-03-28 14:31:56.648', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Помидоры', NULL, NULL);
INSERT INTO res_ingredient VALUES (39, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Огурцы', NULL, NULL);
INSERT INTO res_ingredient VALUES (40, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Перец болгарский', NULL, NULL);
INSERT INTO res_ingredient VALUES (41, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Баклажаны', NULL, NULL);
INSERT INTO res_ingredient VALUES (42, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Чеснок', NULL, NULL);
INSERT INTO res_ingredient VALUES (43, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'зелень', NULL, 1000, 'KILO', 'GRAMM', 'Фасоль стручковая', NULL, NULL);
INSERT INTO res_ingredient VALUES (44, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи, специи', NULL, 1000, 'KILO', 'GRAMM', 'Имбирь свежий (корень)', NULL, NULL);
INSERT INTO res_ingredient VALUES (45, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Овощная смесь мексиканская', NULL, NULL);
INSERT INTO res_ingredient VALUES (46, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Капуста брокколи', NULL, NULL);
INSERT INTO res_ingredient VALUES (47, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Капуста бельгийская', NULL, NULL);
INSERT INTO res_ingredient VALUES (48, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'Капуста цветная', NULL, NULL);
INSERT INTO res_ingredient VALUES (49, NULL, NULL, '2014-03-28 14:31:56.649', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Помидоры соленые', NULL, NULL);
INSERT INTO res_ingredient VALUES (50, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Черри маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (51, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Корнишоны маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (52, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'маринады, овощи', NULL, 1000, 'KILO', 'GRAMM', 'Капуста квашеная', NULL, NULL);
INSERT INTO res_ingredient VALUES (53, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'овощи, специи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Каперсы маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (54, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'консервы', NULL, 1000, 'KILO', 'GRAMM', 'Фасоль красная в томате', NULL, NULL);
INSERT INTO res_ingredient VALUES (55, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'консервы', NULL, 1000, 'KILO', 'GRAMM', 'Горошек зеленый', NULL, NULL);
INSERT INTO res_ingredient VALUES (56, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Огурцы соленые', NULL, NULL);
INSERT INTO res_ingredient VALUES (57, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'консервы, грибы', NULL, 1000, 'KILO', 'GRAMM', 'Опята', NULL, NULL);
INSERT INTO res_ingredient VALUES (58, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'грибы, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Шампиньоны маринованые', NULL, NULL);
INSERT INTO res_ingredient VALUES (59, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'специи, овощи', NULL, 1000, 'KILO', 'GRAMM', 'Перец Чили (красный, острый) свежий', NULL, NULL);
INSERT INTO res_ingredient VALUES (60, NULL, NULL, '2014-03-28 14:31:56.65', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Кукуруза', NULL, NULL);
INSERT INTO res_ingredient VALUES (61, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'фрукты', NULL, 1000, 'KILO', 'GRAMM', 'Яблоки', NULL, NULL);
INSERT INTO res_ingredient VALUES (62, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'фрукты, цитрусовые', NULL, 1000, 'KILO', 'GRAMM', 'Апельсин', NULL, NULL);
INSERT INTO res_ingredient VALUES (63, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'фрукты', NULL, 1000, 'KILO', 'GRAMM', 'Ананас', NULL, NULL);
INSERT INTO res_ingredient VALUES (64, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'фрукты, цитрусовые, украшения', NULL, 1000, 'KILO', 'GRAMM', 'Лимон', NULL, NULL);
INSERT INTO res_ingredient VALUES (65, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'заправка, масло', NULL, 1000, 'KILO', 'GRAMM', 'Масло кунжутное', NULL, NULL);
INSERT INTO res_ingredient VALUES (66, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'заправка, масло', NULL, 1000, 'KILO', 'GRAMM', 'Масло оливковое', NULL, NULL);
INSERT INTO res_ingredient VALUES (67, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'заправка, масло', NULL, 1000, 'KILO', 'GRAMM', 'Масло подсолнечное', NULL, NULL);
INSERT INTO res_ingredient VALUES (68, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'зелень', NULL, 1000, 'KILO', 'GRAMM', 'Базилик', NULL, NULL);
INSERT INTO res_ingredient VALUES (69, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'зелень, украшения', NULL, 1000, 'KILO', 'GRAMM', 'Салатный лист', NULL, NULL);
INSERT INTO res_ingredient VALUES (74, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'яйца', NULL, 1000, 'KILO', 'GRAMM', 'Яйца куриные', NULL, NULL);
INSERT INTO res_ingredient VALUES (70, NULL, NULL, '2014-03-28 14:31:56.651', NULL, false, 'зелень, украшения, специи', NULL, 1000, 'KILO', 'GRAMM', 'Петрушка', NULL, NULL);
INSERT INTO res_ingredient VALUES (80, NULL, NULL, '2014-03-28 14:31:56.653', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Желатин', NULL, NULL);
INSERT INTO res_ingredient VALUES (98, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Орегано (майоран)', NULL, NULL);
INSERT INTO res_ingredient VALUES (111, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'орехи', NULL, 1000, 'KILO', 'GRAMM', 'Миндаль ядро', NULL, NULL);
INSERT INTO res_ingredient VALUES (121, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные, масло', NULL, 1000, 'KILO', 'GRAMM', 'Масло сливочное несоленое', NULL, NULL);
INSERT INTO res_ingredient VALUES (139, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'овощи, грибы', NULL, 1000, 'KILO', 'GRAMM', 'Шампиньоны свежие', NULL, NULL);
INSERT INTO res_ingredient VALUES (71, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'зелень, украшения, специи', NULL, 1000, 'KILO', 'GRAMM', 'Укроп', NULL, NULL);
INSERT INTO res_ingredient VALUES (81, NULL, NULL, '2014-03-28 14:31:56.653', NULL, false, 'соус, заправка', NULL, 1000, 'KILO', 'GRAMM', 'Кетчуп томатный', NULL, NULL);
INSERT INTO res_ingredient VALUES (99, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Паприка', NULL, NULL);
INSERT INTO res_ingredient VALUES (117, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'малочные', NULL, 1000, 'KILO', 'GRAMM', 'Сметана 40%', NULL, NULL);
INSERT INTO res_ingredient VALUES (127, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'мучные', NULL, 1000, 'KILO', 'GRAMM', 'Фунчоза', NULL, NULL);
INSERT INTO res_ingredient VALUES (132, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'алкоголь', NULL, 1000, 'KILO', 'GRAMM', 'Вино белое сухое', NULL, NULL);
INSERT INTO res_ingredient VALUES (72, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'зелень, специи', NULL, 1000, 'KILO', 'GRAMM', 'Мята перечная', NULL, NULL);
INSERT INTO res_ingredient VALUES (82, NULL, NULL, '2014-03-28 14:31:56.653', NULL, false, 'заправка, соус', NULL, 1000, 'KILO', 'GRAMM', 'Горчица', NULL, NULL);
INSERT INTO res_ingredient VALUES (96, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Лавровый лист', NULL, NULL);
INSERT INTO res_ingredient VALUES (106, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'семена, специи', NULL, 1000, 'KILO', 'GRAMM', 'Кунжутные семечки', NULL, NULL);
INSERT INTO res_ingredient VALUES (115, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Фета', NULL, NULL);
INSERT INTO res_ingredient VALUES (125, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные', NULL, 1000, 'KILO', 'GRAMM', 'Творог полужирный', NULL, NULL);
INSERT INTO res_ingredient VALUES (130, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'мучные', NULL, 1000, 'KILO', 'GRAMM', 'Блины', NULL, NULL);
INSERT INTO res_ingredient VALUES (73, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'зелень, овощи, украшения', NULL, 1000, 'KILO', 'GRAMM', 'Лук зеленый (перо)', NULL, NULL);
INSERT INTO res_ingredient VALUES (83, NULL, NULL, '2014-03-28 14:31:56.653', NULL, false, 'специи, мучные', NULL, 1000, 'KILO', 'GRAMM', 'Сухари панировачные', NULL, NULL);
INSERT INTO res_ingredient VALUES (88, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'уксус', NULL, 1000, 'KILO', 'GRAMM', 'Уксусная эссенция 70%', NULL, NULL);
INSERT INTO res_ingredient VALUES (93, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Специи для баранины', NULL, NULL);
INSERT INTO res_ingredient VALUES (103, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Инжир сушеный', NULL, NULL);
INSERT INTO res_ingredient VALUES (108, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'специи, сухофрукты', NULL, 1000, 'KILO', 'GRAMM', 'Чернослив без косточек', NULL, NULL);
INSERT INTO res_ingredient VALUES (113, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные', NULL, 1000, 'KILO', 'GRAMM', 'Молоко 2,5%', NULL, NULL);
INSERT INTO res_ingredient VALUES (123, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Сыр мраморный', NULL, NULL);
INSERT INTO res_ingredient VALUES (128, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'мучные, украшения', NULL, 1000, 'KILO', 'GRAMM', 'Валованы', NULL, NULL);
INSERT INTO res_ingredient VALUES (133, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'алкоголь', NULL, 1000, 'KILO', 'GRAMM', 'Коньяк (бренди)', NULL, NULL);
INSERT INTO res_ingredient VALUES (75, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'заправка, соус', NULL, 1000, 'KILO', 'GRAMM', 'Соус соевый', NULL, NULL);
INSERT INTO res_ingredient VALUES (85, NULL, NULL, '2014-03-28 14:31:56.654', NULL, false, 'заправка, соус, уксус', NULL, 1000, 'KILO', 'GRAMM', 'Уксус бальзамический белый', NULL, NULL);
INSERT INTO res_ingredient VALUES (90, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'специи, сахар', NULL, 1000, 'KILO', 'GRAMM', 'Сахар песок', NULL, NULL);
INSERT INTO res_ingredient VALUES (100, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Красный перец молотый', NULL, NULL);
INSERT INTO res_ingredient VALUES (118, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Гауда', NULL, NULL);
INSERT INTO res_ingredient VALUES (131, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'мучные', NULL, 1000, 'KILO', 'GRAMM', 'Тесто слоеное', NULL, NULL);
INSERT INTO res_ingredient VALUES (76, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'украшения, зелень, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Маслины без косточек', NULL, NULL);
INSERT INTO res_ingredient VALUES (86, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'специи, добавки', NULL, 1000, 'KILO', 'GRAMM', 'Сода пищевая', NULL, NULL);
INSERT INTO res_ingredient VALUES (95, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Специи для рыбы', NULL, NULL);
INSERT INTO res_ingredient VALUES (105, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Чили молотый', NULL, NULL);
INSERT INTO res_ingredient VALUES (110, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'орехи', NULL, 1000, 'KILO', 'GRAMM', 'Цукаты', NULL, NULL);
INSERT INTO res_ingredient VALUES (120, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Филадельфия', NULL, NULL);
INSERT INTO res_ingredient VALUES (138, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'фрукты, цитрусовые', NULL, 1000, 'KILO', 'GRAMM', 'Грейпфрут', NULL, NULL);
INSERT INTO res_ingredient VALUES (77, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'заправка, соус', NULL, 1000, 'KILO', 'GRAMM', 'Майонез', NULL, NULL);
INSERT INTO res_ingredient VALUES (87, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'уксус, заправка', NULL, 1000, 'KILO', 'GRAMM', 'Винный уксус', NULL, NULL);
INSERT INTO res_ingredient VALUES (92, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Черный перец', NULL, NULL);
INSERT INTO res_ingredient VALUES (102, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Шафран', NULL, NULL);
INSERT INTO res_ingredient VALUES (116, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Чечел', NULL, NULL);
INSERT INTO res_ingredient VALUES (126, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'мучные, хлеб', NULL, 1000, 'KILO', 'GRAMM', 'Батон нарезной', NULL, NULL);
INSERT INTO res_ingredient VALUES (135, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'овощи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Початки кукурузы', NULL, NULL);
INSERT INTO res_ingredient VALUES (78, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'специи, консервы', NULL, 1000, 'KILO', 'GRAMM', 'Томатная паста', NULL, NULL);
INSERT INTO res_ingredient VALUES (91, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Соль', NULL, NULL);
INSERT INTO res_ingredient VALUES (101, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Корица', NULL, NULL);
INSERT INTO res_ingredient VALUES (119, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные', NULL, 1000, 'KILO', 'GRAMM', 'Сметана домашняя', NULL, NULL);
INSERT INTO res_ingredient VALUES (137, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'яйца', NULL, 1000, 'KILO', 'GRAMM', 'Яйца перепелиные', NULL, NULL);
INSERT INTO res_ingredient VALUES (79, NULL, NULL, '2014-03-28 14:31:56.652', NULL, false, 'специи, соус', NULL, 1000, 'KILO', 'GRAMM', 'Жидкий дым', NULL, NULL);
INSERT INTO res_ingredient VALUES (97, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Кориандр', NULL, NULL);
INSERT INTO res_ingredient VALUES (107, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Мед натуральный', NULL, NULL);
INSERT INTO res_ingredient VALUES (112, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'орехи', NULL, 1000, 'KILO', 'GRAMM', 'Кешью ядро', NULL, NULL);
INSERT INTO res_ingredient VALUES (122, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Сыр плавленный колбасный копченый', NULL, NULL);
INSERT INTO res_ingredient VALUES (136, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'мясо, птица, филе', NULL, 1000, 'KILO', 'GRAMM', 'Утиное филе', NULL, NULL);
INSERT INTO res_ingredient VALUES (84, NULL, NULL, '2014-03-28 14:31:56.654', NULL, false, 'заправка, соус, уксус', NULL, 1000, 'KILO', 'GRAMM', 'Уксус 9%', NULL, NULL);
INSERT INTO res_ingredient VALUES (89, NULL, NULL, '2014-03-28 14:31:56.655', NULL, false, 'уксус', NULL, 1000, 'KILO', 'GRAMM', 'Уксус соевый', NULL, NULL);
INSERT INTO res_ingredient VALUES (94, NULL, NULL, '2014-03-28 14:31:56.656', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Специи для курицы', NULL, NULL);
INSERT INTO res_ingredient VALUES (104, NULL, NULL, '2014-03-28 14:31:56.657', NULL, false, 'специи', NULL, 1000, 'KILO', 'GRAMM', 'Зира', NULL, NULL);
INSERT INTO res_ingredient VALUES (109, NULL, NULL, '2014-03-28 14:31:56.658', NULL, false, 'орехи, украшения', NULL, 1000, 'KILO', 'GRAMM', 'Орех Грецкий', NULL, NULL);
INSERT INTO res_ingredient VALUES (114, NULL, NULL, '2014-03-28 14:31:56.659', NULL, false, 'молочные, сыр', NULL, 1000, 'KILO', 'GRAMM', 'Пармезан', NULL, NULL);
INSERT INTO res_ingredient VALUES (124, NULL, NULL, '2014-03-28 14:31:56.66', NULL, false, 'молочные', NULL, 1000, 'KILO', 'GRAMM', 'Сливки 35%', NULL, NULL);
INSERT INTO res_ingredient VALUES (129, NULL, NULL, '2014-03-28 14:31:56.661', NULL, false, 'мучные', NULL, 1000, 'KILO', 'GRAMM', 'Мука', NULL, NULL);
INSERT INTO res_ingredient VALUES (134, NULL, NULL, '2014-03-28 14:31:56.662', NULL, false, 'соки', NULL, 1000, 'KILO', 'GRAMM', 'Сок гранатовый', NULL, NULL);
INSERT INTO res_ingredient VALUES (161, 4, NULL, '2014-03-30 12:10:26.116', NULL, false, 'фрукты', NULL, 1000, 'KILO', 'GRAMM', 'киви', NULL, NULL);
INSERT INTO res_ingredient VALUES (181, 4, NULL, '2014-03-30 14:12:32.889', NULL, false, 'с газом', NULL, 1000, 'KILO', 'GRAMM', 'минеральная вода ', NULL, NULL);
INSERT INTO res_ingredient VALUES (182, 4, NULL, '2014-03-30 14:14:54.305', NULL, false, 'целая', NULL, 1000, 'KILO', 'GRAMM', 'баранина тушка', NULL, NULL);
INSERT INTO res_ingredient VALUES (183, 4, NULL, '2014-03-30 14:17:45.911', NULL, false, 'зелень', NULL, 1000, 'KILO', 'GRAMM', 'кинза', NULL, NULL);
INSERT INTO res_ingredient VALUES (184, 4, NULL, '2014-03-30 16:29:03.221', NULL, false, 'крупа', NULL, 1000, 'KILO', 'GRAMM', 'рис', NULL, NULL);
INSERT INTO res_ingredient VALUES (185, 4, NULL, '2014-03-30 17:31:02.372', NULL, false, 'масло', NULL, 1000, 'KILO', 'GRAMM', 'маргарин', NULL, NULL);
INSERT INTO res_ingredient VALUES (186, 4, NULL, '2014-03-30 17:34:08.542', NULL, false, 'говядина', NULL, 1000, 'KILO', 'GRAMM', 'печень', NULL, NULL);
INSERT INTO res_ingredient VALUES (187, 4, NULL, '2014-03-30 17:39:09.199', NULL, false, 'овощи', NULL, 1000, 'KILO', 'GRAMM', 'капуста белокачанная', NULL, NULL);


-- Completed on 2014-03-31 12:50:35 AQTT

--
-- PostgreSQL database dump complete
--


menu.bankets=Мероприятия
bankets.dashboard=Основная информация
bankets.add=Добавить мероприятие
bankets.edit=Править мероприятие
bankets.date=Число проведения
bankets.time=Время начала
bankets.type=Тип мероприятия
bankets.location=Зал
bankets.peopleAmount=Гостей
bankets.title=Примечания
bankets.event=Мероприятие
bankets.datetime=Дата и Время
bankets.calc=Калькуляция
bankets.shop=Закуп
bankets.payment=Оплата
bankets.action.shop=Составить Закупочный лист
bankets.action.calc=Провести Калькуляцию
bankets.tables=Столы
bankets.foods=Блюда
bankets.list=Список мероприятии
bankets.list.all=Весь список мероприятии
bankets.supplies=Закупочный лист
bankets.drinks=Напитки
bankets.tea=Чайный стол
bankets.fruits=Фрукты
bankets.calc.start=Начать калькуляцию
bankets.bills=Счета
bankets.blanks=Заготовки
bankets.contacts=Контакты
bankets.contacts.new=Добавить новый контакт
bankets.contacts.select.msg=Выберите контакты заказчика
bankets.contacts.select.btn=Выбрать
bankets.contacts.name=Заказчик
bankets.contacts.mobile=Мобильный
bankets.contacts.telephone=Домашний
bankets.contacts.iin=ИИН
bankets.tables.recalc=Пересчитать

blanks.amount.single=В одной порции
blanks.amount.overall=Общее количество

menu.calendar=Календарь
calendar.today=Cегодня
calendar.month=месяц
calendar.week=неделя
calendar.day=день

calendar.filter.day.this=Сегодня
calendar.filter.month.this=В этом месяце
calendar.filter.week.this=На неделе

menu.contacts=Контакты
contacts.mobile=Мобильный телефон
contacts.telephone=Домашний телефон
contacts.address=Адрес
contacts.firstname=Имя
contacts.lastname=Фамилия
contacts.mobile.enter=Введитие мобильный телефон
contacts.telephone.enter=Введитие домашний телефон
contacts.address.enter=Введитие домашний адрес
contacts.firstname.enter=Введитие имя
contacts.lastname.enter=Введитие фамилию
contacts.iin=ИИН
contacts.iin.enter=Введитие ИИН
contacts.add=Добавить контакт
contacts.name=ФИО
contacts.tags=Тэги
contacts.tags.enter=Введитие тэги
contacts.iin.unique.error=Контакт с таким ИИН уже существует

menu.employees=Персонал
employees.name=ФИО
employees.type=Должность
employees.add=Добавить работника
employees.firstname=Имя
employees.lastname=Фамилия
employees.type.select=Выберитие должность
employees.firstname.enter=Введитие имя
employees.lastname.enter=Введитие фамилию
employees.schedule=График
employees.salary=Заработная плата
employees.displaySchedule=Выводить на графике
employees.displaySchedule.true=Да
employees.displaySchedule.false=Нет


menu.foods=Блюда
foods.add=Добавить блюдо
foods.select=Выбор блюда
foods.places=Количество мест
foods.portions=Размер порции
foods.portions.enter=Введите размер порции
foods.food.empty=Блюдо не выбрано
foods.info.sum=Суммарная информация
foods.info.detail=Детальная информация
foods.title.ru=Наименование блюда на русском языке
foods.title.enter=Введите наименование блюда
foods.title.kk=Наименование блюда на казахском языке
foods.title.kk.enter=Введите наименование блюда на казахском языке
foods.title.en=Наименование блюда на английском языке
foods.title.en.enter=Введите наименование блюда на английском языке
foods.title=Наименование блюда
foods.portions.overall=Всего порции
foods.price=Цена за порцию
foods.price.enter=Введите цену за порцию
foods.price.overall=Общая сумма
foods.select.garnish=Выбор гарнира
foods.cost=Себестоимость
foods.cost.enter=Введите себестоимость блюда
foods.foodType=Тип блюда
foods.foodType.enter=Выберите тип блюда
foods.weight=Вес (г.)
foods.weight.enter=Введите вес блюда

menu.ingredient=Ингредиенты
menu.ingredients=Ингредиенты
ingredient.tags=Тэги
ingredient.tags.enter=Введите ключевые слова
ingredient.title=Наименование
ingredient.measureType=Единица измерения
ingredient.measureType.short=Ед. измерения
ingredient.title.enter=Введите наименование ингредиента

items.measureType.UNKNOWN=Неизвестно
items.measureType.GRAMM=Граммы
items.measureType.KILO=Килограммы
items.measureType.LITER=Литры
items.measureType.PIECES=Штуки

items.measureType.short.UNKNOWN=неизв.
items.measureType.short.GRAMM=гр.
items.measureType.short.KILO=кг.
items.measureType.short.LITER=л.
items.measureType.short.PIECES=шт.

items.measureType.add=Добавить единицу измерения
items.measureType.title=Наименование
items.measureType.netto=Вес нетто в граммах
items.measureType.brutto=брутто

items.measureType.unit=Единица

items.measureTypes=Единицы измерения
items.measureType.transformRatio=Пропорция на один грамм

ingredient.select=Выбор ингредиента
ingredient.amount=Количество
ingredient.amount.enter=Введите количество ингредиента
ingredient.add=Добавить ингредиент

menu.invoices=Счета и оплаты
invoices.add=Добавить оплату
invoices.id=Номер
invoices.title=Основание
invoices.title.enter=Введите основание
invoices.amount=Сумма
invoices.amount.enter=Введите сумму
invoices.type=Тип
invoices.list=Журнал
invoices.date=Дата
invoices.creator=Автор
invoices.lists=Журналы
invoices.list.add=Добавить журнал
invoices.list.title=Наименование
invoices.list.title.enter=Введите наименование журнала
invoices.main=Главная
invoices.invoiceType.EXPENSE=Расход
invoices.invoiceType.INCOME=Приход
invoices.invoiceType.BILL=Счет
invoices.banket=Счет
invoices.banket.type.FOOD=Блюда


menu.tables=Столы
tables.add=Добавить столы
tables.table_for_amount = на {0} чел
tables.select=Выбор стола
tables.quantity=Кол–во столов
tables.peopleAmount=Кол–во персон
tables.quantity.enter=Введите количество столов
tables.peopleAmount.enter=Введите количество людей
tables.unplacedPeople=Не рассаженных гостей
tables.title=Стол

menu.management=Управление рестораном
management.foods=Блюда
management.ingredients=Ингредиенты
management.tables=Столы
management.locations=Залы
management.stock=Склад
menu.kitchen=Kухня
management.cutlery=Столовые приборы

menu.supplies=Закупки
supplies.date=Дата закупа
supplies.date.enter=Введите дату закупа
supplies.view=Просмотреть закуп
supplies.name=Наименование закупа
supplies.name.enter=Введите наименование закупа
supplies.title=Наименование продукта
supplies.amount=Количество
supplies.measureType=Единица измерения
supplies.approve=Утвердить
supplies.update=Обновить
supplies.approved=Утверждено
supplies.approved.true=Закуп утвержден
supplies.approved.false=Закуп не утвержден
supplies.list.approved.true=Список утвержден
supplies.list.approved.false=Список не утвержден
supplies.need.amount=Необходимо
supplies.stock.amount=На складе
supplies.stock.blocked=Снято со склада
supplies.need.overall=Всего
supplies.final.amount=Закуплено
supplies.final.price=Цена за ед.
supplies.final.price.short=Цена
supplies.final.sum=Сумма
supplies.final.tostock=Приход на склад
supplies.final.result.add=Добавить закуп
supplies.action.block=Снять со склада (блок)
supplies.action.unblock=Вернуть на склад (разблок)
supplies.action.delivery.set=Доставка
supplies.action.delivery.unset=Убрать доставку
supplies.edited=Изменено
supplies.added=Добавлено
supplies.deleted=Удалено
supplies.list=Закупочный список
supplies.lists=Закупочные списки
supplies.list.add=Добавить список
supplies.add=Добавить продукт
supplies.item.select=Выбор продукта
supplies.amount.enter=Введите необходимое количество
supplies.result=Закуплено
supplies.foods=Распределение по блюдам

supplies.final.sum.overall=Итого сумма
supplies.final.amount.overall=Итого закуплено

payments.order.overall.foods=Итого всех блюд
payments.order.overall.sum=Общая сумма
payments.date=Дата оплаты
payments.amount=Сумма
payments.basis=Основание
payments.amount.enter=Введите сумму
payments.basis.select=Введите основание
payments.creator=Принято кем
payments.add=Добавить оплату
payments.balance=Баланс
bankets.payments=Оплата банкета
payments.invoice.income=Входящие по мероприятию
payments.invoice.bill=Счета по мероприятию
payments.invoice.income.overall=Итоговая сумма
payments.invoice.bill.overall=Итого по счетам
payments.invoice.bill.add=Добавить счет на оплату
payments.order.service.percents=Обслуживание
payments.order.person.cost=Стоимость на человека

menu.profile=Профиль

menu.stock=Склад
stock.amount=На складе
stock.locked=Занято
stock.available=Доступно
stock.update.date=Дата
stock.update.amount=Количество
stock.update.user=Пользователь
stock.update=Складской учет
stock.balance=Остаток

menu.household=Хозяйственные товары
household.tags=Тэги
household.tags.enter=Введите ключевые слова
household.title=Наименование
household.measureType=Единица измерения
household.title.enter=Введите наименование хоз. товара
household.add=Добавить хоз. товар
household.transformMeasureType=Единица измерения 2
items.measureType.transformRatio=Пропорция

menu.orders.templates=Правила калькуляции
orders.templates.rules=Правила
orders.templates.tableItem=Тип стола
orders.templates.foodType=Тип блюда
orders.templates.places=Количество мест
orders.templates.title=Наименование правила
orders.templates.title.enter=Введите наименование правила
orders.templates.add=Добавить правило

month.1=Январь
month.2=Февраль
month.3=Март
month.4=Апрель
month.5=Май
month.6=Июнь
month.7=Июль
month.8=Август
month.9=Сентябрь
month.10=Октябрь
month.11=Ноябрь
month.12=Декабрь

month.1.short=Янв
month.2.short=Фев
month.3.short=Мар
month.4.short=Апр
month.5.short=Май
month.6.short=Июн
month.7.short=Июл
month.8.short=Авг
month.9.short=Сен
month.10.short=Окт
month.11.short=Ноя
month.12.short=Дек

month.sp.1=Января
month.sp.2=Февраля
month.sp.3=Марта
month.sp.4=Апреля
month.sp.5=Мая
month.sp.6=Июня
month.sp.7=Июля
month.sp.8=Августа
month.sp.9=Сентября
month.sp.10=Октября
month.sp.11=Ноября
month.sp.12=Декабря

day.2=Понедельник
day.3=Вторник
day.4=Среда
day.5=Четверг
day.6=Пятница
day.7=Суббота
day.1=Воскресенье

day.2.short=Пон
day.3.short=Вто
day.4.short=Сре
day.5.short=Чет
day.6.short=Пят
day.7.short=Суб
day.1.short=Вос

day.2.min=Пн
day.3.min=Вт
day.4.min=Ср
day.5.min=Чт
day.6.min=Пт
day.7.min=Cб
day.1.min=Вс

auth.password=пароль
auth.login.another.user=Войти под другим пользователем
auth.signout=Выход

#====================Actions
action.save=Сохранить
action.cancel=Отмена
action.delete=Удалить
action.add=Добавить
action.search=Поиск
action.next=Далее
action.back=Назад
action.pdf=Экспорт в PDF
action.print=Печать
action.edit=Править
action.enable=Восстановить

main.item.created=Дата добавления
main.item.lastEdited=Последнее изменение
main.item.creator=Автор

menu.preview=Просмотр и печать
menu.main=Главная
menu.timeline=Действия

page.prev=Назад
page.next=Вперед

project.title=Саз-Сырнай

constraint.required=Требуется значение
error.required=Поле должно быть заполнено
error.empty=Ничего нет

#======================Messages
msg.info.nodata=Данных нет

#======================Alerts
msg.alert.success=Успех!
msg.alert.error=Ошибка!
msg.alert.info=Информация!
msg.alert.success.text=Операция завершилась успешно.
msg.alert.error.text=Ошибка при выполнении операции.

#===================Activities
create=добавил(а)
edit=исправил(а)
delete=удалил(а)
Banket=мероприятие
Order=калькуляцию
OrderTableItem=стол мероприятия
Food=блюдо
FoodIngredient=ингредиент блюда
Ingredient=ингредиент
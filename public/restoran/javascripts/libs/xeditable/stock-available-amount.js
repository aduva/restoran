(function ($) {
    "use strict";
    
    var StockAmount = function (options) {
        this.init('stock', options, StockAmount.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(StockAmount, $.fn.editabletypes.abstractinput);

    $.extend(StockAmount.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function() {
           this.$input = this.$tpl.find('input');
           if (this.options.min !== null) {
                this.$input.attr('min', this.options.min);
            } 
            
            if (this.options.max !== null) {
                this.$input.attr('max', this.options.max);
            } 
            
            if (this.options.step !== null) {
                this.$input.attr('step', this.options.step);
            }             
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }

            var prev = parseInt($(element).html());

            var html = prev + parseInt(value.amount);
            $(element).html(html); 
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="amount"]').val(value.amount);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
              amount: this.$input.filter('[name="amount"]').val(), 
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="amount"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    StockAmount.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-xedit-plugin"><input type="number" name="amount" class="input-small" required></div>',
             
        inputclass: '',
        step: null,
        min: null,
        max: null,
    });

    $.fn.editabletypes.stock = StockAmount;

}(window.jQuery));
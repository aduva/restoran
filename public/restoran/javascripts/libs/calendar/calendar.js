moment.lang('ru');
var options = {
  withCallbacks: true
},
 dayTemplate, groupname;


var clndr1 = $('#clndr1').clndr({
  template: $('#template-calendar').html(),
  startWithMonth: moment(),
  clickEvents: {
    click: function(target) {
      selectDay(target);
    },
    onMonthChange: function(month) {
      var start = moment(month).startOf("month"),
          end = month.endOf("month") + 1;

      var url = "/" + groupname + "/bankets/getBankets?start=" + start + "&end=" + end + "&_=" + moment();
      $.get(url, function( data ) {
        clndr1.setEvents(data);
      });
    }
  },
  multiDayEvents: {
    startDate: 'startDate',
    endDate: 'endDate'
  },
  forceSixRows: false
});

var clndr2 = $('#clndr2').clndr({
  template: $('#template-calendar').html(),
  startWithMonth: moment().add("months",1),
  clickEvents: {
    click: function(target) {
      selectDay(target);
    },
    onMonthChange: function(month) {
      var start = month.startOf("month") + 1,
      end = month.endOf("month") + 1;

      var url = "/" + groupname + "/bankets/getBankets?start=" + start + "&end=" + end + "&_=" + moment();
      $.get(url, function( data ) {
        clndr2.setEvents(data);
      });
    }
  },
  multiDayEvents: {
    startDate: 'startDate',
    endDate: 'endDate'
  },
  forceSixRows: false
});

function selectDay(target) {
  $(".day").removeClass("is-selected");
  $(target.element).addClass("is-selected");
  $('#side-info').html(dayTemplate(target));
  var ulr = '/' + groupname + '/data/amount/supplies/'+ (target.date.format('YYYY/MM/DD')),
    today = moment().startOf("day");

  console.log();
  if(today.isBefore(target.date, "day") || today.format("DD/MM/YYYY") === target.date.format("DD/MM/YYYY")) {
    $('#addEventButton').removeClass('disabled');
  }

  $.get(ulr, function (data) {
      if(data == "true")
        $('#supplyViewButton').removeClass('disabled');
  });
}

function setMonthNames() {
  var months = $('.month-select');
  $.each(months, function( index, value ) {
    var month = $(value).html(),
        curmonth = parseInt(moment().format("M")),
        nextmonth = curmonth + 1;

    if(curmonth == month || curmonth + 1 == month) {
      $(value).addClass("is-active");
      // $('.month-select:nth-child(' + (index+1) + ')').addClass("is-active");
    }
    $(value).html(moment($(value).html(), "MM").format("MMM"));
  });
}

$(".month-select").on("mousemove", function(e) {
  var left = $('.month-select:nth-child('+($(this).index())+')'),
      right = $('.month-select:nth-child('+($(this).index()+2)+')'),
      all = $('.month-select');

  all.removeClass('is-hover');
  if(!$(this).hasClass('is-active'))
      $(this).addClass('is-hover');

  if(e.offsetX < $(this).width()/2){
      if($(this).index() == 0 && !right.hasClass('is-active')) {
          right.addClass('is-hover');
      }
      else if($(this).index() != 0 && !left.hasClass('is-active')) {
          left.addClass('is-hover');
      }
  }
  else {
      if($(this).index() == (all.length-1) && !left.hasClass('is-active')) {
          left.addClass('is-hover');
      }
      else if($(this).index() != (all.length-1) && !right.hasClass('is-active')) {
          right.addClass('is-hover');
      }
  }
});

$(".month-select").on("mouseleave", function(e) {
  $('.month-select').removeClass("is-hover");
});

$(".month-select").on("click", function(e) {
  var left = $('.month-select:nth-child('+($(this).index())+')'),
      right = $('.month-select:nth-child('+($(this).index()+2)+')'),
      all = $('.month-select');
  all.removeClass('is-active is-hover');
  $(this).addClass('is-active');

  // clndr1.setMonth(parseInt($(this).attr('id')), options);

  if(e.offsetX < $(this).width()/2){
      if($(this).index() == 0) {
          right.addClass('is-active');
          clndr1.setMonth(parseInt($(this).attr('id')), options);
          clndr2.setMonth(parseInt(right.attr('id')), options);
      }
      else {
          left.addClass('is-active');
          clndr1.setMonth(parseInt(left.attr('id')), options);
          clndr2.setMonth(parseInt($(this).attr('id')), options);
      }
  }
  else {
      if($(this).index() == (all.length-1)) {
          left.addClass('is-active');
          clndr1.setMonth(parseInt(left.attr('id')), options);
          clndr2.setMonth(parseInt($(this).attr('id')), options);
      }
      else {
          right.addClass('is-active');
          clndr1.setMonth(parseInt($(this).attr('id')), options);
          clndr2.setMonth(parseInt(right.attr('id')), options);
      }
  }
});

$(".today-button").on("click", function(e) {
  e.preventDefault();
  activate(parseInt(moment().format("M")) + 1);

  clndr2.today();
  clndr1.today(options);
  clndr2.forward(options);
  setTimeout(function() {
    $("#clndr1 .today").click();  
  }, 100);
});

$(".next-button").on("click", function(e) {
  forward();
});

$(".previous-button").on("click", function(e) {
  back();
});

$(document).ready(function(e) {
  setMonthNames();

  var start = moment().startOf("month"),
      end = moment().endOf("month") + 1;

  var url = "/" + groupname + "/bankets/getBankets?start=" + start + "&end=" + end + "&_=" + moment();
  $.get(url, function( data ) {
    clndr1.setEvents(data);
  });

  start = moment().add("months",1).startOf("month");
  end = moment().add("months",1).endOf("month") + 1;

  url = "/" + groupname + "/bankets/getBankets?start=" + start + "&end=" + end + "&_=" + moment();
  $.get(url, function( data ) {
    clndr2.setEvents(data);
  });

  dayTemplate = _.template($('#template-day').html());
  $(".today-button").click();
});

$(document).keydown( function(e) {
  if(e.keyCode == 37) {
    // left arrow
    back();
  }
  if(e.keyCode == 39) {
    // right arrow
    forward();
  }
});

function forward() {
  clndr1.forward(options);
  clndr2.forward(options);
  var m = parseInt(clndr1.month.format("M")) + 1;
  activate(m);
}

function back() {
  clndr1.back(options);
  clndr2.back(options);
  var m = parseInt(clndr1.month.format("M")) + 1;
  activate(m);
}

function activate(m) {
  $(".month-select").removeClass("is-active");
  var n = m + 1;
  if(n > 14 )
    n = 3;

  $(".month-select:nth-child(" + m + ")").addClass("is-active");
  $(".month-select:nth-child(" + n + ")").addClass("is-active");
}

// This function will be executed when the user scrolls the page.
$(window).scroll(function(e) {
    // Get the position of the location where the scroller starts.
    // var scroller_anchor = $(".scroller_anchor").offset().top;
     
    // // Check if the user has scrolled and the current position is after the scroller start location and if its not already fixed at the top
    // if ($(this).scrollTop() >= scroller_anchor && $('.scroller').css('position') != 'fixed')
    // {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
    //     $('.scroller').css({
    //         'position': 'fixed',
    //         'top': '0px'
    //     });
    //     // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
    //     $('.scroller_anchor').css('height', '50px');
    // }
    // else if ($(this).scrollTop() < scroller_anchor && $('.scroller').css('position') != 'relative')
    // {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.
         
    //     // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
    //     $('.scroller_anchor').css('height', '0px');
         
    //     // Change the CSS and put it back to its original position.
    //     $('.scroller').css({
    //         'position': 'relative'
    //     });
    // }
});
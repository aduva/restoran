moment.lang('ru');

var options = {
  withCallbacks: true
},
 dayTemplate,
 groupname;


var clndr = $('#schedule').clndr({
  template: $('#template-calendar').html(),
  startWithMonth: moment(),
  showAdjacentMonths: true,
  clickEvents: {
    click: function(day) {
      if($(day.element).hasClass("empDay")) {
        dayClicked(day);
      }
    },
    onMonthChange: function(month) {
      monthChanged(month);
    }
  }
});

function setMonthNames() {
  var months = $('.month-select');
  $.each(months, function( index, value ) {
    var month = $(value).html(),
        curmonth = parseInt(moment().format("M")),
        nextmonth = curmonth + 1;

    if(curmonth == month) {
      $(value).addClass("is-active");
      // $('.month-select:nth-child(' + (index+1) + ')').addClass("is-active");
    }
    $(value).html(moment($(value).html(), "MM").format("MMM"));
  });
}

$(".month-select").on("mousemove", function(e) {
  var left = $('.month-select:nth-child('+($(this).index())+')'),
      right = $('.month-select:nth-child('+($(this).index()+2)+')'),
      all = $('.month-select');

  all.removeClass('is-hover');
  if(!$(this).hasClass('is-active'))
      $(this).addClass('is-hover');

  // if(e.offsetX < $(this).width()/2){
  //     if($(this).index() == 0 && !right.hasClass('is-active')) {
  //         right.addClass('is-hover');
  //     }
  //     else if($(this).index() != 0 && !left.hasClass('is-active')) {
  //         left.addClass('is-hover');
  //     }
  // }
  // else {
  //     if($(this).index() == (all.length-1) && !left.hasClass('is-active')) {
  //         left.addClass('is-hover');
  //     }
  //     else if($(this).index() != (all.length-1) && !right.hasClass('is-active')) {
  //         right.addClass('is-hover');
  //     }
  // }
});

function dayClicked(day) {
  console.log(day);
  var cur = $(day.element),
      empid = cur.attr('data-empid'),
      workDays = $('span.workDays[data-empid=' + empid + ']'),
      count = parseInt(workDays.text());
  
  jsRoutes.restoran.controllers.Employees.toggleSchedule(groupname).ajax({
    method: 'PUT',
    data: {
      employee: empid,
      date: day.date.format("DD.MM.YYYY")
    },
    success: function(response) {
      if(response.result === 'success') {
        if(response.action === 'delete') {
          cur.text('');
        } else if(response.action === 'add') {
          cur.text('X');
        }
        $('span.workDays[data-empid=' + empid + ']').empty();
        $('span.workDays[data-empid=' + empid + ']').text(response.workDays);
      }
    }
  });
}

$(".month-select").on("mouseleave", function(e) {
  $('.month-select').removeClass("is-hover");
});

$(".month-select").on("click", function(e) {
  var left = $('.month-select:nth-child('+($(this).index())+')'),
      right = $('.month-select:nth-child('+($(this).index()+2)+')'),
      all = $('.month-select');
  all.removeClass('is-active is-hover');
  $(this).addClass('is-active');

  clndr.setMonth(parseInt($(this).attr('id')), options);
});

$(document).keydown( function(e) {
  if(e.keyCode == 37) {
    // left arrow
    back();
  }
  if(e.keyCode == 39) {
    // right arrow
    forward();
  }
});

$(".next-button").on("click", function(e) {
  forward();
});

$(".previous-button").on("click", function(e) {
  back();
});

function forward() {
  clndr.forward(options);
  var m = parseInt(clndr.month.format("M")) + 1;
  activate(m);
}

function back() {
  clndr.back(options);
  var m = parseInt(clndr.month.format("M")) + 1;
  activate(m);
}

function activate(m) {
  $(".month-select").removeClass("is-active");

  $(".month-select:nth-child(" + m + ")").addClass("is-active");
  // $(".month-select:nth-child(" + n + ")").addClass("is-active");
}

function monthChanged(month) {
  console.log(month);
  var start = moment(month).startOf("month"),
      end = month.endOf("month") + 1;

  var url = "/" + groupname + "/management/employees/schedule/get?start=" + start + "&end=" + end + "&_=" + moment();
  $.get(url, function( data ) {
    clndr.setEvents(data);
  });
}

$(document).ready(function(e) {
  setMonthNames();
  monthChanged(moment());
});
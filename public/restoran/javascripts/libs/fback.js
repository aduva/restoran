var loadingTpl = "<div class='overlay'></div><div class='loading-img'></div>";

function loadingStarted(elem) {
    $(elem).append(loadingTpl);
}

function loadingFinished(elem) {
	$(elem + " > .overlay").remove();
    $(elem + " > .loading-img").remove();
}

function toggleAlert(type) {
	$('#alert-' + type).fadeIn(1000).delay(3000).fadeOut(1000);

}
import core.CmsModulesPlugin;
import core.models.CmsModule;

import java.lang.reflect.Method;

import play.Application;
import play.GlobalSettings;
import play.i18n.*;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Results.*;
import play.Play;

public class Global extends GlobalSettings {

	CmsModulesPlugin cms = null;
	
    public void onStart(Application app) {
        play.Logger.debug("Application Started");

        restoran.controllers.Stock.startUnlocker();

        if(Play.isDev()) {
        	// core.InitialData.insert(app);
        	    	
        	cms = Play.application().plugin(CmsModulesPlugin.class);
        	String moduleUrl = Play.application().configuration().getString("module.url");
        	
        	String moduleName = "restoran";
        	
        	if(moduleUrl == null)
        		moduleUrl = "restoran";
        	
        	CmsModule m = CmsModule.findByName(moduleName);
        	if(m == null) {
        		restoran.InitialData.insertModule(app, moduleName, moduleUrl);
        		m = CmsModule.findByName(moduleName);
        	}
        	cms.loadModule(m, restoran.controllers.Application.class);
        	
        	// restoran.InitialData.insertData(app);
        }
    }

    @Override
    public Result onHandlerNotFound(RequestHeader request) {
        Context ctx = Context.current();
        ctx.flash().put("error", Messages.get("error.notfound"));
        return Controller.redirect( "/sazsyrnai" );
    }

}
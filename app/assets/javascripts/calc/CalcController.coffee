CalcController = BaseController.extend ->
	_notifications: null
	_calcModel: null

	init: ($scope, CalcModel, Notifications, $route) ->
		@_notifications = Notifications
		@_calcModel = CalcModel
		super $scope
		@_calcModel.loadData()
		null

	defineListeners: ->
		super
		null

	defineScope: ->
		@$scope.instance = 'CalcController'
		null

	destroy: ->
		#Nothing
		null

CalcController.$inject = ['$scope','CalcModel','Notifications','$route'];
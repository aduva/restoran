CalcModel = EventDispatcher.extend ->
	_calcService: null

	loadData: ->
		@_calcService.loadData().then(@_handleDataLoadSuccess(@), @_handleDataLoadError(@))
		null

	_handleDataLoadSuccess: (result) ->
		@_data = result.data.data
		null

	_handleDataLoadError: (error) ->
		console.warn('CalcModel: Data Load Error')
		null

CalcModelProvider = Class.extend ->
	instance: new CalcModel()

	$get: ['CalcService', (CalcService) ->
		@instance._calcService = CalcService
		@instance
	]

angular.module('calc.CalcModel', []).provider('CalcModel', CalcModelProvider)
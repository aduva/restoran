CalcService = Class.extend ->
	LOAD_DATA_URL: '/data'

	loadData: ->
		@$http.get(@LOAD_DATA_URL)

	null

->
	CalcServiceProvider = Class.extend ->
		instance: new CalcService()

		$get: ['$http', ($http) ->
			@instance.$http = $http
			@instance
		]

		null

	angular.module('calc.CalcService', []).provider('CalcService', CalcServiceProvider)
	null
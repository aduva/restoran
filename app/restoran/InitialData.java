package restoran;

import java.util.List;
import java.util.Map;

import play.Application;
import play.libs.Yaml;
import core.models.CmsModule;

import com.avaje.ebean.Ebean;
import com.github.aduva.elasticsearch.IndexService;

import restoran.models.*;

public class InitialData {
	private static Map<String,List<Object>> all, foods;
	public static void insertModule(Application app, String name, String url) {
		all = loadData();

		for(Object module : all.get("module")) {
			((CmsModule) module).name = name;
			((CmsModule) module).url = url;
			((CmsModule) module).save();
		}
	}

	public static void reindex() {
		play.Logger.debug(">>>>>>> reindex");
		// Clean the index
		IndexService.cleanIndex();

		for(Location item : Ebean.find(Location.class).findList()) {
			item.index();
		}

		for(BanketType item : Ebean.find(BanketType.class).findList()) {
			item.index();
		}

		for(FoodType item : Ebean.find(FoodType.class).findList()) {
			item.index();
		}

		for(Food item : Ebean.find(Food.class).findList()) {
			item.index();
		}

		for(Item item : Ebean.find(Item.class).findList()) {
			item.index();
		}

		for(FoodIngredient item : Ebean.find(FoodIngredient.class).findList()) {
			item.index();
		}

		for(Supply item : Ebean.find(Supply.class).findList()) {
			item.index();
		}

		for(StockItem item : Ebean.find(StockItem.class).findList()) {
			item.index();
		}

		for(StockItemTakeOut item : Ebean.find(StockItemTakeOut.class).findList()) {
			item.index();
		}

		for(OrderTemplate item : Ebean.find(OrderTemplate.class).findList()) {
			item.index();
		}

		for(OrderTemplatePlace item : Ebean.find(OrderTemplatePlace.class).findList()) {
			item.index();
		}

		for(OrderTemplateItem item : Ebean.find(OrderTemplateItem.class).findList()) {
			item.index();
		}

		for(TableItem item : Ebean.find(TableItem.class).findList()) {
			item.index();
		}

		for(LocationTableItem item : Ebean.find(LocationTableItem.class).findList()) {
			item.index();
		}

		for(Banket item : Ebean.find(Banket.class).findList()) {
			item.index();
		}

		for(Order item : Ebean.find(Order.class).findList()) {
			item.index();
		}

		for(Invoice item : Ebean.find(Invoice.class).findList()) {
			item.index();
		}

		for(InvoiceItem item : Ebean.find(InvoiceItem.class).findList()) {
			item.index();
		}

		for(BanketInvoice item : Ebean.find(BanketInvoice.class).findList()) {
			item.index();
		}

		for(Profile item : Ebean.find(Profile.class).findList()) {
			item.index();
		}

		for(Contact item : Ebean.find(Contact.class).findList()) {
			item.index();
		}

		for(ContactItem item : Ebean.find(ContactItem.class).findList()) {
			item.index();
		}

		for(EmployeeType item : Ebean.find(EmployeeType.class).findList()) {
			item.index();
		}

		for(Employee item : Ebean.find(Employee.class).findList()) {
			item.index();
		}
		for(EmployeeSchedule item : Ebean.find(EmployeeSchedule.class).findList()) {
			item.index();
		}
	}
	
	public static void insertData(Application app) {
		all = loadData();
		if(Ebean.find(Location.class).findRowCount() == 0) {
			// Clean the index
			IndexService.cleanIndex();
			for(Object data : all.get("locations")) {
				Location item = (Location) data;
				item.save();
			}
		}

		if(Ebean.find(BanketType.class).findRowCount() == 0) {
			for(Object data : all.get("banketTypes")) {
				BanketType item = (BanketType) data;
				item.save();
			}
		}

		if(Ebean.find(FoodType.class).findRowCount() == 0) {
			for(Object data : all.get("foodTypes")) {
				FoodType item = (FoodType) data;
				item.save();
			}
		}

		// if(Ebean.find(Food.class).findRowCount() == 0) {
		// 	for(Object data : foods.get("foods")) {
		// 		Food item = (Food) data;
		// 		item.save();
		// 	}
		// }
		
		// if(Ebean.find(Ingredient.class).findRowCount() == 0) {
		// 	for(Object data : foods.get("ingredients")) {
		// 		Ingredient item = (Ingredient) data;
		// 		item.save();
		// 	}
		// }

		// if(Ebean.find(FoodIngredient.class).findRowCount() == 0) {
		// 	for(Object data : foods.get("foodIngredients")) {
		// 		FoodIngredient item = (FoodIngredient) data;
		// 		item.save();
		// 	}
		// }

		if(Ebean.find(OrderTemplate.class).findRowCount() == 0) {
			for(Object data : all.get("orderTemplates")) {
				OrderTemplate item = (OrderTemplate) data;
				item.save();
			}
		}

		if(Ebean.find(OrderTemplatePlace.class).findRowCount() == 0) {
			for(Object data : all.get("orderTemplatePlaces")) {
				OrderTemplatePlace item = (OrderTemplatePlace) data;
				item.save();
			}
		}

		if(Ebean.find(OrderTemplateItem.class).findRowCount() == 0) {
			for(Object data : all.get("orderTemplateItems")) {
				OrderTemplateItem item = (OrderTemplateItem) data;
				item.save();
			}
		}

		if(Ebean.find(TableItem.class).findRowCount() == 0) {
			for(Object data : all.get("tables")) {
				TableItem item = (TableItem) data;
				item.save();
			}

			for(Object data : all.get("locationTables")) {
				LocationTableItem item = (LocationTableItem) data;
				item.save();
			}
		}
	}
	
	private static Map<String,List<Object>> loadData() { 
		if(all == null)
			all = (Map<String,List<Object>>)Yaml.load("restoran-data.yml");
		return all;
	}
}

package restoran.utils;

import play.Application;

import org.reflections.*;
import org.reflections.util.*;
import org.reflections.scanners.*;

import java.util.Set;

public class Classpath {

	public static Set<Class<?>> getTypesAnnotatedWith(Application app, String packageName, Class<? extends java.lang.annotation.Annotation> annotation) {
		return getReflection(app, packageName).getTypesAnnotatedWith(annotation);
	}

	public static <T> Set<Class<? extends T>> getSubTypesOf(Application app, String packageName, Class<T> type) {
		Set<Class<? extends T>> subtypes = getReflection(app, packageName).getSubTypesOf(type);
		return subtypes;
	}

	public static Reflections getReflection(Application app, String packageName) {
		return new Reflections(
               new ConfigurationBuilder()
						.addUrls(ClasspathHelper.forPackage(packageName, app.classloader()))
						.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(packageName + ".")))
						.setScanners(new TypesScanner(), new TypeAnnotationsScanner(), new SubTypesScanner()));
	}
}
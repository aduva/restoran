package restoran.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

public class DateTimeHelper {
	public static boolean notSameDay(Date startDate, Date endDate) {
		DateTime start = new DateTime(startDate);
		DateTime end = new DateTime(endDate);

		int year = end.getYear() - start.getYear();
		int month = end.getMonthOfYear() - start.getMonthOfYear();
		int day = end.getDayOfMonth() - start.getDayOfMonth();

		if(year != 0 || month != 0 || day != 0)
			return true;

		return false;
	}
}
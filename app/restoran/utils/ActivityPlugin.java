package restoran.utils;

import play.Application;
import play.Logger;
import play.Plugin;
import play.Configuration;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ActivityPlugin extends Plugin {
    private final Application application;
    public Map<String, Class<?>> models;

    public ActivityPlugin(Application application) {
        this.application = application;
    }

    @Override
    public boolean enabled() {
        return true;
    }

    @Override
    public void onStart() {
        play.Logger.debug("Starting Activity Plugin");
        Configuration ebeanConf = Configuration.root().getConfig("activity");
        String[] modelsToLoad = ebeanConf.getString("models").split(",");
        loadModels(modelsToLoad);
    }

    private void loadModels(String[] toLoad) {
        Set<Class<?>> modelClasses = new HashSet<Class<?>>();

        for(String load: toLoad) {
            load = load.trim();
            if(load.endsWith(".*")) {
                modelClasses.addAll(Classpath.getSubTypesOf(application, load.substring(0, load.length()-2), core.models.BaseModel.class));
            }
        }

        models = new LinkedHashMap<String, Class<?>>(modelClasses.size());
        for(Class<?> c : modelClasses) {
            String name = c.getSimpleName();

            play.Logger.debug("[Activity] Registering class: " + name);

            if(models.containsKey(name))
                throw new RuntimeException("[Activity] Such model name is already defined [" + name + "]");

            models.put(name, c);
        }
    }

    private static String getName(Class<?> c) {
        return c.getSimpleName();
    }
}

package restoran.repos;

import java.util.List;
import java.util.Date;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;

import restoran.models.*;
import core.repos.UserRepo;
import core.controllers.Langs;

import com.github.aduva.elasticsearch.*;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.facet.*;
import org.elasticsearch.search.facet.terms.*;
import org.elasticsearch.search.facet.terms.longs.*;
import org.elasticsearch.search.facet.terms.strings.*;
import org.elasticsearch.search.sort.SortOrder;

public class FoodsRepo {

	public static IndexResults<Item> filterItems(String value, String itemType, boolean showDisabled, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "title." + Langs.curLang(), "tags", "measureType.title." + Langs.curLang() };

		play.Logger.debug(">>>>> " + displaySize + " " + displayStart);

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(!itemType.equals("items")) {
			TermFilterBuilder itemTerm = FilterBuilders.termFilter("itemType", itemType.toLowerCase());
			query = QueryBuilders.filteredQuery(query, itemTerm);
		}

		if(showDisabled == false) {
			TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, disabled);
		}

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter(fields[0], value.toLowerCase());
			PrefixFilterBuilder tagsPrefix = FilterBuilders.prefixFilter(fields[1], value.toLowerCase());
			PrefixFilterBuilder measurePrefix = FilterBuilders.prefixFilter(fields[2], value.toLowerCase());
			OrFilterBuilder or = FilterBuilders.orFilter(titlePrefix, tagsPrefix, measurePrefix);
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Item> indexQuery = Elastic.query(Item.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Item.class, indexQuery);
	}
	
	public static IndexResults<FoodIngredient> filterFoodIngredients(String value, int displayStart, int displaySize, int sortCol, String sortDirection, Long food) {
		String[] fields = { "title." + Langs.curLang(), "cost", "price", "foodType.title." + Langs.curLang() };

		QueryBuilder query = QueryBuilders.matchAllQuery();
		TermFilterBuilder foodFilter = FilterBuilders.termFilter("food", food);
		query = QueryBuilders.filteredQuery(query, foodFilter);

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title." + Langs.curLang(), value.toLowerCase());
			OrFilterBuilder or = null;
			try {
				TermFilterBuilder amount = FilterBuilders.termFilter("amount", Double.valueOf(value));	
				or = FilterBuilders.orFilter(titlePrefix, amount);
			} catch(NumberFormatException e) {
			}

			if(or != null)
				query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<FoodIngredient> indexQuery = Elastic.query(FoodIngredient.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart * displaySize);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(FoodIngredient.class, indexQuery);
	}

	public static IndexResults<FoodIngredient> foodIngredients(Long food) {
		QueryBuilder query = QueryBuilders.matchAllQuery();
		TermFilterBuilder foodFilter = FilterBuilders.termFilter("food", food);
		query = QueryBuilders.filteredQuery(query, foodFilter);

		IndexQuery<FoodIngredient> indexQuery = Elastic.query(FoodIngredient.class);
		indexQuery.setBuilder(query);
		indexQuery.from(0);
		indexQuery.size(200);
		indexQuery.addSort("id", SortOrder.valueOf("ASC"));
		
		return Elastic.search(FoodIngredient.class, indexQuery);
	}

	public static IndexResults<FoodIngredient> foodIngredientsByIngredient(Long ingredient) {
		QueryBuilder query = QueryBuilders.matchAllQuery();
		TermFilterBuilder ingredientFilter = FilterBuilders.termFilter("ingredient", ingredient);
		query = QueryBuilders.filteredQuery(query, ingredientFilter);

		IndexQuery<FoodIngredient> indexQuery = Elastic.query(FoodIngredient.class);
		indexQuery.setBuilder(query);
		indexQuery.from(0);
		indexQuery.size(200);
		indexQuery.addSort("id", SortOrder.valueOf("ASC"));
		
		return Elastic.search(FoodIngredient.class, indexQuery);
	}

	public static IndexResults<Food> filterFoods(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "title." + Langs.curLang(), "cost", "price", "foodType.title." + Langs.curLang() };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title." + Langs.curLang(), value.toLowerCase());
			PrefixFilterBuilder typePrefix = FilterBuilders.prefixFilter("foodType.title." + Langs.curLang(), value.toLowerCase());
			OrFilterBuilder or = null;
			try {
				TermFilterBuilder cost = FilterBuilders.termFilter("cost", Double.valueOf(value));	
				TermFilterBuilder price = FilterBuilders.termFilter("price", Double.valueOf(value));
				or = FilterBuilders.orFilter(titlePrefix, cost, price, typePrefix);
			} catch(NumberFormatException e) {
				or = FilterBuilders.orFilter(titlePrefix, typePrefix);
			}
			if(or != null)
				query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Food> indexQuery = Elastic.query(Food.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Food.class, indexQuery);
	}

	public static IndexResults<Food> foodsByTitle(String q, boolean showDisabled, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title." + Langs.curLang(), q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		if(showDisabled == false) {
			TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, disabled);
		}

		IndexQuery<Food> indexQuery = Elastic.query(Food.class);
		indexQuery.setBuilder(query);
		if(page > 0 && pageSize > 0) {
			indexQuery.from((page - 1) * pageSize);
			indexQuery.size(pageSize);
		}
		
		return Elastic.search(Food.class, indexQuery);
	}

	public static IndexResults<FoodType> foodTypesByTitle(String q, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title.ru", q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		IndexQuery<FoodType> indexQuery = Elastic.query(FoodType.class);
		indexQuery.setBuilder(query);
		if(page > 0 && pageSize > 0) {
			indexQuery.from((page - 1) * pageSize);
			indexQuery.size(pageSize);
		}
		
		return Elastic.search(FoodType.class, indexQuery);
	}

	public static IndexResults<Food> foodsByTitle(String q, boolean showDisabled, Long foodType, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title." + Langs.curLang(), q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		if(showDisabled == false) {
			TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, disabled);
		}
		
		if(foodType != null)	{
			TermFilterBuilder term = FilterBuilders.termFilter("foodType", foodType);
			query = QueryBuilders.filteredQuery(query, term);
		}

		IndexQuery<Food> indexQuery = Elastic.query(Food.class);
		indexQuery.setBuilder(query);
		if(page > 0 && pageSize > 0) {
			indexQuery.from((page - 1) * pageSize);
			indexQuery.size(pageSize);
		}
		
		return Elastic.search(Food.class, indexQuery);
	}
}

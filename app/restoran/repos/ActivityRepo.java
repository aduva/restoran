package restoran.repos;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;

import core.models.activity.Activity;
import core.models.activity.ActivityObject;
import core.models.activity.ActivityObjectInterface;

import java.util.List;
import java.util.Date;

import restoran.models.*;
import core.repos.*;
import core.controllers.Langs;

import com.github.aduva.elasticsearch.*;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.facet.*;
import org.elasticsearch.search.facet.terms.*;
import org.elasticsearch.search.facet.terms.longs.*;
import org.elasticsearch.search.facet.terms.strings.*;
import org.elasticsearch.search.sort.SortOrder;

public class ActivityRepo {
	public static Page<Activity> findAllByActorOrtarget(ActivityObjectInterface actor, int page, int pageSize) {
		String q = "find * where "
				+ "(actor.id = :aid and actor.objectType = :aot) "
				+ "or "
				+ "(subject.id = :aid and subject.objectType = :aot) "
				+ "order by date desc";
		return Ebean.createQuery(Activity.class, q)
				.setParameter("aid", actor.id())
				.setParameter("aot", actor.objectType())
				.findPagingList(pageSize).getPage(page);
	}
	
	public static Page<Activity> findAllByActor(ActivityObjectInterface actor, int page, int pageSize) {
		return Ebean.find(Activity.class).where().eq("actor.id", actor.id()).eq("actor.objectType", actor.objectType()).findPagingList(pageSize).getPage(page);
	}
	
	public static List<Activity> findAllBySubject(ActivityObjectInterface subject) {
		ActivityObject obj = findObject(Long.valueOf(subject.id()), subject.objectType());
		play.Logger.debug(">>>> " + obj.id);

		return Ebean.find(Activity.class).where().eq("subject", obj.id).findList();
	}
	
	public static IndexResults<Activity> findAllByObjectType(String type, int page, int pageSize) {

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(type != null && !type.isEmpty()) {
			TermFilterBuilder target = FilterBuilders.termFilter("target.objectType", type);
			TermFilterBuilder subject = FilterBuilders.termFilter("subject.objectType", type);	
			TermFilterBuilder actor = FilterBuilders.termFilter("actor.objectType", type);	
			OrFilterBuilder or = FilterBuilders.orFilter(actor, subject, target);
			
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Activity> indexQuery = Elastic.query(Activity.class);
		indexQuery.setBuilder(query);
		indexQuery.from((page - 1) * pageSize);
		indexQuery.size(pageSize);
		indexQuery.addSort("date", SortOrder.DESC);
		
		return Elastic.search(Activity.class, indexQuery);
	}

	public static IndexResults<Activity> findAllByObject(Long id, int page, int pageSize) {

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(id != null) {
			TermFilterBuilder target = FilterBuilders.termFilter("target", id);
			TermFilterBuilder subject = FilterBuilders.termFilter("subject", id);	
			TermFilterBuilder actor = FilterBuilders.termFilter("actor", id);	
			OrFilterBuilder or = FilterBuilders.orFilter(actor, subject, target);
			
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Activity> indexQuery = Elastic.query(Activity.class);
		indexQuery.setBuilder(query);
		indexQuery.from((page - 1) * pageSize);
		indexQuery.size(pageSize);
		indexQuery.addSort("date", SortOrder.DESC);
		
		return Elastic.search(Activity.class, indexQuery);
	}

	public static List<Activity> findAllByObject(Long objectId) {
		return Ebean.find(Activity.class).where().disjunction()
				.add(Expr.eq("actor", objectId))
				.add(Expr.eq("subject", objectId))
				.add(Expr.eq("target", objectId))
				.endJunction()
				.orderBy("date desc")
			.findList();
	}

	public static ActivityObject findObject(Long id, String objectType) {
		return Ebean.find(ActivityObject.class).where().eq("objectId", id).eq("objectType", objectType).findUnique();
	}
}

package restoran.repos;

import java.util.*;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;

import restoran.models.*;
import core.repos.*;
import core.controllers.Langs;

import com.github.aduva.elasticsearch.*;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.facet.*;
import org.elasticsearch.search.facet.terms.*;
import org.elasticsearch.search.facet.terms.longs.*;
import org.elasticsearch.search.facet.terms.strings.*;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.facet.FacetBuilders.*;

public class RestoranRepo {

	public static IndexResults<OrderTemplate> orderTemplatesByTitle(String q, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title." + Langs.curLang(), q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		IndexQuery<OrderTemplate> indexQuery = Elastic.query(OrderTemplate.class);
		indexQuery.setBuilder(query);
		indexQuery.from((page - 1) * pageSize);
		indexQuery.size(pageSize);
		
		return Elastic.search(OrderTemplate.class, indexQuery);
	}

	public static IndexResults<Contact> filterContacts(String value, String[] fields, int displayStart, int displaySize, int sortCol, String sortDirection) {
		

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			value = value.toLowerCase();
			PrefixFilterBuilder namePrefix = FilterBuilders.prefixFilter(fields[0], value);
			PrefixFilterBuilder iinPrefix = FilterBuilders.prefixFilter(fields[1] , value);
			PrefixFilterBuilder mobilePrefix = FilterBuilders.prefixFilter(fields[2] , value);
			PrefixFilterBuilder telephonePrefix = FilterBuilders.prefixFilter(fields[3] , value);
			PrefixFilterBuilder addressPrefix = FilterBuilders.prefixFilter(fields[4] , value);
			
			OrFilterBuilder or = FilterBuilders.orFilter(namePrefix, iinPrefix, mobilePrefix, telephonePrefix, addressPrefix);
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Contact> indexQuery = Elastic.query(Contact.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Contact.class, indexQuery);
	}

	public static IndexResults<Employee> filterEmployees(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "profile.name", "employeeType.title." + Langs.curLang() };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			value = value.toLowerCase();
			PrefixFilterBuilder namePrefix = FilterBuilders.prefixFilter(fields[0], value);
			PrefixFilterBuilder empTypePrefix = FilterBuilders.prefixFilter(fields[1] , value);
			// PrefixFilterBuilder name = FilterBuilders.prefixFilter("creator.name", value);
			
			OrFilterBuilder or = FilterBuilders.orFilter(namePrefix, empTypePrefix);
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Employee> indexQuery = Elastic.query(Employee.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Employee.class, indexQuery);
	}

	public static IndexResults<EmployeeType> employeeTypesByTitle(String q, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title.ru", q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		IndexQuery<EmployeeType> indexQuery = Elastic.query(EmployeeType.class);
		indexQuery.setBuilder(query);
		indexQuery.from((page - 1) * pageSize);
		indexQuery.size(pageSize);
		
		return Elastic.search(EmployeeType.class, indexQuery);
	}

	public static IndexResults<Supply> filterSupplyLists(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "date", "title", "creator" };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title", value);
			
			// OrFilterBuilder or = null;
			// try {
			// 	TermFilterBuilder cost = FilterBuilders.termFilter("cost", Double.valueOf(value));	
			// 	TermFilterBuilder price = FilterBuilders.termFilter("price", Double.valueOf(value));
			// 	or = FilterBuilders.orFilter(titlePrefix, cost, price, typePrefix);
			// } catch(NumberFormatException e) {
			// 	or = FilterBuilders.orFilter(titlePrefix, typePrefix);
			// }
			// if(or != null)
				query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		IndexQuery<Supply> indexQuery = Elastic.query(Supply.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Supply.class, indexQuery);
	}

	public static List<Map<String, String>> getFacets(IndexResults<InvoiceItem> indexResult) {
		List<Map<String, String>> facets = new ArrayList<Map<String, String>>();
		for(Facet f : indexResult.facets.facets()) {
			InternalTermsFacet isf = (InternalTermsFacet) f;
			
			for(TermsFacet.Entry entry : isf.getEntries()) {
				Map<String, String> facet = new HashMap<String, String>();
				play.Logger.debug(entry.getTerm() + " " + entry.getCount());
				facet.put("term", String.valueOf(entry.getTerm()));
				facet.put("count", String.valueOf(entry.getCount()));
				facets.add(facet);
			}
		}

		return facets;
	}

	public static IndexResults<InvoiceItem> facetInvoiceTitles(String value) {
		QueryBuilder query = QueryBuilders.matchAllQuery();
		IndexQuery<InvoiceItem> indexQuery = Elastic.query(InvoiceItem.class);
		indexQuery.setBuilder(query);

		TermsFacetBuilder titleFacet = FacetBuilders.termsFacet("title");
		titleFacet.facetFilter(
			FilterBuilders.andFilter(
				FilterBuilders.termFilter("invoiceType", "income"),
        		FilterBuilders.prefixFilter("title", value)
        	)
    	);

		titleFacet.field("title");
		indexQuery.addFacet(titleFacet);

		// indexQuery.from((page - 1) * pageSize);
		// indexQuery.size(pageSize);

		return Elastic.search(InvoiceItem.class, indexQuery);
	}

	public static IndexResults<InvoiceItem> filterInvoiceItems(String value, Long id, boolean showDisabled, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "id", "title", "amount", "creator" };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title", value);
			PrefixFilterBuilder name = FilterBuilders.prefixFilter("creator.name", value);
			
			OrFilterBuilder or = null;
			try {
				TermFilterBuilder idTerm = FilterBuilders.termFilter("id", Long.valueOf(value));
				TermFilterBuilder amount = FilterBuilders.termFilter("amount", Integer.parseInt(value));
				or = FilterBuilders.orFilter(idTerm, titlePrefix, amount, name);
			} catch(NumberFormatException e) {
				or = FilterBuilders.orFilter(titlePrefix, name);
			}
			if(or != null)
				query = QueryBuilders.filteredQuery(query, or);
		}

		if(id != null) {
			TermFilterBuilder invoice = FilterBuilders.termFilter("invoice", id);
			query = QueryBuilders.filteredQuery(query, invoice);
		}

		if(showDisabled == false) {
			TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, disabled);
		}

		IndexQuery<InvoiceItem> indexQuery = Elastic.query(InvoiceItem.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[0], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(InvoiceItem.class, indexQuery);
	}

	public static IndexResults<Invoice> filterInvoices(String value, boolean showDisabled, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "title" };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title", value);
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		if(showDisabled == false) {
			TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, disabled);
		}

		IndexQuery<Invoice> indexQuery = Elastic.query(Invoice.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Invoice.class, indexQuery);
	}

	public static List<InvoiceItem> filterBanketInvoices(String value, Long invoiceId, String invoiceType, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = {"id", "title", "amount", "creator", "dateCreated" };

		// QueryBuilder query = QueryBuilders.matchAllQuery();

		// if(value != null && !value.isEmpty()) {
		// 	PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("title", value);
		// 	query = QueryBuilders.filteredQuery(query, titlePrefix);
		// }

		// if(invoiceId != null) {
		// 	TermFilterBuilder invoice = FilterBuilders.termFilter("invoice", invoiceId.toString());	
		// 	query = QueryBuilders.filteredQuery(query, invoice);
		// }

		// if(invoiceType != null) {
		// 	TermFilterBuilder type = FilterBuilders.termFilter("invoiceType", invoiceType.toLowerCase());	
		// 	query = QueryBuilders.filteredQuery(query, type);
		// }

		// TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
		// query = QueryBuilders.filteredQuery(query, disabled);

		// IndexQuery<InvoiceItem> indexQuery = Elastic.query(InvoiceItem.class);
		// indexQuery.setBuilder(query);
		// indexQuery.from(displayStart);
		// indexQuery.size(displaySize);
		// indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		// return Elastic.search(InvoiceItem.class, indexQuery);

		List<InvoiceItem> res = BaseRepo.find(InvoiceItem.class)
			.where().eq("invoiceType", invoiceType.toUpperCase()).eq("invoice", invoiceId)
			.setFirstRow(displayStart).setMaxRows(displaySize).order("id desc").findList();
		return res;
	}

	public static IndexResults<StockItem> filterStockIngredients(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "ingredient.title." + Langs.curLang(), "amount" };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("ingredient.title." + Langs.curLang(), value.toLowerCase());
			OrFilterBuilder or = null;
			try {
				TermFilterBuilder amount = FilterBuilders.termFilter("amount", Double.valueOf(value));	
				or = FilterBuilders.orFilter(titlePrefix, amount);
			} catch(NumberFormatException e) {
				or = FilterBuilders.orFilter(titlePrefix);
			}
			if(or != null)
				query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<StockItem> indexQuery = Elastic.query(StockItem.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(StockItem.class, indexQuery);
	}

	public static IndexResults<StockItemTakeOut> filterStockItemTakeOuts(Long id, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "dateCreated", "amount", "creator" };

		QueryBuilder query = QueryBuilders.matchAllQuery();
		TermFilterBuilder ingr = FilterBuilders.termFilter("item", id);
		// if(value != null && !value.isEmpty()) {
		// 	PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("ingredient.title." + Langs.curLang(), value.toLowerCase());
		// 	OrFilterBuilder or = null;
		// 	try {
		// 		TermFilterBuilder amount = FilterBuilders.termFilter("amount", Double.valueOf(value));	
		// 		or = FilterBuilders.orFilter(titlePrefix, amount);
		// 	} catch(NumberFormatException e) {
		// 		or = FilterBuilders.orFilter(titlePrefix);
		// 	}
		// 	if(or != null)
		// 		query = QueryBuilders.filteredQuery(query, or);
		// }

		query = QueryBuilders.filteredQuery(query, ingr);
		play.Logger.debug(sortCol + " " + sortDirection);

		IndexQuery<StockItemTakeOut> indexQuery = Elastic.query(StockItemTakeOut.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(StockItemTakeOut.class, indexQuery);
	}

	public static IndexResults<OrderTemplate> filterOrderTemplates(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "title." + Langs.curLang() };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter(fields[0], value.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}

		IndexQuery<OrderTemplate> indexQuery = Elastic.query(OrderTemplate.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart * displaySize);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(OrderTemplate.class, indexQuery);
	}

	public static BanketInvoice findBanketInvoice(Long banketId) {
		return BaseRepo.find(BanketInvoice.class).where().eq("banket", banketId).findUnique();
	}

	public static List<InvoiceItem> findBanketInvoices(Long banketId) {
		return findBanketInvoices(banketId, false);
	}
	
	public static List<InvoiceItem> findBanketInvoices(Long banketId, boolean disables) {
		BanketInvoice bi = findBanketInvoice(banketId);
		if(bi == null)
			return null;

		if(disables) 
			return Ebean.find(InvoiceItem.class).where().eq("invoice", bi.invoice).orderBy("dateCreated desc").findList();
			
		return Ebean.find(InvoiceItem.class).where().eq("invoice", bi.invoice).eq("isDisabled", false).orderBy("dateCreated desc").findList();
	}

	public static List<InvoiceItem> findBanketInvoicesOverall(Long banketId, String type) {
		BanketInvoice bi = findBanketInvoice(banketId);
		if(bi == null)
			return null;
			
		return Ebean.find(InvoiceItem.class).where().eq("invoice", bi.invoice).eq("isDisabled", false).orderBy("dateCreated desc").findList();
	}
	
	public static IndexResults<Banket> filterBankets(String value, int displayStart, int displaySize, int sortCol, String sortDirection) {
		String[] fields = { "banketType.title." + Langs.curLang(), "date", "peopleAmount", "location.title." + Langs.curLang() };

		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(value != null && !value.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter(fields[0], value.toLowerCase());
			// PrefixFilterBuilder datePrefix = FilterBuilders.prefixFilter(fields[1], value.toLowerCase());
			// PrefixFilterBuilder peoplePrefix = FilterBuilders.prefixFilter(fields[2], value.toLowerCase());
			PrefixFilterBuilder locationPrefix = FilterBuilders.prefixFilter(fields[3], value.toLowerCase());
			OrFilterBuilder or = FilterBuilders.orFilter(titlePrefix, /*datePrefix, peoplePrefix,*/ locationPrefix);
			query = QueryBuilders.filteredQuery(query, or);
		}

		IndexQuery<Banket> indexQuery = Elastic.query(Banket.class);
		indexQuery.setBuilder(query);
		indexQuery.from(displayStart * displaySize);
		indexQuery.size(displaySize);
		indexQuery.addSort(fields[sortCol], SortOrder.valueOf(sortDirection.toUpperCase()));
		
		return Elastic.search(Banket.class, indexQuery);
	}

	public static List<Location> findUserLocations(Long userId) {
		List<Long> groups = UserRepo.findGroupIds(userId);
		List<Location> list = Ebean.find(Location.class).where().in("group", groups).findList();
		return list;
	}

	public static List<BanketType> findUserBanketTypes(Long userId) {
		List<Long> groups = UserRepo.findGroupIds(userId);
		List<BanketType> list = Ebean.find(BanketType.class).where().in("group", groups).findList();
		return list;
	}

	public static Order findBanketOrder(Long id) {
		return Ebean.find(Order.class).where().eq("banket", id).findUnique();
	}

	public static List<OrderTemplateItem> findTemplateItems(Long id) {
		return Ebean.find(OrderTemplateItem.class).where().eq("template", id).orderBy("foodType").findList();
	}

	public static OrderTemplateItem findTemplateItemByFoodType(Long foodType) {
		return Ebean.find(OrderTemplateItem.class).where().eq("foodType", foodType).eq("template", 1L).findUnique();
	}

	public static IndexResults<LocationTableItem> tablesByTitle(String q, Long location, int page, int pageSize) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		if(q != null && !q.isEmpty()) {
			PrefixFilterBuilder titlePrefix = FilterBuilders.prefixFilter("tableItem.title." + Langs.curLang(), q.toLowerCase());
			query = QueryBuilders.filteredQuery(query, titlePrefix);
		}
		
		if(location != null)	{
			TermFilterBuilder term = FilterBuilders.termFilter("location", location);
			query = QueryBuilders.filteredQuery(query, term);
		}
		

		IndexQuery<LocationTableItem> indexQuery = Elastic.query(LocationTableItem.class);
		indexQuery.setBuilder(query);
		indexQuery.from((page - 1) * pageSize);
		indexQuery.size(pageSize);
		
		return Elastic.search(LocationTableItem.class, indexQuery);
	}

	public static IndexResults<Banket> banketsByDate(Date start, Date end) {
		return banketsByDate(start, end, false);
	}

	public static IndexResults<Banket> banketsByDate(Date start, Date end, boolean findDisabled) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		NumericRangeFilterBuilder range = FilterBuilders.numericRangeFilter("date");

		if(start != null && end != null) {
			range.gt(start);
			range.lte(end);
			query = QueryBuilders.filteredQuery(query, range);
		}

		if(!findDisabled) {
			TermFilterBuilder term = FilterBuilders.termFilter("isDisabled", false);
			query = QueryBuilders.filteredQuery(query, term);
		}
		

		IndexQuery<Banket> indexQuery = Elastic.query(Banket.class);
		indexQuery.setBuilder(query);
		indexQuery.from(0);
		indexQuery.size(200);
		indexQuery.addSort("date", SortOrder.ASC);
		
		try {
			return Elastic.search(Banket.class, indexQuery);
		} catch(Exception e) {

		}

		return null;
	}

	public static IndexResults<EmployeeSchedule> schedulesByDate(Date start, Date end, Long employee) {
		QueryBuilder query = QueryBuilders.matchAllQuery();

		NumericRangeFilterBuilder range = FilterBuilders.numericRangeFilter("date");

		if(start != null && end != null) {
			range.gte(start);
			range.lte(end);
			query = QueryBuilders.filteredQuery(query, range);
		}

		if(employee != null) {
			TermFilterBuilder emp = FilterBuilders.termFilter("employee", employee);
			query = QueryBuilders.filteredQuery(query, emp);
		}		

		TermFilterBuilder disabled = FilterBuilders.termFilter("isDisabled", false);
		query = QueryBuilders.filteredQuery(query, disabled);

		IndexQuery<EmployeeSchedule> indexQuery = Elastic.query(EmployeeSchedule.class);
		indexQuery.setBuilder(query);
		indexQuery.from(0);
		indexQuery.size(2000);
		indexQuery.addSort("date", SortOrder.ASC);
		
		try {
			return Elastic.search(EmployeeSchedule.class, indexQuery);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static TableItem findTableByCode(TableItem.Code code) {
		return Ebean.find(TableItem.class).where().eq("code", code).findUnique();
	}

	public static List<FoodType> findFoodTypes() {
		return BaseRepo.findAll(FoodType.class);
	}

	public static List<LocationTableItem> findLocationTables() {
		return Ebean.find(LocationTableItem.class).where().orderBy("priority").findList();
	}

	public static LocationTableItem findLocationTable(Long location, Long tableItem) {
		return Ebean.find(LocationTableItem.class).where().eq("location", location).eq("tableItem", tableItem).findUnique();
	}

	public static List<FoodOrder> findFoodOrders(Long id) {
		return Ebean.find(FoodOrder.class).where().eq("order", id).isNull("parent").orderBy("foodType, id").findList();
	}

	public static List<FoodOrder> findFoodOrdersByParent(Long id) {
		return Ebean.find(FoodOrder.class).where().eq("parent", id).orderBy("foodType, id").findList();	
	}

	public static void deleteFoodOrdersByParent(Long id) {
		List<Object> ids = Ebean.find(FoodOrder.class).where().eq("parent", id).findIds();
		Ebean.delete(FoodOrder.class, ids);
	}

	public static List<FoodOrderTableItem> findFoodOrderByTables(Long orderId, Long tableItem) {
		return Ebean.find(FoodOrderTableItem.class).where().eq("order", orderId).eq("orderTableItem", tableItem).findList();
	}

	public static List<LocationTableItem> findTablesByLocation(Long location) {
		return Ebean.find(LocationTableItem.class).where().eq("location", location).orderBy("priority").findList();
	}

	public static List<OrderTableItem> findOrderTables(Long order) {
		return Ebean.find(OrderTableItem.class).where().eq("order", order).findList();
	}

	public static List<OrderTableItem> findOrderTablesByTableItem(Long order, Long tableItem) {
		return Ebean.find(OrderTableItem.class).where().eq("order", order).eq("tableItem", tableItem).findList();
	}

	public static List<FoodOrderTableItem> findFoodOrderTableItems(Long order, Long foodOrder) {
		return Ebean.find(FoodOrderTableItem.class).where().eq("order", order).eq("foodOrder", foodOrder).findList();
	}

	public static void deleteFoodOrderTables(Long foodOrder) {
		List<Object> ids = Ebean.find(FoodOrderTableItem.class).where().eq("foodOrder", foodOrder).findIds();
		Ebean.delete(FoodOrderTableItem.class, ids);
	}

	public static void deleteFoodOrderTablesByOrder(Long order) {
		List<Object> ids = Ebean.find(FoodOrderTableItem.class).where().eq("order", order).findIds();
		Ebean.delete(FoodOrderTableItem.class, ids);
	}

	public static void deleteFoodOrderTablesByOrderAndTableItem(Long order, Long orderTableItem) {
		List<Object> ids = Ebean.find(FoodOrderTableItem.class).where().eq("order", order).eq("orderTableItem", orderTableItem).findIds();
		Ebean.delete(FoodOrderTableItem.class, ids);
	}

	public static void deleteOrderTables(Long order) {
		List<Object> ids = Ebean.find(OrderTableItem.class).where().eq("order", order).findIds();
		Ebean.delete(OrderTableItem.class, ids);
	}

	public static List<OrderTemplate> findOrderTemplates() {
		return BaseRepo.findAll(OrderTemplate.class);
	}
}

package restoran.models;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.IndexUtils;

import core.models.BaseModel;
import core.repos.BaseRepo;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.data.format.Formats;
import static play.data.validation.Constraints.*;

@Entity
@Table(name="res_banket")
@IndexType(name="res_banket")
public class Banket extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "banket";
	
	@Required
	@Column(name="title_")
	public String title;

	@Required
	@Column(name="banket_type_")
	public Long banketType;
	
	@Column(name="date_")
	@Formats.DateTime(pattern="dd.MM.yyyy HH:mm")
	public Calendar date;

	@Transient
	@Required
	@Formats.DateTime(pattern="HH:mm")
	public Date time;

	@Transient
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Date date_submit;

	@Transient
	@Required
	public String date_;

	@Required
	@Column(name="people_amount_")
	public int peopleAmount;
	
	@Required
	@Column(name="location_")
	public Long location;

	@Required
	@Column(name="group_")
	public Long group;

	@Override
	public String displayName() {
		return toString();
	}

	@Override
	public String toString() {
		return new StringBuffer()
			.append(core.utils.DateTimeHelper.formatDate(date, "dd.MM.yyyy HH:mm"))
			.append(" - ").append(location().title()).append(" - ")
			.append(this.banketType().title()).append(" - ")
			.append(this.peopleAmount).toString();
			
	}

	public String shortString() {
		return new StringBuffer()
			.append(core.utils.DateTimeHelper.formatDate(date, "dd.MM.yyyy HH:mm"))
			.append(" - ").append(location().title()).toString();
			
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Location location() {
		return BaseRepo.findById(Location.class, location);
	}

	public BanketType banketType() {
		return BaseRepo.findById(BanketType.class, banketType);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("peopleAmount", peopleAmount);
		map.put("group", group.toString());
		map.put("location", location.toString());
		map.put("location.title.ru", location().title.ru);
		map.put("location.title.kk", location().title.kk);
		map.put("location.title.en", location().title.en);
		map.put("banketType", banketType.toString());
		map.put("banketType.title.ru", banketType().title.ru);
		map.put("banketType.title.kk", banketType().title.kk);
		map.put("banketType.title.en", banketType().title.kk);
		map.put("title", title);
		map.put("date", date.getTime());
		map.put("id", id.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Banket item = new Banket();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = (String) map.get("title");
		item.group = Long.valueOf( (String) map.get("group") );
		item.location = Long.valueOf( (String) map.get("location") );
		item.banketType = Long.valueOf( (String) map.get("banketType") );
		item.peopleAmount = (int) map.get("peopleAmount");
		item.date = Calendar.getInstance();
		item.date.setTime( (Date) IndexUtils.convertValue(map.get("date"), Date.class) );

		return item;
	}

}

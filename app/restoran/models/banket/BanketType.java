package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.avaje.ebean.annotation.EnumValue;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;

@Entity
@Table(name="res_banket_type")
@IndexType(name="res_banket_type")
public class BanketType extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "banket-type";

	public enum Code {
		@EnumValue("DEFAULT")
		DEFAULT, 
		@EnumValue("WEDDING")
		WEDDING
	}

	@Column(name="type_")
	public Code code = Code.DEFAULT;

	@Column(name="group_")
	public Long group;

	@Embedded
	public Title title;

	public String title(String lang) {
		return this.title.get(lang);
	}
	
	public String title() {
		return title(Langs.curLang());
	}

	@Override
	public String displayName() {
		return title.ru + "/" + title.kk + "/" + title.en;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = new HashMap();
		map.put("group", group.toString());
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		map.put("id", id.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		FoodType item = new FoodType();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.group = Long.valueOf( (String) map.get("group") );

		return item;
	}

}

package restoran.models;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.repos.BaseRepo;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import com.avaje.ebean.annotation.EnumValue;

@Entity
@Table(name="res_banket_supply")
@IndexType(name="res_banket_supply")
public class BanketSupply extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "banket-supply";

	@Column(name="banket_")
	public Long banket;

	@Column(name="supply_")
	public Long supply;

	@Override
	public String displayName() {
		return null;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Banket banket() {
		return BaseRepo.findById(Banket.class, this.banket);
	}

	public Supply supply() {
		return BaseRepo.findById(Supply.class, this.supply);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("banket", banket.toString());
		map.put("supply", supply.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		BanketSupply item = new BanketSupply();
		item.id = Long.valueOf( (String) map.get("id") );
		item.banket = Long.valueOf( (String) map.get("banket") );
		item.supply = Long.valueOf( (String) map.get("supply") );
		return item;
	}

}

package restoran.models;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.repos.BaseRepo;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import com.avaje.ebean.annotation.EnumValue;

@Entity
@Table(name="res_banket_profile")
@IndexType(name="res_banket_profile")
public class BanketProfile extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "banket-profile";

	@Column(name="banket_")
	public Long banket;

	@Column(name="profile_")//profileItem.class
	public Long profile;

	@Override
	public String displayName() {
		return profile().toString();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Banket banket() {
		return BaseRepo.findById(Banket.class, this.banket);
	}

	public Profile profile() {
		return BaseRepo.findById(Profile.class, this.profile);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("banket", banket.toString());
		map.put("profile", profile.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		BanketProfile item = new BanketProfile();
		item.id = Long.valueOf( (String) map.get("id") );
		item.banket = Long.valueOf( (String) map.get("banket") );
		item.profile = Long.valueOf( (String) map.get("profile") );
		return item;
	}

}

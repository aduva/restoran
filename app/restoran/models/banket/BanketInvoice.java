package restoran.models;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.repos.BaseRepo;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import com.avaje.ebean.annotation.EnumValue;

@Entity
@Table(name="res_banket_invoice")
@IndexType(name="res_banket_invoice")
public class BanketInvoice extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "banket-invoice";

	@Column(name="banket_")
	public Long banket;

	@Column(name="invoice_")//InvoiceItem.class
	public Long invoice;

	@Column(name="type_")
	public BanketInvoiceType banketInvoiceType;

	public enum BanketInvoiceType {
		@EnumValue("FOOD") FOOD,
		@EnumValue("SERVICE") SERVICE,
		@EnumValue("EQUIPMENT") EQUIPMENT
	}

	@Override
	public String displayName() {
		return invoice().title;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Banket banket() {
		return BaseRepo.findById(Banket.class, this.banket);
	}

	public Invoice invoice() {
		return BaseRepo.findById(Invoice.class, this.invoice);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("banket", banket.toString());
		map.put("invoice", invoice.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		BanketInvoice item = new BanketInvoice();
		item.id = Long.valueOf( (String) map.get("id") );
		item.banket = Long.valueOf( (String) map.get("banket") );
		item.invoice = Long.valueOf( (String) map.get("invoice") );
		return item;
	}

}

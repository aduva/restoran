package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.User;
import core.repos.BaseRepo;

import java.util.*;

import javax.persistence.*;

import play.i18n.Lang;
import play.i18n.Messages;

@Entity
@Table(name="res_contact_item")
@IndexType(name="res_contact_item")
@SequenceGenerator(name="res_contact_item_seq")
public class ContactItem extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "contact_item";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_contact_item_seq")
	public Long getId() {
		return id;
	}

	@Column(name="contact_")
	public Long contact;

	@Column(name="contact_type_")
	public ContactType contactType = ContactType.UNKNOWN;

	@Column(name="value_")
	public String value;

	@Column(name="main_")
	public boolean isMain;

	public enum ContactType {
		@EnumValue("UNKNOWN") UNKNOWN,
		@EnumValue("MOBILE") MOBILE,
		@EnumValue("TELEPHONE") TELEPHONE,
		@EnumValue("ADDRESS") ADDRESS,
		@EnumValue("EMAIL") EMAIL,
		@EnumValue("IIN") IIN
	}

	@Override
	public String displayName() {
		return null;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Contact contact() {
		if(contact == null)
			return null;
		return BaseRepo.findById(Contact.class, this.contact);
	}

	public static String value(Long contact, ContactType type) {
		ContactItem item = null;
		try {
			item = BaseRepo.find(ContactItem.class).where().eq("contact", contact).eq("contactType", type).eq("isMain", true).findUnique();
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(item == null)
			return null;
		return item.value;
	}

	public static List<ContactItem> values(Long contact, ContactType type) {
		List<ContactItem> items = null;
		try {
			items = BaseRepo.find(ContactItem.class).where().eq("contact", contact).eq("contactType", type).orderBy("isMain").findList();
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(items == null)
			return null;
		return items;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("contact", contact);

		map.put("contactType.title.ru", Messages.get(Lang.forCode("ru"),"contacts.contactType." + contactType));
		map.put("contactType.title.kk", Messages.get(Lang.forCode("kk"),"contacts.contactType." + contactType));
		map.put("contactType.title.en", Messages.get(Lang.forCode("en"),"contacts.contactType." + contactType));
		
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		ContactItem item = new ContactItem();
		item.id = Long.valueOf( (String) map.get("id") );

		item.contact = Long.valueOf( (String) map.get("contact") );
		item.contactType = ContactType.valueOf( (String) map.get("contactType") );
		item.value = (String) map.get("value");
		item.isMain = (Boolean) map.get("isMain");

		return item;
	}

}

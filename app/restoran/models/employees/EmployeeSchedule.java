package restoran.models;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;
import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.IndexUtils;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.repos.BaseRepo;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

import play.data.format.Formats;

@Entity
@Table(name="res_employee_schedule")
@IndexType(name="res_employee_schedule")
@SequenceGenerator(name="res_employee_schedule_seq")
public class EmployeeSchedule extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "employee_schedule";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_employee_schedule_seq")
	public Long getId() {
		return id;
	}

	@Column(name="employee_")
	public Long employee;

	@Column(name="date_")
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Calendar date;

	@Override
	public String displayName() {
		return null;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Employee employee() {
		return BaseRepo.findById(Employee.class, this.employee);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("employee", employee);
		map.put("date", date.getTime());
		
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		EmployeeSchedule item = new EmployeeSchedule();
		item.id = Long.valueOf( (String) map.get("id") );

		item.employee = Long.valueOf( (Integer) map.get("employee") );
		item.date = Calendar.getInstance();
		item.date.setTime( (Date) IndexUtils.convertValue(map.get("date"), Date.class) );

		return item;
	}

}

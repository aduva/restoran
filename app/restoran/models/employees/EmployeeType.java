package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="res_employee_type")
@IndexType(name="res_employee_type")
public class EmployeeType extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "employee-type";

	@Column(name="group_")
	public Long group;

	@Embedded
	public Title title;

	public String title(String lang) {
		return this.title.get(lang);
	}
	
	public String title() {
		return title(Langs.curLang());
	}

	@Override
	public String displayName() {
		return title.ru + "/" + title.kk + "/" + title.en;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = new HashMap();
		map.put("group", group.toString());
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		map.put("id", id.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		EmployeeType item = new EmployeeType();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.group = Long.valueOf( (String) map.get("group") );

		return item;
	}

}

package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.*;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.User;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

@Entity
@Table(name="res_profile")
@IndexType(name="res_profile")
@SequenceGenerator(name="res_profile_seq")
public class Profile extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "profile";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_profile_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="user_")
	public Long user;

	@Column(name="firstname_")
	public String firstname;

	@Column(name="lastname_")
	public String lastname;

	@Override
	public String displayName() {
		return name();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public User user() {
		if(user == null)
			return null;
		return BaseRepo.findById(User.class, this.user);
	}

	public String name() {
		return new StringBuffer().append(firstname).append(" ").append(lastname).toString();
	}

	public String toString() {
		return name();
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("firstname", firstname);
		map.put("lastname", lastname);
		map.put("user", user);
		map.put("group", group);
		
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Profile item = new Profile();
		item.id = Long.valueOf( (String) map.get("id") );

		item.firstname = (String) map.get("firstname");
		item.lastname = (String) map.get("lastname");

		item.user = Long.valueOf( (String) map.get("user") );
		item.group = Long.valueOf( (String) map.get("group") );

		return item;
	}

}

package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.*;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.User;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

@Entity
@Table(name="res_contact")
@IndexType(name="res_contact")
@SequenceGenerator(name="res_contact_seq")
public class Contact extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "contact";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_contact_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="profile_")
	public Long profile;

	@Transient
	public String name, mobile, telephone, address, iin;

	@Override
	public String displayName() {
		return profile().name();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Profile profile() {
		if(profile == null)
			return null;
		return BaseRepo.findById(Profile.class, this.profile);
	}

	public String toString() {
		if(profile == null)
			return null;
		return profile().name();
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("profile", profile);
		map.put("profile.name", toString());

		map.put("profile.mobile", ContactItem.value(id, ContactItem.ContactType.MOBILE));
      map.put("profile.telephone", ContactItem.value(id, ContactItem.ContactType.TELEPHONE));
      map.put("profile.address", ContactItem.value(id, ContactItem.ContactType.ADDRESS));
      map.put("profile.iin", ContactItem.value(id, ContactItem.ContactType.IIN));

		map.put("group", group);
		
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Contact item = new Contact();
		item.id = Long.valueOf( (String) map.get("id") );

		item.profile = Long.valueOf( (Integer) map.get("profile") );
		item.group = Long.valueOf( (Integer) map.get("group") );

		item.name = (String) map.get("profile.name");
		item.mobile = (String) map.get("profile.mobile");
		item.telephone = (String) map.get("profile.telephone");
		item.address = (String) map.get("profile.address");
		item.iin = (String) map.get("profile.iin");
		item.tags = (String) map.get("tags");

		return item;
	}

}

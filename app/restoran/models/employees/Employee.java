package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.*;

import core.controllers.Langs;
import core.repos.BaseRepo;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

@Entity
@Table(name="res_employee")
@IndexType(name="res_employee")
@SequenceGenerator(name="res_employee_seq")
public class Employee extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "employee";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_employee_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="employee_type_")
	public Long employeeType;

	@Column(name="profile_")
	public Long profile;

	@Column(name="salary_")
	public Integer salary;	

	@Column(name="display_schedule_")
	public boolean displaySchedule = false;

	@Override
	public String displayName() {
		return toString();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Profile profile() {
		return BaseRepo.findById(Profile.class, this.profile);
	}

	public EmployeeType employeeType() {
		return BaseRepo.findById(EmployeeType.class, this.employeeType);
	}

	public String toString() {
		return profile().name();
	}

	public String firstname() {
		return profile().firstname;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("employeeType", employeeType);
		map.put("salary", salary);
		map.put("group", group);
		map.put("profile", profile);
		map.put("profile.name", profile().name());
		map.put("employeeType.title.ru", employeeType().title.ru);
		map.put("employeeType.title.kk", employeeType().title.kk);
		map.put("employeeType.title.en", employeeType().title.en);
		
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Employee item = new Employee();
		item.id = Long.valueOf( (String) map.get("id") );

		item.employeeType = Long.valueOf( (Integer) map.get("employeeType") );
		item.profile = Long.valueOf( (Integer) map.get("profile") );
		item.salary = (Integer) map.get("salary");
		item.group = Long.valueOf( (Integer) map.get("group") );

		return item;
	}

}

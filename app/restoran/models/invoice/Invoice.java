package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.repos.BaseRepo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

@Entity
@Table(name="res_invoice")
@IndexType(name="res_invoice")
@SequenceGenerator(name="res_invoice_seq")
public class Invoice extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "invoice";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_invoice_seq")
	public Long getId() {
		return id;
	}

	@Column(name="title_")
	public String title;

	@Override
	public String displayName() {
		return title;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("title", title);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Invoice item = new Invoice();
		item.id = Long.valueOf( (String) map.get("id") );
		item.title = (String) map.get("title");
		return item;
	}

}

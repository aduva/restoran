package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.repos.BaseRepo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

@Entity
@Table(name="res_invoice_item")
@IndexType(name="res_invoice_item")
@SequenceGenerator(name="res_invoice_item_seq")
public class InvoiceItem extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "invoice";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_invoice_item_seq")
	public Long getId() {
		return id;
	}

	@Column(name="title_")
	public String title;

	@Column(name="invoice_")
	public Long invoice;

	@Column(name="amount_")
	public Integer amount = 0;

	@Column(name="closed_")
	public boolean isClosed;

	@Column(name="invoice_type_")
	public InvoiceType invoiceType = InvoiceType.EXPENSE;

	public enum InvoiceType {
		@EnumValue("INCOME")	INCOME,
		@EnumValue("EXPENSE") EXPENSE,
		@EnumValue("BILL")	BILL
	}

	@Override
	public String displayName() {
		return title;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Invoice invoice() {
		return BaseRepo.findById(Invoice.class, invoice);
	}

	@Transient
	public String invoiceTitle, creatorName;

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("invoice", invoice);
		map.put("amount", amount);
		map.put("title", title);
		map.put("isClosed", isClosed);
		map.put("invoiceType", invoiceType.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		InvoiceItem item = new InvoiceItem();
		item.id = Long.valueOf( (String) map.get("id") );
		item.invoice = Long.valueOf((Integer) map.get("invoice"));
		item.amount = (Integer) map.get("amount");
		item.isClosed = (Boolean) map.get("isClosed");
		item.title = (String) map.get("title");
		item.dateCreated = (Date) IndexUtils.convertValue(map.get("dateCreated"), Date.class);
		item.creator = Long.valueOf((Integer) map.get("creator"));

		item.invoiceType = InvoiceType.valueOf((String) map.get("invoiceType"));

		return item;
	}

}

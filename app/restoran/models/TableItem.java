package restoran.models;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.repos.BaseRepo;
import restoran.repos.RestoranRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import com.avaje.ebean.annotation.EnumValue;

@Entity
@Table(name="res_table_item")
@IndexType(name="res_table_item")
public class TableItem extends BaseModel {

	private static final long serialVersionUID = 1L;
	public static final String objectType = "table-item";

	@Column(name="group_")
	public Long group;

	@Column(name="people_amount_")
	public int peopleAmount;

	public enum Code {
		@EnumValue("DEFAULT")
		DEFAULT, 
		@EnumValue("WEDDING")
		WEDDING
	}

	@Column(name="type_")
	public Code code = Code.DEFAULT;

	@Embedded
	public Title title;

	public String title(String lang) {
		return this.title.get(lang);
	}
	
	public String title() {
		return title(Langs.curLang());
	}
	
	@Override
	public String displayName() {
		return title.ru + "/" + title.kk + "/" + title.en;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Group group() {
		return BaseRepo.findById(Group.class, this.group);
	}

	private static TableItem weddingTable = null;

	public static TableItem weddingTable() {
		if(weddingTable == null)
			weddingTable = RestoranRepo.findTableByCode(Code.WEDDING);
		return weddingTable;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("group", group.toString());
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		map.put("id", id.toString());
		map.put("peopleAmount", peopleAmount);
		map.put("code", code.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		TableItem item = new TableItem();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.group = Long.valueOf( (String) map.get("group") );
		item.peopleAmount = (Integer) map.get("peopleAmount");
		item.code = Code.valueOf((String) map.get("code"));

		return item;
	}
}

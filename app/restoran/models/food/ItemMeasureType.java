package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.repos.BaseRepo;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

import play.i18n.Lang;
import play.i18n.Messages;

@Entity
@Table(name="res_item_measure_type")
@IndexType(name="res_item_measure_type")
@SequenceGenerator(name="res_item_measure_type_seq")
public class ItemMeasureType extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "item_measure_type";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_item_measure_type_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="item_")
	public Long item;

	@Column(name="transform_ratio_")
	public double transformRatio;

	@Embedded
	public Title title;

	public void title(String value) {
		this.title.set(Langs.curLang(), value);
	}
	
	public String title() {
		return title.get(Langs.curLang());
	}

	@Override
	public String displayName() {
		return title();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);

		map.put("transformRatio", transformRatio);

		map.put("id", id.toString());
		map.put("item", item);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		ItemMeasureType item = new ItemMeasureType();
		item.id = Long.valueOf( (String) map.get("id") );
		item.item = Long.valueOf( (Integer) map.get("item") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.tags = (String) map.get("tags");
		item.transformRatio = (Double) map.get("transformRatio");

		return item;
	}

}

/*
CREATE TABLE res_item_measure_type
(
  id_ bigint NOT NULL,
  creator_ bigint,
  editor_ bigint,
  date_created_ timestamp without time zone,
  date_edited_ timestamp without time zone,
  disabled_ boolean,
  tags_ character varying(255),
  group_ bigint,
  item_ bigint,
  transform_ratio_ double precision,
  ru character varying(255),
  kk character varying(255),
  en character varying(255),
  
  CONSTRAINT pk_res_item_measure_type PRIMARY KEY (id_)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE res_item_measure_type
  OWNER TO postgres;

create sequence res_item_measure_type_seq;
*/
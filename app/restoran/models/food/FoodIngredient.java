package restoran.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;
import core.models.security.*;
import core.models.activity.*;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.annotation.PreDestroy;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import java.util.Map;
import java.util.HashMap;

@Entity
@Table(name="res_food_ingredient")
@IndexType(name="res_food_ingredient")
public class FoodIngredient extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "food_ingredient";

	@Column(name="food_")
	public Long food;

	@Column(name="ingredient_")
	public Long ingredient;

	@Column(name="amount_")
	public double amount;

	@Column(name="measure_type_")
	public Long measureType;

	@Override
	public String displayName() {
		return food().title() + " > " + ingredient().title();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Item ingredient() {
		return BaseRepo.findById(Item.class, ingredient);
	}

	public Food food() {
		return BaseRepo.findById(Food.class, food);
	}

	public ItemMeasureType measureType() {
		return BaseRepo.findById(ItemMeasureType.class, measureType);
	}

	// public double transformedAmount() {
	// 	return this.amount / ingredient().measureType().transformRatio;
	// }

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("food", food);
		map.put("ingredient", ingredient);
		map.put("measureType", measureType);
		map.put("ingredient.title.ru", ingredient().title.ru);
		map.put("ingredient.title.kk", ingredient().title.kk);
		map.put("ingredient.title.en", ingredient().title.en);
		map.put("id", id.toString());
		
		map.put("amount", amount);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		FoodIngredient item = new FoodIngredient();
		item.id = Long.valueOf( (String) map.get("id") );

		play.Logger.debug("\n");play.Logger.debug("\n");play.Logger.debug("\n");
		play.Logger.debug(">>>> " + map.get("measureType"));
		play.Logger.debug("\n");play.Logger.debug("\n");play.Logger.debug("\n");

		item.food = Long.valueOf( (Integer) map.get("food") );
		item.ingredient = Long.valueOf( (Integer) map.get("ingredient") );
		// try {
		// String mt = (String) map.get("measureType");
		if(map.get("measureType") == null)
			item.measureType = null;
		else
			item.measureType = Long.valueOf( (Integer) map.get("measureType") );
		// } catch(NullPointerException e) {
		// 	e.printStackTrace();
		// }
		item.amount = (Double) map.get("amount");

		return item;
	}

	@Override
	@PostPersist
	public void postPersist() {
		try {
			ActivityObject.create(this);
			Activity.Builder.build().actor(User.me().activityObject()).verb("create").subject(this.activityObject()).target(food().activityObject()).activity().save();
		} catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	@PostUpdate
	public void postUpdate() {
		Activity.Builder.build().actor(User.me().activityObject()).verb("edit").subject(this.activityObject()).target(food().activityObject()).activity().save();
	}

	@Override
	@PreDestroy
	public void preDestroy() {
		Activity.Builder.build().actor(User.me().activityObject()).verb("delete").subject(this.activityObject()).target(food().activityObject()).activity().save();
	}

}

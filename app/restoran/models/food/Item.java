package restoran.models;

import com.avaje.ebean.annotation.EnumValue;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Indexable;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.repos.BaseRepo;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;

import play.i18n.Lang;
import play.i18n.Messages;

@Entity
@Table(name="res_item")
@IndexType(name="res_item")
@SequenceGenerator(name="res_item_seq")
public class Item extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "item";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_item_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="measure_type_")
	public Long measureType;

	@Column(name="item_type_")
	public ItemType itemType = ItemType.UNKNOWN;

	public enum ItemType {
		@EnumValue("UNKNOWN") UNKNOWN,
		@EnumValue("INGREDIENT") INGREDIENT,
		@EnumValue("HOUSEHOLD") HOUSEHOLD
	}

	@Embedded
	public Title title;

	public void title(String value) {
		this.title.set(Langs.curLang(), value);
	}
	
	public String title() {
		return title.get(Langs.curLang());
	}

	public ItemMeasureType measureType() {
		if(measureType == null)
			return null;
		return BaseRepo.findById(ItemMeasureType.class, measureType);
	}

	@Override
	public String displayName() {
		return title();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		map.put("measureType", measureType);
		
		map.put("itemType", itemType);
		map.put("itemType.title.ru", Messages.get(Lang.forCode("ru"),"items.itemType." + itemType));
		map.put("itemType.title.kk", Messages.get(Lang.forCode("kk"),"items.itemType." + itemType));
		map.put("itemType.title.en", Messages.get(Lang.forCode("en"),"items.itemType." + itemType));

		map.put("id", id.toString());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Item item = new Item();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.tags = (String) map.get("tags");
		try {
			item.measureType = Long.valueOf((Integer) map.get("measureType"));
		} catch(Exception e) {
			e.printStackTrace();
		}
		item.itemType = ItemType.valueOf((String) map.get("itemType"));

		return item;
	}

}

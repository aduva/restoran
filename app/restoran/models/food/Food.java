package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.*;

import core.controllers.Langs;
import core.repos.BaseRepo;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

@Entity
@Table(name="res_food")
@IndexType(name="res_food")
@SequenceGenerator(name="res_food_seq")
public class Food extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "food";

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="res_food_seq")
	public Long getId() {
		return id;
	}

	@Column(name="group_")
	public Long group;

	@Column(name="food_type_")
	public Long foodType;

	@Column(name="cost_")
	public double cost;

	@Column(name="price_")
	public double price;

	@Column(name="weight_")
	public double weight;

	@Embedded
	public Title title;

	public String titleOf(String lang) {
		return this.title.get(lang);
	}

	public void title(String value) {
		this.title.set(Langs.curLang(), value);
	}
	
	public String title() {
		return titleOf(Langs.curLang());
	}

	@Override
	public String displayName() {
		return title();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public FoodType foodType() {
		return BaseRepo.findById(FoodType.class, this.foodType);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("foodType", foodType.toString());
		map.put("foodType.title.ru", foodType().title.ru);
		map.put("foodType.title.kk", foodType().title.kk);
		map.put("foodType.title.en", foodType().title.en);
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		map.put("id", id.toString());
		map.put("cost", cost);
		map.put("price", price);
		map.put("weight", weight);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Food item = new Food();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");
		item.foodType = Long.valueOf( (String) map.get("foodType") );
		item.cost = (Double) map.get("cost");
		item.price = (Double) map.get("price");
		item.weight = (Double) map.get("weight");

		return item;
	}

}

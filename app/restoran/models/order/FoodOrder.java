   package restoran.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;
import restoran.repos.RestoranRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;

@Entity
@Table(name="res_order_food")
@IndexType(name="res_order_food")
public class FoodOrder extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "food_order";

	@Column(name="order_")
	public Long order;

	@Column(name="food_")
	public Long food;

	@Column(name="food_type_")
	public Long foodType;

	@Column(name="portions_per_place_")
	public double portions;

	@Column(name="overall_")
	public double overall;

	@Column(name="parent_food_")
	public Long parent;

	@Column(name="price_")
	public double price;

	@Column(name="to_supply_")
	public boolean toSupply = true;

	@Column(name="invoice_item_")
	public Long invoiceItem;

	@Override
	public String displayName() {
		return "food: " + food + " of order: " + order;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Food food() {
		if(this.food == null)
			return null;
		return BaseRepo.findById(Food.class, this.food);
	}

	public Order order() {
		return BaseRepo.findById(Order.class, this.order);
	}

	public List<FoodOrder> childs() {
		return RestoranRepo.findFoodOrdersByParent(this.id);
	}

	public InvoiceItem invoiceItem() {
		return BaseRepo.findById(InvoiceItem.class, invoiceItem);
	}

}

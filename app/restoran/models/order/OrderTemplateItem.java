package restoran.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

@Entity
@Table(name="res_order_template_item")
@IndexType(name="res_order_template_item")
public class OrderTemplateItem extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "order-template-item";

	@Column(name="order_template_")
	public Long template;

	@Column(name="food_type_")
	public Long foodType;

	@Column(name="food_amount_")
	public int foodAmount;

	@Column(name="max_portions_")
	public int maxPortions;

	@Column(name="min_portions_")
	public double minPortions;

	@Override
	public String displayName() {
		return "order template item of template: " + template;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public FoodType foodType() {
		return BaseRepo.findById(FoodType.class, foodType);
	}

}

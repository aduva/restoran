package restoran.models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

@Entity
@Table(name="res_food_order_table_item")
@IndexType(name="res_food_order_table_item")
public class FoodOrderTableItem extends BaseModel {

	private static final long serialVersionUID = 1L;
	public static final String objectType = "order-table-item";

	@Column(name="order_")
	public Long order;

	@Column(name="order_table_item_")
	public Long orderTableItem;

	@Column(name="food_order_")
	public Long foodOrder;

	@Column(name="places_")
	public int places;

	@Column(name="portions_")
	public double portions;

	@Column(name="overall_")
	public double overall;
	
	@Override
	public String displayName() {
		return "order-table-item [" + this.id + "]";
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public FoodOrder foodOrder() {
		return BaseRepo.findById(FoodOrder.class, this.foodOrder);
	}

	public OrderTableItem orderTableItem() {
		return BaseRepo.findById(OrderTableItem.class, this.orderTableItem);
	}

	public Food food() {
		try {
			return foodOrder().food();
		} catch(Exception e) {}

		return null;
	}
}

package restoran.models;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

@Entity
@Table(name="res_order_template")
@IndexType(name="res_order_template")
public class OrderTemplate extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "order-template";

	@Column(name="service_percents_")
	public int servicePercents;

	@Embedded
	public Title title;

	public String titleOf(String lang) {
		return this.title.get(lang);
	}

	public void title(String value) {
		this.title.set(Langs.curLang(), value);
	}
	
	public String title() {
		return titleOf(Langs.curLang());
	}

	@Override
	public String displayName() {
		return "order template: " + id;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("title.ru", title.ru);
		map.put("title.kk", title.kk);
		map.put("title.en", title.en);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		OrderTemplate item = new OrderTemplate();
		item.id = Long.valueOf( (String) map.get("id") );

		item.title = new Title();
		item.title.ru = (String) map.get("title.ru");
		item.title.kk = (String) map.get("title.kk");
		item.title.en = (String) map.get("title.en");

		return item;
	}

}

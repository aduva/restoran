package restoran.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

@Entity
@Table(name="res_order_template_place")
@IndexType(name="res_order_template_place")
public class OrderTemplatePlace extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "order-template-place";

	@Column(name="order_template_")
	public Long template;

	@Column(name="food_type_")
	public Long foodType;

	@Column(name="location_table_item_")
	public Long locationTableItem;

	@Column(name="places_")
	public int places;

	@Override
	public String displayName() {
		return "order template place rule of template: " + template;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public FoodType foodType() {
		return BaseRepo.findById(FoodType.class, foodType);
	}

	public LocationTableItem locationTableItem() {
		return BaseRepo.findById(LocationTableItem.class, this.locationTableItem);
	}

	public boolean joined() {
		return locationTableItem().joined;
	}

	public String toString() {
		return this.id + " " + this.places;
	}

	public static OrderTemplatePlace findPlace(Long template, Long locationTableItem, Long foodType) {
		OrderTemplatePlace otp = BaseRepo.find(OrderTemplatePlace.class)
			.where().eq("template", template)
			.eq("locationTableItem", locationTableItem)
			.eq("foodType", foodType).findUnique();

		return otp;
	}

	public static int getPlace(Long template, Long locationTableItem, Long foodType) {
		OrderTemplatePlace otp = findPlace(template, locationTableItem, foodType);

		if(otp == null)
			return 0;
		return otp.places;
	}

}

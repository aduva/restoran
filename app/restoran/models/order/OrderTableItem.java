package restoran.models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.*;
import core.models.activity.*;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.annotation.PreDestroy;

@Entity
@Table(name="res_order_table_item")
@IndexType(name="res_order_table_item")
public class OrderTableItem extends BaseModel {

	private static final long serialVersionUID = 1L;
	public static final String objectType = "order_table_item";

	@Column(name="order_")
	public Long order;

	@Column(name="location_table_item_")
	public Long locationTableItem;

	@Column(name="table_item_")
	public Long tableItem;

	@Column(name="quantity_")
	public int quantity;

	@Column(name="people_amount_")
	public int peopleAmount;
	
	@Override
	public String displayName() {
		return tableItem().title() + " " + this.quantity + "x" + this.peopleAmount;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public LocationTableItem tableItem() {
		return BaseRepo.findById(LocationTableItem.class, this.locationTableItem);
	}

	public boolean joined() {
		return tableItem().joined;
	}

	public Banket banket() {
		Order item = BaseRepo.findById(Order.class, order);
		if(item == null)
			return null;
		return item.banket();
	}

	@Override
	@PostPersist
	public void postPersist() {
		try {
			ActivityObject.create(this);
			Activity.Builder.build().actor(User.me().activityObject()).verb("create").subject(this.activityObject()).target(banket().activityObject()).activity().save();
		} catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	@PostUpdate
	public void postUpdate() {
		Activity.Builder.build().actor(User.me().activityObject()).verb("edit").subject(this.activityObject()).target(banket().activityObject()).activity().save();
	}

	@Override
	@PreDestroy
	public void preDestroy() {
		Activity.Builder.build().actor(User.me().activityObject()).verb("delete").subject(this.activityObject()).target(banket().activityObject()).activity().save();
	}
}

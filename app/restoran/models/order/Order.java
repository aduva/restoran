package restoran.models;

import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;

import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;

import play.data.format.Formats;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import core.models.activity.*;
import core.models.security.User;

@Entity
@Table(name="res_order")
@IndexType(name="res_order")
public class Order extends BaseModel { //8778-666-55-87-Zhaslan
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "order";

	@Column(name="group_")
	public Long group;

	@Column(name="banket_")
	public Long banket;

	@Column(name="template_")
	public Long template;

	@Column(name="people_amount_")
	public int peopleAmount;

	@Column(name="unplaced_people_amount_")
	public int unplacedPeopleAmount;

	@Column(name="overall_payment_")
	public double overallPayment;

	@Column(name="supply_date_")
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Calendar supplyDate;

	@Column(name="finalized_")
	public boolean finalized;

	@Column(name="supply_approved_")
	public boolean approved;

	@Column(name="supply_")
	public Long supply;

	@Column(name="service_percents_")
	public int servicePercents;

	@Override
	public String displayName() {
		return "Калькуляция - " + banket().toString();
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public static Order create(Banket banket, OrderTemplate template) {
		Order order = new Order();
		order.banket = banket.id;
		order.group = banket.group;
		order.peopleAmount = banket.peopleAmount;
		order.unplacedPeopleAmount = banket.banketType().code == BanketType.Code.WEDDING ? order.peopleAmount - 4 : order.peopleAmount;
		order.template = template.id;

		DateTime supplyDate = new DateTime(banket.date).minusDays(2).withTimeAtStartOfDay();
		DateTime now = DateTime.now().withTimeAtStartOfDay();
		if(supplyDate.getMillis() < now.getMillis())
			supplyDate = now;

		order.supplyDate = Calendar.getInstance();
		order.supplyDate.setTimeInMillis(supplyDate.getMillis());

		order.save();
		return order;
	}

	public static Order update(Order order, Banket banket) {
		play.Logger.debug(">>>>>>> " + order.peopleAmount + "  " + banket.peopleAmount);
		order.peopleAmount = banket.peopleAmount;
		order.unplacedPeopleAmount = banket.banketType().code == BanketType.Code.WEDDING ? order.peopleAmount - 4 : order.peopleAmount;

		DateTime supplyDate = new DateTime(banket.date).minusDays(2).withTimeAtStartOfDay();
		DateTime now = DateTime.now().withTimeAtStartOfDay();
		if(supplyDate.getMillis() < now.getMillis())
			supplyDate = now;

		order.supplyDate = Calendar.getInstance();
		order.supplyDate.setTimeInMillis(supplyDate.getMillis());

		order.update();
		return order;
	}

	public Map toIndex() {
		Map map = super.toIndex();
		map.put("group", group.toString());
		map.put("banket", banket.toString());
		map.put("peopleAmount", peopleAmount);
		map.put("finalized", finalized);
		return map;
	}

	public Indexable fromIndex(Map map) {
		Order item = new Order();
		item.peopleAmount = (int) map.get("peopleAmount");
		item.finalized = (boolean) map.get("finalized");
		item.group = Long.valueOf( (String) map.get("group") );
		item.banket = Long.valueOf( (String) map.get("banket") );
		return null;
	}

	public Banket banket() {
		return BaseRepo.findById(Banket.class, banket);
	}

	public Supply supply() {
		return BaseRepo.findById(Supply.class, supply);
	}
	
	@Override
	@PostPersist
	public void postPersist() {
		try {
			ActivityObject.create(this);
			Activity.Builder.build().actor(User.me().activityObject()).verb("create").subject(this.activityObject()).target(banket().activityObject()).activity().save();
		} catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	@PostUpdate
	public void postUpdate() {
		Activity.Builder.build().actor(User.me().activityObject()).verb("edit").subject(this.activityObject()).target(banket().activityObject()).activity().save();
	}

}

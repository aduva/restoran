package restoran.models;

import java.util.Map;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

@Entity
@Table(name="res_location_table_item")
@IndexType(name="res_location_table_item")
public class LocationTableItem extends BaseModel {

	private static final long serialVersionUID = 1L;
	public static final String objectType = "location-table-item";

	@Column(name="location_")
	public Long location;

	@Column(name="table_item_")
	public Long tableItem;

	@Column(name="quantity_")
	public int quantity;

	@Column(name="joined_")
	public boolean joined;

	@Column(name="priority_")
	public int priority;

	@Transient
	public TableItem table;
	
	@Override
	public String displayName() {
		return "location-table-item [" + this.id + "]";
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Location location() {
		return BaseRepo.findById(Location.class, this.location);
	}

	public TableItem tableItem() {
		if(this.table == null)
			this.table = BaseRepo.findById(TableItem.class, this.tableItem);
		return table;
	}

	public String title() {
		return tableItem().title();
	}

	public int peopleAmount() {
		if(joined)
			return tableItem().peopleAmount * 2 - 2;

		return tableItem().peopleAmount;
	}

	@Override
	public Map toIndex() {
		table = tableItem();
		Map map = super.toIndex();
		map.put("location", location.toString());
		map.put("tableItem", tableItem.toString());
		map.put("tableItem.title.ru", table.title.ru);
		map.put("tableItem.title.kk", table.title.kk);
		map.put("tableItem.title.en", table.title.en);
		map.put("tableItem.peopleAmount", table.peopleAmount);
		map.put("id", id.toString());
		map.put("quantity", quantity);
		map.put("joined", joined);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		LocationTableItem item = new LocationTableItem();
		item.id = Long.valueOf( (String) map.get("id") );
		item.location = Long.valueOf( (String) map.get("location") );
		item.tableItem = Long.valueOf( (String) map.get("tableItem") );
		item.quantity = (Integer) map.get("quantity");

		item.joined = (boolean) map.get("joined");

		item.table = new TableItem();
		item.table.title = new Title();
		item.table.title.ru = (String) map.get("tableItem.title.ru");
		item.table.title.kk = (String) map.get("tableItem.title.kk");
		item.table.title.en = (String) map.get("tableItem.title.en");
		item.table.peopleAmount = (Integer) map.get("tableItem.peopleAmount");
		item.table.id = tableItem;

		return item;
	}
}

package restoran.models;

import javax.persistence.Entity;
import com.avaje.ebean.annotation.Sql;
import java.util.Date;

@Entity
@Sql
public class SupplyResult {

	public String supplyDate;
	public int amount;

	public String toString() {
		return supplyDate + " " + amount;
	}

}

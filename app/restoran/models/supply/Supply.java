package restoran.models;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.IndexUtils;

import core.models.BaseModel;
import core.repos.BaseRepo;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.data.format.Formats;
import static play.data.validation.Constraints.*;

@Entity
@Table(name="res_supply")
@IndexType(name="res_supply")
public class Supply extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "supply";

	@Column(name="date_")
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Calendar date;

	@Column(name="title_")
	public String title;

	@Override
	public String displayName() {
		return title;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("title", title);
		map.put("date", date.getTime());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		Supply item = new Supply();
		item.id = Long.valueOf( (String) map.get("id") );
		item.creator = Long.valueOf( (Integer) map.get("creator") );
		item.title = (String) map.get("title");
		item.date = Calendar.getInstance();
		item.date.setTime( (Date) IndexUtils.convertValue(map.get("date"), Date.class) );
		return item;
	}

}

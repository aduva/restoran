package restoran.models;

import javax.persistence.Entity;
import com.avaje.ebean.annotation.Sql;
import java.util.Date;
import core.repos.BaseRepo;
import play.data.format.Formats;

@Entity
@Sql
public class SupplyTemp {

	public Date date;
	public int count;

}

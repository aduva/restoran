package restoran.models;

import core.repos.BaseRepo;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import static play.data.validation.Constraints.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
// import com.avaje.ebean.annotation.Sql;

import core.models.BaseModel;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import play.data.format.Formats;

@Entity
@Table(name="res_supply_item")
@IndexType(name="res_supply_item")
public class SupplyItem extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "supply-item";

	@Column(name="item_")
	public Long item;

	@Column(name="supply_")
	public Long supply;

	@Column(name="amount_")
	public double amount;

	@Column(name="locked_")
	public double locked;

	@Column(name="date_")
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Calendar date;

	public Item item() {
		return BaseRepo.findById(Item.class, item);
	}

	public Supply supply() {
		return BaseRepo.findById(Supply.class, supply);
	}

	@Override
	public String displayName() {
		return item().title.ru;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("item", item);
		map.put("supply", supply);
		map.put("item.title.ru", item().title.ru);
		map.put("item.title.kk", item().title.kk);
		map.put("item.title.en", item().title.en);
		map.put("item.tags", item().tags);
		map.put("amount", amount);
		map.put("date", date.getTime());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		SupplyItem item = new SupplyItem();
		item.id = Long.valueOf( (String) map.get("id") );
		item.item = (Long) map.get("item");
		item.supply = (Long) map.get("supply");
		item.amount = (Double) map.get("amount");
		item.date = Calendar.getInstance();
		item.date.setTime( (Date) IndexUtils.convertValue(map.get("date"), Date.class) );
		return item;
	}

}

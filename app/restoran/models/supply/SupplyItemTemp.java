package restoran.models;

import javax.persistence.Entity;
import com.avaje.ebean.annotation.Sql;
import java.util.Date;
import core.repos.BaseRepo;

@Entity
@Sql
public class SupplyItemTemp {

	public String title;
	public Integer amount;
	public Long item;
	public Integer approvedAmount;
	public Boolean isDeleted;
	public Long supplyItem;

	public String description;

	public double locked;

	public Item item() {
		return BaseRepo.findById(Item.class, item);
	}

	public SupplyItem supplyItem() {
		return BaseRepo.findById(SupplyItem.class, supplyItem);
	}

}

package restoran.models;

import core.repos.BaseRepo;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import static play.data.validation.Constraints.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
// import com.avaje.ebean.annotation.Sql;

import core.models.BaseModel;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import play.data.format.Formats;

@Entity
@Table(name="res_supply_item_result")
@IndexType(name="res_supply_item_result")
public class SupplyItemResult extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "supply-item-result";

	@Column(name="item_")
	public Long item;

	@Column(name="date_")
	@Formats.DateTime(pattern="dd.MM.yyyy")
	public Calendar date;

	@Column(name="final_amount_")
	public double finalAmount;

	@Column(name="single_price_")
	public int singlePrice;

	@Column(name="sum_")
	public int sum;

	public Item item() {
		if(item == null)
			return null;
		return BaseRepo.findById(Item.class, item);
	}

	@Override
	public String displayName() {
		return item().title.ru;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("item", item);
		map.put("item.title.ru", item().title.ru);
		map.put("item.title.kk", item().title.kk);
		map.put("item.title.en", item().title.en);
		map.put("item.tags", item().tags);
		map.put("finalAmount", finalAmount);
		map.put("singlePrice", singlePrice);
		map.put("sum", sum);
		map.put("date", date.getTime());
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		SupplyItemResult item = new SupplyItemResult();
		item.id = Long.valueOf( (String) map.get("id") );
		item.item = (Long) map.get("item");
		item.finalAmount = (Double) map.get("finalAmount");
		item.singlePrice = Integer.parseInt((String) map.get("singlePrice"));
		item.sum = Integer.parseInt((String) map.get("sum"));
		item.date = Calendar.getInstance();
		item.date.setTime( (Date) IndexUtils.convertValue(map.get("date"), Date.class) );

		return item;
	}

}

package restoran.models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.controllers.Langs;
import core.models.BaseModel;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.Indexable;

import com.avaje.ebean.annotation.EnumValue;

@Entity
@Table(name="res_location")
@IndexType(name="res_location")
public class Location extends BaseModel {

	private static final long serialVersionUID = 1L;
	public static final String objectType = "location";

	@Column(name="group_")
	public Long group;

	@Column(name="people_amount_")
	public int peopleAmount;

	// @Column(name="code_")
	// public Code code;

	// public enum Code {
	// 	@EnumValue("FIRST") FIRST,
	// 	@EnumValue("SECOND") SECOND
	// }

	@Embedded
	public Title title;

	public String title(String lang) {
		return this.title.get(lang);
	}
	
	public String title() {
		return title(Langs.curLang());
	}
	
	@Override
	public String displayName() {
		return title.ru + "/" + title.kk + "/" + title.en;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Group group() {
		return BaseRepo.findById(Group.class, this.group);
	}
}

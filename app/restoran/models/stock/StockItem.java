package restoran.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import java.util.Map;
import java.util.HashMap;

@Entity
@Table(name="res_stock_item")
@IndexType(name="res_stock_item")
public class StockItem extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "stock_item";

	@Column(name="item_")
	public Long item;

	@Column(name="amount_")
	public double amount;

	@Column(name="locked_")
	public double locked;

	@Override
	public String displayName() {
		return null;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Item item() {
		return BaseRepo.findById(Item.class, item);
	}

	public static double availableAmountOf(Long itemId) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return 0;
		return s.amount - s.locked;
	}

	public static double amountOf(Long itemId) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return 0;
		return s.amount;
	}

	public static double lockedOf(Long itemId) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return 0;
		return s.locked;
	}

	public static void lock(Long itemId, double lockAmount) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return;

		if(s.amount < lockAmount)
			lockAmount = s.amount;
		s.locked += lockAmount;
		s.update();
	}

	public static void unlock(Long itemId, double unlockAmount) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return;

		if(s.locked < unlockAmount)
			unlockAmount = s.locked;
		s.locked -= unlockAmount;
		s.update();
	}

	public static void minusLock(Long itemId, double unlockAmount) {
		StockItem s = BaseRepo.find(StockItem.class).where().eq("item", itemId).findUnique();
		if(s == null)
			return;

		if(s.locked < unlockAmount)
			unlockAmount = s.locked;
		s.locked -= unlockAmount;

		if(s.amount < unlockAmount)
			unlockAmount = s.amount;
		s.amount -= unlockAmount;
		
		s.update();	
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("item", item);
		map.put("item.title.ru", item().title.ru);
		map.put("item.title.kk", item().title.kk);
		map.put("item.title.en", item().title.en);
		map.put("id", id.toString());
		
		map.put("amount", amount);
		map.put("locked", locked);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		StockItem item = new StockItem();
		item.id = Long.valueOf( (String) map.get("id") );

		item.item = Long.valueOf( (Integer) map.get("item") );
		item.amount = (Double) map.get("amount");
		item.locked = (Double) map.get("locked");

		return item;
	}

}

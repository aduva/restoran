package restoran.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import core.models.BaseModel;
import core.repos.BaseRepo;

import com.github.aduva.elasticsearch.Indexable;
import com.github.aduva.elasticsearch.IndexUtils;
import com.github.aduva.elasticsearch.annotations.IndexType;
import com.github.aduva.elasticsearch.Elastic;

import java.util.Map;
import java.util.HashMap;
import java.util.Date;

@Entity
@Table(name="res_stock_item_takeout")
@IndexType(name="res_stock_item_takeout")
public class StockItemTakeOut extends BaseModel {
	
	private static final long serialVersionUID = 1L;
	public static final String objectType = "stock-item-takeout";

	@Column(name="item_")
	public Long item;

	@Column(name="amount_")
	public double amount;

	@Override
	public String displayName() {
		return null;
	}

	@Override
	public String objectType() {
		return objectType;
	}

	public Item item() {
		return BaseRepo.findById(Item.class, item);
	}

	@Override
	public Map toIndex() {
		Map map = super.toIndex();
		map.put("item", item);
		map.put("item.title.ru", item().title.ru);
		map.put("item.title.kk", item().title.kk);
		map.put("item.title.en", item().title.en);
		map.put("id", id.toString());
		map.put("amount", amount);
		return map;
	}

	@Override
	public Indexable fromIndex(Map map) {
		StockItemTakeOut item = new StockItemTakeOut();
		item.id = Long.valueOf( (String) map.get("id") );

		item.item = Long.valueOf( (Integer) map.get("item") );
		item.creator = Long.valueOf( (Integer) map.get("creator") );
		item.amount = (Double) map.get("amount");
		item.dateCreated = (Date) IndexUtils.convertValue(map.get("dateCreated"), Date.class);

		return item;
	}

}

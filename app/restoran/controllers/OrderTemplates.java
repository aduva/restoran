package restoran.controllers;

import restoran.repos.*;
import core.repos.UserRepo;
import core.repos.BaseRepo;
import core.models.security.User;
import core.models.security.Group;
import core.models.multilingual.Title;
import core.models.activity.*;

import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.ArrayNode;

import play.mvc.Call;
import play.Routes;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Controller;
import play.mvc.Result;
import play.i18n.Messages;
import play.data.DynamicForm;
import play.data.Form;

import restoran.views.html.management.order.*;
import java.util.*;
import restoran.models.*;
import core.utils.Page;

import com.github.aduva.elasticsearch.*;
import play.mvc.With;

@With(SessionController.class)
public class OrderTemplates extends Controller {

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.OrderTemplates.view(groupname, id);
   }

   public static Result index(String groupname, int page, int pageSize) {
      return ok(index.render(groupname, page-1, pageSize));
   }

   public static Result view(String groupname, Long id) {
      OrderTemplate item = BaseRepo.findById(OrderTemplate.class, id);
      if(item == null)
         return redirect(restoran.controllers.routes.OrderTemplates.index(groupname, 1, 10));

      IndexResults<Activity> activities = null;
      try {
         activities = ActivityRepo.findAllByObject(item.activityObject().id, 1, 20);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(activities != null)
         return ok(view.render(groupname, item, activities.results));

      return ok(view.render(groupname, item, null));
   }

   public static Result viewRules(String groupname, Long id) {
      OrderTemplate item = BaseRepo.findById(OrderTemplate.class, id);
      if(item == null)
         return redirect(restoran.controllers.routes.OrderTemplates.index(groupname, 1, 10));

      List<OrderTemplatePlace> places = BaseRepo.find(OrderTemplatePlace.class).where().eq("template", id).orderBy("locationTableItem").findList();

      return ok(rules.render(groupname, item, places));
   }

   public static Result updatePlace(String groupname, Long id, Long table, Long foodType) {
      OrderTemplatePlace place = OrderTemplatePlace.findPlace(id, table, foodType);
      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("value").toString().isEmpty())
         return badRequest();

      Integer value = Integer.parseInt(form.get("value"));

      play.Logger.debug(">>>>>> " + place + " " + value);

      if(place == null) {
         place = new OrderTemplatePlace();
         place.template = id;
         place.locationTableItem = table;
         place.foodType = foodType;
         place.places = value;
         place.save();
      } else {
         place.places = value;
         place.update();
      }

      return ok();
   }

   public static Result editTitle(String groupname, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      OrderTemplate item = BaseRepo.findById(OrderTemplate.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      String title = form.get("value").toString();

      item.title(title);
      item.update();

      return ok();  
   }

   public static Result add(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("ru").toString().isEmpty())
         return badRequest();

      OrderTemplate item = new OrderTemplate();
      item.title = new Title();
      item.title.ru = form.get("ru").toString();
      item.save();

      ObjectNode n = Json.newObject();
      n.put("title", item.title());
      n.put("id", item.id);

      return ok(n);
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      IndexResults<OrderTemplate> result = RestoranRepo.filterOrderTemplates(q, displayStart, displaySize, sortCol, sortDirection);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(OrderTemplate f : result.results) {
         ObjectNode o = Json.newObject();
         o.put("DT_RowId", f.id);
         o.put("0", "<a href=\"" + VIEW(groupname, f.id) + "\">" + f.title() + "</a>");
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

   public static class PlaceForm {
      public Long locationTableItem;
      public List<Integer> foodType;
   }
}

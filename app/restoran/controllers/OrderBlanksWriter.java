package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.joda.time.DateTime;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;

import java.io.File;
import jxl.*; 
import jxl.write.*; 

public class OrderBlanksWriter {
	public static void write(Long orderId) {
		try {
			WritableWorkbook workbook = Workbook.createWorkbook(new File("output.xls"));
			WritableSheet sheet = workbook.createSheet("First Sheet", 0);

			int titleCol = 1, titleRow = 1;

			Label label = new Label(1, 1, "A label record"); 
			sheet.addCell(label); 

			jxl.write.Number number = new jxl.write.Number(3, 4, 3.1459); 
			sheet.addCell(number);

			workbook.write(); 
			workbook.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
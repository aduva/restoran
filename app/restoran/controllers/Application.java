package restoran.controllers;

import restoran.repos.RestoranRepo;
import core.repos.UserRepo;
import core.models.security.User;
import core.models.security.Group;
import core.controllers.auth.Signin;
import core.controllers.UserManager;

import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.ArrayNode;

import play.Routes;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import play.i18n.Messages;

import restoran.views.html.index;
import restoran.views.html.signin.storedLogin;
import restoran.views.html.signin.unstoredLogin;

import java.util.*;
import restoran.models.*;

import com.github.aduva.elasticsearch.*;

public class Application extends Controller {
  
	public static Result index(String group) {
      if(!UserManager.isUserAuthenticated())
         return login();
	   return ok(index.render(group));
	}

   public static Result login() {
      User u = UserManager.getStoredUser();

      if(u == null)
         return ok(unstoredLogin.render());

      return ok(storedLogin.render(u));
   }

   public static Result loginAnotherUser() {
      UserManager.clearCurrentUser();
      return redirect( restoran.controllers.routes.Application.login() );
   }

   public static Result reindex() {
      restoran.InitialData.reindex();
      return redirect("/sazsyrnai");
   }

	public static Result data(String group) {
		
		ObjectNode n = Json.newObject();
		n.put("peopleAmount", 300);
		play.Logger.debug(">>>>>>> " + 300);
   	return ok(n);
   }

   public static List<Location> locations() {
   	List<Location> list = RestoranRepo.findUserLocations(User.me().id);

   	return list;
   }

   public static Map<String, String> banketTypes() {
      List<BanketType> list = RestoranRepo.findUserBanketTypes(User.me().id);

      if(list == null)
         return null;

      Map<String, String> map = new LinkedHashMap<String, String>(list.size());
      for(BanketType t : list) {
         map.put(t.id.toString(), t.title());
      }

      return map;
   }

   public static Map<String, String> groups() {
   	List<Group> list = UserRepo.findUserGroups(User.me().id);

   	if(list == null)
   		return null;

   	Map<String, String> map = new LinkedHashMap<String, String>(list.size());
   	for(Group g : list) {
   		map.put(g.id.toString(), g.title());
   	}

   	return map;
   }

   public static List<Group> groupsList() {
      return UserRepo.findUserGroups(User.me().id);
   }

   @With(SessionController.class)
   public static Result tables(String group, String callback, String q, int pageSize, int page, Long location) {
      IndexResults<LocationTableItem> results = RestoranRepo.tablesByTitle(q, location == 0 ? null : location, page, pageSize);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(LocationTableItem f : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", f.id);
         o.put("title", f.table.title() + " " + Messages.get("tables.table_for_amount", f.peopleAmount()));
         o.put("quantity", f.quantity);
         o.put("location", f.location().title());
         o.put("peopleAmount", f.peopleAmount());
         arr.add(o);
      }

      n.put("more", results.pageCurrent != results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   public static Result javascriptRoutes() {
      response().setContentType("text/javascript");
      return ok(Routes.javascriptRouter("jsRoutes",
         restoran.controllers.routes.javascript.Orders.addFood(),
         restoran.controllers.routes.javascript.Orders.supplyFood(),
         restoran.controllers.routes.javascript.Orders.deleteFood(),
         restoran.controllers.routes.javascript.Orders.addTableItem(),
         restoran.controllers.routes.javascript.Orders.deleteTable(),
         restoran.controllers.routes.javascript.Orders.addPayment(),
         restoran.controllers.routes.javascript.Orders.recalcTables(),
         restoran.controllers.routes.javascript.Orders.overallPersonSumGet(),

         restoran.controllers.routes.javascript.Foods.addIngredient(),
         restoran.controllers.routes.javascript.Foods.foodIngredientClick(),
         restoran.controllers.routes.javascript.Foods.deleteIngredient(),
         restoran.controllers.routes.javascript.Foods.editIngredient(),
         restoran.controllers.routes.javascript.Foods.add(),

         restoran.controllers.routes.javascript.Items.add(),
         restoran.controllers.routes.javascript.Items.addMeasureType(),
         restoran.controllers.routes.javascript.Items.deleteMeasureType(),

         restoran.controllers.routes.javascript.OrderTemplates.add(),

         restoran.controllers.routes.javascript.Invoices.add(),
         restoran.controllers.routes.javascript.Invoices.addItem(),

         restoran.controllers.routes.javascript.Supplies.block(),
         restoran.controllers.routes.javascript.Supplies.unblock(),
         restoran.controllers.routes.javascript.Supplies.addSupplyItem(),
         restoran.controllers.routes.javascript.Supplies.deleteSupplyItem(),
         restoran.controllers.routes.javascript.Supplies.finalAmount(),
         restoran.controllers.routes.javascript.Supplies.itemClick(),

         restoran.controllers.routes.javascript.Employees.add(),
         restoran.controllers.routes.javascript.Employees.toggleSchedule(),

         restoran.controllers.routes.javascript.Contacts.add(),
         
         restoran.controllers.routes.javascript.Stock.update(),
         restoran.controllers.routes.javascript.Stock.itemClick()
      ));
   }
}

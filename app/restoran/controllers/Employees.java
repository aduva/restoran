package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.joda.time.DateTime;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.employees.*;

import play.data.format.Formats;
import static play.data.validation.Constraints.*;
import play.mvc.With;

@With(SessionController.class)
public class Employees extends Controller {

   public static final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
   public static final DecimalFormat df = new DecimalFormat("#.#");
   public static final Form<EmpForm> form = new Form(EmpForm.class);

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Employees.view(groupname, id);
   }

   public static Result index(String groupname, int page, int pageSize) {
      return ok(index.render(groupname, page-1, pageSize));
   }

   public static Result view(String groupname, Long id) {
      Employee item = BaseRepo.findById(Employee.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Employees.index(groupname, 1, 20));

      return ok(view.render(groupname, item, null));
   }

   public static Result schedule(String groupname) {
      return ok(schedule.render(groupname));
   }

   public static Result viewContacts(String groupname, Long id) {
      return ok();
   }

   public static Result add(String groupname) {
      Form<EmpForm> f = form.bindFromRequest();
      play.Logger.debug(f.toString());

      if(form.hasErrors())
         return badRequest();

      EmpForm emp = f.get();

      Profile profile = new Profile();
      profile.firstname = emp.firstname;
      profile.lastname = emp.lastname;
      profile.group = 1L;
      profile.save();

      Contact contact = new Contact();
      contact.profile = profile.id;
      contact.group = 1L;
      contact.save();

      ContactItem mobile = new ContactItem();
      mobile.contactType = ContactItem.ContactType.MOBILE;
      mobile.value = emp.mobile;
      mobile.contact = contact.id;
      mobile.isMain = true;
      mobile.save();

      ContactItem telephone = new ContactItem();
      telephone.contactType = ContactItem.ContactType.TELEPHONE;
      telephone.value = emp.telephone;
      telephone.contact = contact.id;
      telephone.isMain = true;
      telephone.save();

      ContactItem address = new ContactItem();
      address.contactType = ContactItem.ContactType.ADDRESS;
      address.value = emp.address;
      address.contact = contact.id;
      address.isMain = true;
      address.save();

      Employee employee = new Employee();
      employee.employeeType = emp.employeeType;
      employee.group = 1L;
      employee.profile = profile.id;
      employee.save();

      contact.index();

      return ok();
   }

   public static Result editSalary(String groupname, Long id) {
      Employee item = BaseRepo.findById(Employee.class, id);

      if(item == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("value").toString().isEmpty())
         return badRequest();

      item.salary = Integer.parseInt(form.get("value").toString());
      item.update();

      return ok();
   }

   public static Result editEmployeeType(String groupname, Long id) {
      Employee item = BaseRepo.findById(Employee.class, id);

      if(item == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("value").toString().isEmpty())
         return badRequest();

      item.employeeType = Long.valueOf(form.get("value").toString());
      item.update();

      return ok();
   }

   public static Result editDisplaySchedule(String groupname, Long id) {
      Employee item = BaseRepo.findById(Employee.class, id);

      if(item == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("value").toString().isEmpty())
         return badRequest();

      item.displaySchedule = Boolean.valueOf(form.get("value").toString());
      item.update();

      return ok();
   }

   public static Result updateDisabled(String groupname, Long id) {
      Employee item = BaseRepo.findById(Employee.class, id);
      if(item == null)
         return redirect(restoran.controllers.routes.Employees.index(groupname, 1, 20));

      boolean disabled = !item.isDisabled;

      if(disabled) {
         List<EmployeeSchedule> sheds = BaseRepo.find(EmployeeSchedule.class).where().eq("employee", id).findList();

         if(sheds != null) {
            for(EmployeeSchedule shed : sheds) {
               shed.isDisabled = disabled;
               shed.update();
            }
         }
      }

      item.isDisabled = disabled;
      item.update();
      return redirect(VIEW(groupname, id));
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         IndexResults<Employee> result = RestoranRepo.filterEmployees(q, displayStart, displaySize, sortCol, sortDirection);
         for(Employee f : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", f.id);
            o.put("0", "<a href='" + VIEW(groupname, f.id) + "'>" + f.toString() + "</a>");
            o.put("1", f.employeeType().title());

            Contact contact = BaseRepo.find(Contact.class).where().eq("profile", f.profile).findUnique();
            if(contact == null) {
               o.put("2", ContactItem.value(contact.id, null));
               o.put("3", ContactItem.value(contact.id, null));
               o.put("4", ContactItem.value(contact.id, null));
               arr.add(o);
               continue;                  
            }

            o.put("2", ContactItem.value(contact.id, ContactItem.ContactType.MOBILE));
            o.put("3", ContactItem.value(contact.id, ContactItem.ContactType.TELEPHONE));
            o.put("4", ContactItem.value(contact.id, ContactItem.ContactType.ADDRESS));
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }

   public static Result employeeTypes(String groupname, String callback, String q, int pageSize, int page) {
      IndexResults<EmployeeType> results = RestoranRepo.employeeTypesByTitle(q, page, pageSize);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(EmployeeType f : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", f.id);
         o.put("title", f.title());
         arr.add(o);
      }

      n.put("more", results.pageCurrent != results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   public static List<Employee> findAll() {
      return BaseRepo.findAll(Employee.class);
   }

   public static List<Employee> findSchedulers() {
      return BaseRepo.find(Employee.class).where().eq("displaySchedule", true).eq("isDisabled", false).orderBy("employeeType, id").findList();
   }

   public static Result getSchedules(String groupname, Long _start, Long _end) {
      Date start = new Date(_start);
      Date end = new Date(_end);

      play.Logger.debug(">>>>>>> " + start + " " + end);

      IndexResults<EmployeeSchedule> results = RestoranRepo.schedulesByDate(start, end, null);

      IndexResults<Banket> bankets = RestoranRepo.banketsByDate(start, end);

      if(results == null && bankets == null)
         return ok();

      ArrayNode arr = toJson(results, bankets);

      return ok(arr);
   }

   public static Result toggleSchedule(String groupname) {
      Form<ScheduleForm> f = new Form(ScheduleForm.class).bindFromRequest();

      if(f.hasErrors())
         return badRequest();

      ScheduleForm form = f.get();
      EmployeeSchedule es = BaseRepo.find(EmployeeSchedule.class).where().eq("employee", form.employee).eq("date", form.date).findUnique();

      ObjectNode n = Json.newObject();
      n.put("result", "success");

      if(es == null) {
         es = new EmployeeSchedule();
         es.employee = form.employee;
         es.date = Calendar.getInstance();
         es.date.setTime(form.date);
         es.save();
         n.put("action", "add");
      } else {
         if(es.isDisabled) {
            n.put("action", "add");
            es.isDisabled = false;
         } else {
            es.isDisabled = true;
            n.put("action", "delete");   
         }
         
         es.update();
         
      }

      DateTime date = new DateTime(form.date);
      DateTime start = date.withDayOfMonth(1);
      DateTime end = date.plusMonths(1).withDayOfMonth(1).withTimeAtStartOfDay().minusSeconds(1);
      play.Logger.debug(start.toString() + " " + end.toString());

      int workDays = BaseRepo.find(EmployeeSchedule.class)
         .where().eq("employee", form.employee)
         .eq("isDisabled", false)
         .between("date", start.toDate(), end.toDate()).findRowCount();

      n.put("workDays", workDays);

      return ok(n);
   }

   private static ArrayNode toJson(IndexResults<EmployeeSchedule> results, IndexResults<Banket> bankets) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("schedules");

      if(results != null) {
         for(EmployeeSchedule b : results.results) {
            ObjectNode o = Json.newObject();
            o.put("id", b.id);
            o.put("emp", b.employee);
            o.put("date", formatter.format(b.date.getTime()));
            arr.add(o);
         }
      }

      if(bankets != null) {
         for(Banket b : bankets.results) {
            ObjectNode o = Json.newObject();
            o.put("banket", true);
            o.put("date", formatter.format(b.date.getTime()));
            o.put("banketDisabled", b.isDisabled);
            arr.add(o);
         }
      }

      if(results == null && bankets == null)
         return null;

      return arr;
   }

   public static class ScheduleForm {
      @Required public Long employee;
      @Required @Formats.DateTime(pattern="dd.MM.yyyy") public Date date;
   }

   public static class EmpForm {
      @Required public String mobile;
      public String telephone;
      @Required public String address;
      @Required public String firstname;
      @Required public String lastname;
      @Required public Long employeeType;
   }
}

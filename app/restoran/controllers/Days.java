package restoran.controllers;

import restoran.repos.RestoranRepo;
import core.repos.BaseRepo;
import core.models.security.User;
import core.models.security.Group;

import org.codehaus.jackson.node.ObjectNode;

import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.i18n.Messages;

import java.util.*;
import java.text.*;
import restoran.models.*;

import restoran.views.html.days.*;

import org.joda.time.DateTime;
import com.avaje.ebean.*;
import javax.persistence.Entity;
import com.avaje.ebean.annotation.Sql;

import play.mvc.With;

@With(SessionController.class)
public class Days extends Controller {
	public static Result index(String group) {
		return ok(index.render(group));
	}
}
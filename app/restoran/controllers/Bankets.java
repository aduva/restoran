package restoran.controllers;

import restoran.repos.*;
import core.repos.BaseRepo;
import com.avaje.ebean.Page;
import core.models.activity.*;

import be.objectify.deadbolt.java.actions.Dynamic;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Call;
import play.mvc.With;

import restoran.models.*;
import restoran.repos.RestoranRepo;

import restoran.views.html.bankets.add;
import restoran.views.html.bankets.edit;
import restoran.views.html.bankets.view;
import restoran.views.html.bankets.index;
import restoran.views.html.bankets.contacts.addContact;
import restoran.views.html.bankets.contacts.selectContact;

import com.github.aduva.elasticsearch.IndexResults;

import static play.data.validation.Constraints.*;

@With(SessionController.class)
public class Bankets extends Controller {
	
	public static final Form<Banket> form = new Form<Banket>(Banket.class);
	public static final Form<ContactForm> contactForm = new Form<ContactForm>(ContactForm.class);
	public static final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");

	public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Bankets.view(groupname, id);
   }

   public static Call ADDCONTACTS(String groupname, Long id) {
      return restoran.controllers.routes.Bankets.contacts(groupname, id);
   }

	public static Result index(String group) {
		return ok(index.render(group));
	}

	public static List<Banket> all() {
		return BaseRepo.findAll(Banket.class);
	}

	public static List<Banket> today() {
		DateTime today = new DateTime().toDateMidnight().toDateTime();
		DateTime tomorrow = today.plusDays(1);

		IndexResults<Banket> results = RestoranRepo.banketsByDate(today.toDate(), tomorrow.toDate());
		if(results == null)
			return null;

		return results.results;
	}

	public static List<Banket> week() {
		DateTime today = new DateTime().toDateMidnight().toDateTime();
		DateTime sunday = today.dayOfWeek().withMaximumValue().withHourOfDay(23).withMinuteOfHour(59);

		play.Logger.debug(">>>>>>> " + sunday.toDate().toString());

		IndexResults<Banket> results = RestoranRepo.banketsByDate(today.toDate(), sunday.toDate());

		if(results == null)
			return null;

		return results.results;
	}

	public static List<Banket> month() {
		DateTime start = new DateTime().dayOfMonth().withMinimumValue();
		DateTime end = new DateTime().dayOfMonth().withMaximumValue();

		IndexResults<Banket> results = RestoranRepo.banketsByDate(start.toDate(), end.toDate());
		if(results == null)
			return null;

		return results.results;
	}

	public static Result search(String group, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      IndexResults<Banket> result = RestoranRepo.filterBankets(q, displayStart, displaySize, sortCol, sortDirection);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(Banket b : result.results) {
         ObjectNode o = Json.newObject();
         o.put("0", "<input type=\"checkbox\"> <a href=" + VIEW(group,b.id).url() + ">" + b.banketType().title() + "</a>");
         o.put("1", formatter.format(b.date.getTime()));
         o.put("2", b.peopleAmount);
         o.put("3", b.location().title());
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

	public static Result getBankets(String group, Long _start, Long _end) {
		play.Logger.debug(">> " + _start + " " + _end);
		Date start = new Date(_start);
		Date end = new Date(_end);

		IndexResults<Banket> results = RestoranRepo.banketsByDate(start, end);

		if(results == null)
			return ok();

      ArrayNode arr = toJson(group, results);

		return ok(arr);
	}

	private static ArrayNode toJson(String group, IndexResults<Banket> results) {
		if(results == null)
			return null;

		ObjectNode n = Json.newObject();
		ArrayNode arr = n.putArray("bankets");

      for(Banket b : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", b.id);
         o.put("title", b.banketType().title());
         o.put("peopleAmount", b.peopleAmount);
         o.put("location", b.location);
         o.put("startDate", formatter.format(b.date.getTime()));
         o.put("endDate", formatter.format(b.date.getTime()));
         o.put("url", VIEW(group, b.id).url());
         arr.add(o);
      }

      return arr;
	}

	// @Dynamic(value="administrator")
	public static Result add(String group, String date) {
		Form<Banket> f = form;
		if(date != null && !date.isEmpty()){
			Banket b = new Banket();
			b.date_ = date;
			b.peopleAmount = 100;
			b.time = new DateTime().now().withTime(18, 0, 0, 0).toDate();
			f = form.fill(b);
		}
		return ok(add.render(group,f));
	}

	public static Result contacts(String group, Long id) {
		Banket item = BaseRepo.findById(Banket.class, id);
		if(item == null)
			return redirect("/" + group);

		BanketProfile bprofile = BaseRepo.find(BanketProfile.class).where().eq("banket", id).findUnique();
		Profile profile = null;
		Contact contact = null;

		if(bprofile != null) {
			profile = bprofile.profile();
			contact = BaseRepo.find(Contact.class).where().eq("profile", bprofile.profile).findUnique();
		}


		return ok(selectContact.render(group, item, profile, contact));
	}

	public static Result selectContact(String group, Long id) {
		DynamicForm form = Form.form().bindFromRequest();

		play.Logger.debug(">>>>> " + form.toString());

      if(form.get("contact").toString().isEmpty())
         return redirect( restoran.controllers.routes.Bankets.contacts(group, id) );

      Long contactId = Long.valueOf(form.get("contact").toString());

      Contact contact = BaseRepo.findById(Contact.class, contactId);

      if(contact != null) {
      	Profile profile = contact.profile();
      	if(profile != null) {
      		BanketProfile bprofile = BaseRepo.find(BanketProfile.class).where().eq("banket", id).findUnique();
      		if(bprofile == null) {
      			bprofile = new BanketProfile();
      			bprofile.banket = id;
      			bprofile.profile = profile.id;
      			bprofile.save();
      		} else {
      			bprofile.profile = profile.id;
      			bprofile.update();
      		}
      	}
      }

		return redirect( Bankets.VIEW(group, id) );
	}

	public static Result addContact(String groupname, Long id) {
		Banket item = BaseRepo.findById(Banket.class, id);
		if(item == null)
			return redirect("/" + groupname);

		return ok(addContact.render(groupname, item, contactForm));
	}

	public static Result saveContact(String groupname, Long id) {
		Banket item = BaseRepo.findById(Banket.class, id);
		if(item == null)
			return redirect("/" + groupname);

		Form<ContactForm> f = contactForm.bindFromRequest();
		play.Logger.debug(f.toString());

		if(f.hasErrors()) {
			return badRequest(addContact.render(groupname, item, f));
		}

		ContactForm value = f.get();

		ContactItem iinItem = BaseRepo.find(ContactItem.class).where().eq("value", value.iin).eq("contactType", ContactItem.ContactType.IIN).findUnique();
		if(iinItem != null) {
			f.reject("iin", Messages.get("contacts.iin.unique.error"));
			return badRequest(addContact.render(groupname, item, f));
		}

		Contact contact = Contacts.saveContact(value.firstname, value.lastname, value.mobile, value.telephone, null, value.iin, null);

		if(contact == null)
			return badRequest(addContact.render(groupname, item, f));

		Profile profile = contact.profile();
   	if(profile != null) {
   		BanketProfile bprofile = BaseRepo.find(BanketProfile.class).where().eq("banket", id).findUnique();
   		if(bprofile == null) {
   			bprofile = new BanketProfile();
   			bprofile.banket = id;
   			bprofile.profile = profile.id;
   			bprofile.save();
   		} else {
   			bprofile.profile = profile.id;
   			bprofile.update();
   		}
   	}

		return redirect(VIEW(groupname, id));
	}

	public static Result searchContacts(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
      	String[] fields = { "profile.name", "profile.iin", "profile.mobile", "profile.telephone", "profile.address" };
         IndexResults<Contact> result = RestoranRepo.filterContacts(q, fields, displayStart, displaySize, sortCol, sortDirection);
         for(Contact contact : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", contact.id);
            o.put("0", contact.name);
            o.put("1", contact.iin);
            o.put("2", contact.mobile);
            o.put("3", contact.telephone);
            o.put("4", contact.address);
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }
	

	public static Result view(String group, Long id) {
		Banket item = BaseRepo.findById(Banket.class, id);
		if(item == null)
			return redirect("/" + group);

		Order order = RestoranRepo.findBanketOrder(id);

		List<Activity> activities = ActivityRepo.findAllByObject(item.activityObject().id);
		// play.Logger.debug(">>>> " + activities.getList().size());

		BanketProfile bprofile = BaseRepo.find(BanketProfile.class).where().eq("banket", id).findUnique();
		Contact contact = null;
		Profile profile = null;
		if(bprofile != null) {
			profile = bprofile.profile();
			contact = BaseRepo.find(Contact.class).where().eq("profile", bprofile.profile).findUnique();
		}

		return ok(view.render(group,item, order, activities, profile, contact));
	}

	public static Result edit(String group, Long id) {
		Banket item = BaseRepo.findById(Banket.class, id);
		if(item == null)
			return redirect("/" + group);
		
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");

		item.date_ = dateFormat.format(item.date.getTime());
		item.time = item.date.getTime();
		Form<Banket> f = form.fill(item);


		play.Logger.debug(f.toString());

		return ok(edit.render(group,f));
	}

	public static Result save(String group) {
		Form<Banket> f = form.bindFromRequest();
		
		play.Logger.debug(">>>> " + f.toString());

		if(f.hasErrors()) {
		 	return badRequest(add.render(group,f));   
		}

		Banket item = f.get();

		Calendar time = Calendar.getInstance();
		time.setTime(item.time);

		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

		Calendar date = Calendar.getInstance();
		try {
			date.setTime(df.parse(item.date_));
		} catch(Exception e) {
			e.printStackTrace();
			date.setTime(item.date_submit);
		}
		date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));

		play.Logger.debug(">>>>> " + date.getTime());

		item.date = date;
		item.save();

	   return redirect(ADDCONTACTS(group,item.id));
	}

	public static Result update(String group, Long id) {
		Form<Banket> f = form.bindFromRequest();

		Banket item = BaseRepo.findById(Banket.class, id);
		
		play.Logger.debug(">>>> " + f.toString());

		if(f.hasErrors() || item == null) {
		 	return badRequest(edit.render(group,f));
		}

		Banket formItem = f.get();

		// if(formItem.id != item.id)
		// 	return badRequest(edit.render(f));		

		Calendar time = Calendar.getInstance();
		time.setTime(formItem.time);

		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

		Calendar date = Calendar.getInstance();
		try {
			date.setTime(df.parse(formItem.date_));
		} catch(Exception e) {
			e.printStackTrace();
			date.setTime(formItem.date_submit);
		}
		date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));

		play.Logger.debug(">>>>> " + date.getTime());

		boolean update = false, recalc = true;

		if(!item.date.equals(date)) {
			item.date = date;
			update = true;
		}

		if(!item.title.equals(formItem.title)) {
			item.title = formItem.title;
			update = true;
		}

		if(item.location != formItem.location) {
			item.location = formItem.location;
			update = true;
			recalc = true;
		}

		if(item.peopleAmount != formItem.peopleAmount) {
			item.peopleAmount = formItem.peopleAmount;
			update = true;
			recalc = true;
		}

		if(item.banketType != formItem.banketType) {
			item.banketType = formItem.banketType;
			update = true;
			recalc = true;
		}

		if(update) {
			item.update();
		}

		// if(recalc) {
		// 	Orders.recalc(item);
		// }

	   return redirect(VIEW(group,item.id));
	}

	public static class ContactForm {
      @Required public List<String> mobile;
      public String telephone;
      @Required public String firstname;
      @Required public String lastname;
      @MinLength(value=12) @MaxLength(value=12) public String iin;
   }

}

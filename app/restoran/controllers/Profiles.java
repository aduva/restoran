package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.joda.time.DateTime;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;
// import restoran.views.html.management.employees.*;

import play.data.format.Formats;
import static play.data.validation.Constraints.*;
import play.mvc.With;

@With(SessionController.class)
public class Profiles extends Controller {
	public static Result editFirstname(Long id) {
		Profile item = BaseRepo.findById(Profile.class, id);
		if(item == null)
			return badRequest();

		DynamicForm f = Form.form().bindFromRequest();
		play.Logger.debug(f.toString());

		String value = f.get("value").toString();
		item.firstname = value;
		item.update();

		return ok();
	}

	public static Result editLastname(Long id) {
		Profile item = BaseRepo.findById(Profile.class, id);
		if(item == null)
			return badRequest();

		DynamicForm f = Form.form().bindFromRequest();
		play.Logger.debug(f.toString());

		String value = f.get("value").toString();
		item.lastname = value;
		item.update();

		return ok();
	}
}
package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.joda.time.DateTime;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.contacts.*;

import play.data.format.Formats;
import static play.data.validation.Constraints.*;

import play.mvc.With;

@With(SessionController.class)
public class Contacts extends Controller {

   public static final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
   public static final DecimalFormat df = new DecimalFormat("#.#");
   public static final Form<ContactForm> form = new Form(ContactForm.class);

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Contacts.view(groupname, id);
   }

   public static Result index(String groupname, int page, int pageSize) {
      return ok(index.render(groupname, page-1, pageSize));
   }

   public static Result view(String groupname, Long id) {
      Contact item = BaseRepo.findById(Contact.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Contacts.index(groupname, 1, 20));

      return ok();
   }

   public static Result add(String groupname) {
      DynamicForm dform = Form.form().bindFromRequest();
      play.Logger.debug(dform.toString());

      Form<ContactForm> f = form.bindFromRequest();
      play.Logger.debug(f.toString());

      if(form.hasErrors())
         return badRequest();

      ContactForm item = f.get();

      for(String mob : item.mobile) {
         play.Logger.debug(">>> mobs " + item.mobile.size() + " " + mob);
      }

      Contact contact = saveContact(item.firstname, item.lastname, item.mobile, item.telephone, item.address, null, item.tags);

      return ok();
   }

   public static Contact saveContact(String itemFirstname, String itemLastname, List<String> itemMobile, String itemTelephone, String itemAddress, String itemIin, String itemTags) {
      Profile profile = null;
      Contact contact = null;
      if(itemFirstname != null && !itemFirstname.isEmpty()) {
         profile = new Profile();
         profile.firstname = itemFirstname;
         profile.lastname = itemLastname;
         profile.group = 1L;
         profile.save();
      }

      if(profile == null)
         return null;

      contact = new Contact();
      contact.profile = profile.id;
      contact.group = 1L;
      if(itemTags != null && !itemTags.isEmpty()) {
         contact.tags = itemTags;
      }
      contact.save();

      if(itemMobile != null && itemMobile.size() > 0) {
         for(int i = 0; i < itemMobile.size(); i++) {
            if(itemMobile.get(i) == null || itemMobile.get(i).isEmpty())
               continue;
            ContactItem mobile = new ContactItem();
            mobile.contactType = ContactItem.ContactType.MOBILE;
            mobile.value = itemMobile.get(i);
            mobile.contact = contact.id;
            
            if(i == 0)
               mobile.isMain = true;
            
            mobile.save();
         }
      }

      if(itemTelephone != null && !itemTelephone.isEmpty()) {
         ContactItem telephone = new ContactItem();
         telephone.contactType = ContactItem.ContactType.TELEPHONE;
         telephone.value = itemTelephone;
         telephone.contact = contact.id;
         telephone.isMain = true;
         telephone.save();
      }

      if(itemAddress != null && !itemAddress.isEmpty()) {
         ContactItem address = new ContactItem();
         address.contactType = ContactItem.ContactType.ADDRESS;
         address.value = itemAddress;
         address.contact = contact.id;
         address.isMain = true;
         address.save();
      }

      if(itemIin != null && !itemIin.isEmpty()) {
         ContactItem iin = new ContactItem();
         iin.contactType = ContactItem.ContactType.IIN;
         iin.value = itemIin;
         iin.contact = contact.id;
         iin.isMain = true;
         iin.save();
      }

      contact.index();

      return contact;
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         String[] fields = { "profile.name", "tags", "profile.mobile", "profile.telephone", "profile.address" };
         IndexResults<Contact> result = RestoranRepo.filterContacts(q, fields, displayStart, displaySize, sortCol, sortDirection);
         for(Contact contact : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", contact.id);
            o.put("0", contact.name);
            o.put("1", contact.tags);
            o.put("2", contact.mobile);
            o.put("3", contact.telephone);
            o.put("4", contact.address);
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }

   public static class ContactForm {
      @Required public List<String> mobile;
      public String telephone;
      public String address;
      @Required public String firstname;
      public String lastname;
      public String tags;
   }
}

package restoran.controllers;

import com.avaje.ebean.Ebean;
import com.github.aduva.elasticsearch.*;

import core.controllers.Langs;
import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.util.*;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import org.apache.commons.lang3.text.WordUtils;

import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import play.Routes;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.foods.*;
import play.mvc.With;

@With(SessionController.class)
public class Foods extends Controller {

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Foods.view(groupname, id);
   }

   public static Result index(String groupname, int page, int pageSize) {
      return ok(index.render(groupname, page-1, pageSize));
   }

   public static Result view(String groupname, Long id) {
      Food item = BaseRepo.findById(Food.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Foods.index(groupname ,1, 10));

      IndexResults<Activity> activities = null;
      try {
         activities = ActivityRepo.findAllByObject(item.activityObject().id, 1, 20);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(activities != null)
         return ok(view.render(groupname, item, activities.results));

      return ok(view.render(groupname, item, null));
   }

   public static Result viewIngredients(String groupname, Long id) {
      Food item = BaseRepo.findById(Food.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Foods.index(groupname, 1, 10));

      IndexResults<FoodIngredient> list = null;

      try { 
         list = FoodsRepo.foodIngredients(id);
      } catch(Exception e) {
         e.printStackTrace();
         return ok(ingredients.render(groupname, item, null));
      }

      return ok(ingredients.render(groupname, item, list.results));
   }

   public static Result foodIngredientClick(String groupname, Long id) {
      FoodIngredient item = BaseRepo.findById(FoodIngredient.class, id);
      if(item == null)
         return badRequest();

      return ok(itemInfoForm.render(item));
   }

   public static List<FoodIngredient> foodIngredients(Long id) {
      List<FoodIngredient> res = null;

      try { 
         IndexResults<FoodIngredient> list = FoodsRepo.foodIngredients(id);
         res = list.results;
      } catch(Exception e) {
         e.printStackTrace();
      }

      return res;
   }

   public static Result ingrsprint(String groupname) {
      List<Food> foods = BaseRepo.findAll(Food.class);

      return ok(ingrsprint.render(groupname, foods));
   }

   public static Result updateDisabled(String groupname, Long id) {
      Food item = BaseRepo.findById(Food.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Foods.index(groupname, 1, 10));

      boolean disabled = !item.isDisabled;
      item.isDisabled = disabled;
      item.update();

      return redirect(VIEW(groupname, id));
   }

   public static Result add(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("ru").toString().isEmpty() || form.get("foodType").toString().isEmpty() || form.get("price").toString().isEmpty()) {
         play.Logger.debug(">>>>>>>>>>>> badrequest ");
         return badRequest();
      }

      Food item = new Food();
      item.title = new Title();
      item.title.ru = WordUtils.capitalizeFully(form.get("ru").toString());
      item.weight = Double.valueOf(form.get("weight").toString());
      item.price = Double.valueOf(form.get("price").toString());
      item.foodType = Long.valueOf(form.get("foodType").toString());
      item.save();

      return ok("hello");
   }

   public static Result editPrice(String groupname, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Food item = BaseRepo.findById(Food.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      item.price = Double.valueOf(form.get("value"));

      item.update();

      return ok();  
   }

   public static Result editTitle(String groupname, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Food item = BaseRepo.findById(Food.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      String title = WordUtils.capitalizeFully(form.get("value").toString());

      item.title(title);
      item.update();

      return ok();  
   }

   public static Result editType(String groupname, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Food item = BaseRepo.findById(Food.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      Long foodTypeId = Long.valueOf(form.get("value").toString());

      if(item.foodType == foodTypeId)
         return ok();

      FoodType foodType = BaseRepo.findById(FoodType.class, foodTypeId);
      if(foodType == null)
         return badRequest();

      item.foodType = foodTypeId;
      item.update();

      return ok();  
   }

   public static Result editWeight(String groupname, Long id) {
      DynamicForm form = Form.form().bindFromRequest();
      Food item = BaseRepo.findById(Food.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      item.weight = Double.valueOf(form.get("value").toString());
      item.update();

      return ok();  
   }

   public static Result editIngredientAmount(String groupname, Long foodId, Long foodIngredientId) {
      DynamicForm form = Form.form().bindFromRequest();

      play.Logger.debug(">>>>>>> "  +form.toString());

      FoodIngredient item = BaseRepo.findById(FoodIngredient.class, foodIngredientId);

      if(form.get("value").toString().isEmpty() || item == null || !item.food.equals(foodId))
         return badRequest();

      item.amount = Double.valueOf( form.get("value").toString() );
      item.update();

      return ok();
   }

   public static Result editIngredient(String groupname, Long foodId) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(">>>>>>> " + form.toString());

      if(form.get("amount").toString().isEmpty() || form.get("item").toString().isEmpty() || form.get("measureType").toString().isEmpty())
         return badRequest();

      Long itemId = Long.valueOf(form.get("item").toString());
      Long measureTypeId = Long.valueOf(form.get("measureType").toString());

      ItemMeasureType measureType = BaseRepo.findById(ItemMeasureType.class, measureTypeId);
      FoodIngredient item = BaseRepo.findById(FoodIngredient.class, itemId);

      play.Logger.debug("??>>>>>> " + item + " " + measureType + " " + item.food + " " + foodId);

      if(measureType == null || item == null || !item.food.equals(foodId))
         return badRequest();

      item.amount = Double.valueOf( form.get("amount").toString() );
      item.amount *= measureType.transformRatio;
      item.measureType = measureType.id;

      item.update();

      ObjectNode n = Json.newObject();
      n.put("tpl", itemInfoForm.render(item).toString());
      n.put("amount", item.amount/measureType.transformRatio);
      n.put("id", item.id);
      n.put("measureType", item.measureType().title());

      return ok(n);  
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      IndexResults<Food> result = FoodsRepo.filterFoods(q, displayStart, displaySize, sortCol, sortDirection);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(Food f : result.results) {
         ObjectNode o = Json.newObject();
         o.put("DT_RowId", f.id);
         if(f.isDisabled) {
            o.put("0", "<a href=\"" + VIEW(groupname, f.id) + "\"><strike>" + f.title() + "</strike></a>");
         } else {
            o.put("0", "<a href=\"" + VIEW(groupname, f.id) + "\">" + f.title() + "</a>");
         }
         o.put("1", f.foodType().title());
         o.put("2", f.price);
         o.put("3", f.weight);
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

   public static Result ingredients(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q, Long food) {
      IndexResults<FoodIngredient> result = FoodsRepo.filterFoodIngredients(q, displayStart, displaySize, sortCol, sortDirection, food);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(FoodIngredient f : result.results) {
         ObjectNode o = Json.newObject();
         o.put("DT_RowId", f.id);
         o.put("0", "<a href=\"#\">" + f.ingredient().title() + "</a>");
         o.put("1", f.amount);
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

   public static Result addIngredient(String groupname, Long foodId) {
      Food food = BaseRepo.findById(Food.class, foodId);
      
      if(food == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();

      play.Logger.debug(">>>>>> " + form.toString());

      if(form.get("ingredient").toString().isEmpty() || form.get("amount").toString().isEmpty() || form.get("measureType").toString().isEmpty())
         return badRequest();

      Long ingredientId = Long.valueOf(form.get("ingredient").toString());
      Item ingredient = BaseRepo.findById(Item.class, ingredientId);

      Long measureTypeId = Long.valueOf(form.get("measureType").toString());
      ItemMeasureType measureType = BaseRepo.findById(ItemMeasureType.class, measureTypeId);

      if(ingredient == null || measureType == null)
         return badRequest();

      FoodIngredient fi = BaseRepo.find(FoodIngredient.class).where().eq("food", foodId).eq("ingredient", ingredientId).findUnique();
      if(fi != null) {
         return badRequest();
      }
   
      fi = new FoodIngredient();
      fi.food = foodId;
      fi.ingredient = ingredientId;
      fi.amount = Double.valueOf(form.get("amount").toString());
      fi.amount *= measureType.transformRatio;
      fi.measureType = measureTypeId;
      fi.save();
      
      int size = BaseRepo.find(FoodIngredient.class).where().eq("food", foodId).findRowCount();
      ObjectNode n = Json.newObject();
      n.put("title", fi.ingredient().title());
      n.put("id", fi.id);
      n.put("idx", size+1);
      n.put("amount", fi.amount/measureType.transformRatio);
      n.put("measureType", measureType.title());

      return ok(n);
   }

   public static Result deleteIngredient(String groupname, Long foodIngredientId) {

      FoodIngredient fi = BaseRepo.findById(FoodIngredient.class, foodIngredientId);
      fi.delete();

      return ok();
   }
 
   public static Result foodsByType(String groupname, String callback, String q, int pageSize, int page, Long foodType) {
      IndexResults<Food> results = FoodsRepo.foodsByTitle(q, false, foodType, page, pageSize);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(Food f : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", f.id);
         o.put("title", f.title());
         arr.add(o);
      }

      n.put("more", results.pageCurrent != results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   public static Result foods(String groupname, String callback, String q, int pageSize, int page) {
      IndexResults<Food> results = FoodsRepo.foodsByTitle(q, false, page, pageSize);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(Food f : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", f.id);
         o.put("title", f.title());
         o.put("foodType", f.foodType().title());

         OrderTemplateItem tmpl = RestoranRepo.findTemplateItemByFoodType(f.foodType);
         if(tmpl != null) {
            o.put("maxPortions", tmpl.maxPortions);
            o.put("minPortions", tmpl.minPortions);
            o.put("tmpl", "yes");
         } else {
            o.put("tmpl", "no");
         }

         arr.add(o);
      }

      n.put("more", results.pageCurrent != results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   public static Result foodTypes(String groupname, String callback, String q, int pageSize, int page) {
      IndexResults<FoodType> results = FoodsRepo.foodTypesByTitle(q, page, pageSize);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(FoodType f : results.results) {
         ObjectNode o = Json.newObject();
         o.put("id", f.id);
         o.put("title", f.title());
         arr.add(o);
      }

      n.put("more", results.pageCurrent != results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   public static List<FoodType> foodTypes(String groupname) {
      IndexResults<FoodType> results = FoodsRepo.foodTypesByTitle(null, 0, 0);
      return results.results;
   }
}

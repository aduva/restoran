package restoran.controllers;

import core.controllers.UserManager;
import core.models.security.User;

import play.*;
import play.cache.*;
import play.i18n.*;
import play.mvc.*;
import play.mvc.Http.*;

import java.util.Date;

public class SessionController extends Action.Simple {
    
	public Result call(Http.Context ctx) throws Throwable {
		User user = UserManager.getStoredUser();

		if(Play.isProd()) {
			if(user != null) {
				StringBuffer buffer = new StringBuffer().append(user.id.toString()).append(":session");

				String session = (String) Cache.get(buffer.toString());
				if(session != null)
					Cache.set(buffer.toString(), user.id.toString(), 40 * 60 * 1);
				else {
					UserManager.unauthCurrentUser();
					ctx.flash().put("warning", Messages.get("session.expired"));
					return redirect( "/" );
				}
			}
		}

		return delegate.call(ctx);
	}

	public static void startSession(User user) {
		StringBuffer buffer = new StringBuffer().append(user.id.toString()).append(":session");
		Cache.set(buffer.toString(), user.id.toString(), 40 * 60 * 1);
	}
   
}
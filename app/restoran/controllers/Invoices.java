package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.invoices.*;
import play.mvc.With;

import org.apache.commons.lang3.text.WordUtils;

@With(SessionController.class)
public class Invoices extends Controller {

   public static final DecimalFormat df = new DecimalFormat("#.#");

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Invoices.view(groupname, id, 1, 10);
   }

   public static Result index(String groupname, int page, int pageSize) {
      return ok(index.render(groupname, page-1, pageSize));
   }

   public static Result lists(String groupname, int page, int pageSize) {
      return ok(lists.render(groupname, page-1, pageSize));
   }

   public static Result view(String groupname, Long id, int page, int pageSize) {
      Invoice item = BaseRepo.findById(Invoice.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Invoices.index(groupname, 1, 10));

      return ok(view.render(groupname, item, page-1, pageSize));
   }

   public static Result add(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("title").toString().isEmpty())
         return badRequest();

      Invoice item = new Invoice();
      item.title = WordUtils.capitalizeFully(form.get("title").toString());
      item.save();

      return ok();
   }

   public static Result addItem(String groupname, Long id) {
      Invoice item = BaseRepo.findById(Invoice.class, id);

      ObjectNode n = Json.newObject();
      n.put("result", "error");

      if(item == null)
         return ok(n);

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("title").toString().isEmpty() || form.get("amount").toString().isEmpty())
         return ok(n);

      InvoiceItem invoiceItem = new InvoiceItem();
      invoiceItem.title = WordUtils.capitalizeFully(form.get("title").toString());
      invoiceItem.amount = Integer.parseInt(form.get("amount").toString());
      invoiceItem.invoice = id;
      invoiceItem.save();

      n.put("result", "success");
      return ok(n);
   }

   public static Result editTitle(String groupname, Long id) {
      Invoice item = BaseRepo.findById(Invoice.class, id);

      if(item == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("value").toString().isEmpty())
         return badRequest();

      item.title = WordUtils.capitalizeFully(form.get("value").toString());
      item.update();
      return ok();
   }

   public static Result facetTitles(String groupname, String callback, String q, int pageSize, int page) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      try {
         IndexResults<InvoiceItem> results = RestoranRepo.facetInvoiceTitles(q);

         List<Map<String, String>> facets = RestoranRepo.getFacets(results);

         int size = facets.size() <= pageSize ? facets.size() : pageSize;

         play.Logger.debug(">>>>>> " + facets.size() + " " + ((page-1)*size) + " " + (page+size-1));

         if(facets.size() == 0) {
            ObjectNode o = Json.newObject();
            o.put("id", WordUtils.capitalizeFully(q));
            o.put("title", WordUtils.capitalizeFully(q));
            arr.add(o);
         } else {
            for(int i = (page-1) * size; i < page+size-1; i++) {
               ObjectNode o = Json.newObject();
               o.put("id", WordUtils.capitalizeFully(facets.get(i).get("term")));
               o.put("title", WordUtils.capitalizeFully(facets.get(i).get("term")));
               arr.add(o);
            }
         }
         n.put("more", facets.size() > page+pageSize);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("more", false);
      }

      return ok(Jsonp.jsonp(callback, n));
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         IndexResults<InvoiceItem> result = RestoranRepo.filterInvoiceItems(q, null, false, displayStart, displaySize, sortCol, sortDirection);
         for(InvoiceItem f : result.results) {

            String inv = new StringBuffer().append("invoices.invoiceType.").append(f.invoiceType.toString()).toString();

            ObjectNode o = Json.newObject();
            o.put("DT_RowId", f.id);
            o.put("0", f.id);
            o.put("1", f.title);
            o.put("2", new StringBuffer().append(f.amount).append(" (").append(Messages.get(inv)).append(")").toString());
            o.put("3", f.invoice() != null ? f.invoice().title : "");
            o.put("4", f.creator().toString());
            o.put("5", "<span class='date'>" + f.dateCreated.getTime() + "</span>");
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }

   public static Result searchLists(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         IndexResults<Invoice> result = RestoranRepo.filterInvoices(q, false, displayStart, displaySize, sortCol, sortDirection);
         for(Invoice f : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", f.id);
            o.put("0", "<a href=\"" + VIEW(groupname, f.id) + "\">" + f.title + "</a>");
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }

   public static Result searchList(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q, Long id) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         IndexResults<InvoiceItem> result = RestoranRepo.filterInvoiceItems(q, id, false, displayStart, displaySize, sortCol, sortDirection);
         for(InvoiceItem f : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", f.id);
            o.put("0", f.id);
            o.put("1", f.title);
            o.put("2", f.amount);
            o.put("3",  f.creator().toString());
            o.put("4", "<span class='date'>" + f.dateCreated.getTime() + "</span>");
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }
}

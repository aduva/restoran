package restoran.controllers;

import restoran.repos.RestoranRepo;
import core.repos.BaseRepo;
import core.models.security.User;
import core.models.security.Group;

import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.ArrayNode;

import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import play.i18n.Messages;
import play.data.format.Formats;

import java.util.*;
import java.text.*;
import restoran.models.*;
import restoran.views.html.management.supplies.*;

import com.avaje.ebean.*;
import org.joda.time.DateTime;
import com.github.aduva.elasticsearch.*;
import play.mvc.With;

@With(SessionController.class)
public class Supplies extends Controller {

   public static final DecimalFormat df = new DecimalFormat("#.#");
   public static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

   public static Result index(String groupname) {
      return ok(index.render(groupname));
   }

   public static Result lists(String groupname, int page, int pageSize) {
      return ok(lists.render(groupname, page-1, pageSize));
   }

   public static Call VIEW(String groupname, Long id) {
      return restoran.controllers.routes.Supplies.view(groupname, id);
   }

   public static Call LISTS(String groupname) {
      return restoran.controllers.routes.Supplies.lists(groupname, 1, 10);
   }

   public static Call ITEM(String groupname, Long id, String time) {
      return restoran.controllers.routes.Supplies.itemClick(groupname, id, time);
   }

   public static Result view(String groupname, Long id) {
      Supply item = BaseRepo.findById(Supply.class, id);
      if(item == null)
         return redirect( LISTS(groupname) );

      List<SupplyItem> items = BaseRepo.find(SupplyItem.class).where().eq("supply", item.id).findList();

      Order ord = BaseRepo.find(Order.class).where().eq("supply", id).findUnique();

      return ok(view.render(groupname, item, items, (ord == null)));
   }

   public static Result viewByDate(String groupname, String time) {
      Date date = null;
      try {
         date = dateFormat.parse(time);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(date == null)
         return redirect( restoran.controllers.routes.Supplies.index(groupname) );

      String sql = "select i.item_ as item, sum(i.amount_) as amount, sum(i.locked_) as locked from res_supply_item i " +
                   "join res_supply s on i.supply_ = s.id_ and i.disabled_ = false and s.disabled_ = false and s.date_ = '" + time.replace("/", "-") + "' group by item_ order by item_";
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      List<SupplyItemTemp> list = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql).findList();

      return ok(viewByDate.render(groupname, date, list));
   }

   public static Result previewByDate(String groupname, String time) {
      Date date = null;
      try {
         date = dateFormat.parse(time);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(date == null)
         return redirect( restoran.controllers.routes.Supplies.index(groupname) );

      String sql = "select i.item_ as item, sum(i.amount_) as amount, sum(i.locked_) as locked from res_supply_item i " +
                   "join res_supply s on i.supply_ = s.id_ and i.disabled_ = false and s.disabled_ = false and s.date_ = '" + time.replace("/", "-") + "' group by item_ order by item_";
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      List<SupplyItemTemp> list = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql).findList();
      List<Supply> supps = BaseRepo.find(Supply.class).where().eq("date", date).eq("isDisabled", false).findList();

      return ok(previewByDate.render(groupname, date, list, supps));
   }

   public static Result itemClick(String groupname, Long id, String time) {
      Date date = null;
      try {
         date = dateFormat.parse(time);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(date == null)
         return badRequest("bad");

      SupplyItemTemp item = itemClick(id, time);

      return ok(itemInfo.render(item, date));
   }

   private static SupplyItemTemp itemClick(Long id, String time) {
      String sql = "select i.item_ as item, sum(i.amount_) as amount, sum(i.locked_) as locked from res_supply_item i " +
                   "join res_supply s on i.supply_ = s.id_ and i.disabled_ = false and s.disabled_ = false and s.date_ = '" + time.replace("/", "-") + 
                   "' where i.item_ = " + id +
                   " group by item_ order by item_";
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      SupplyItemTemp item = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql).findUnique();
      return item;
   }

   public static Result searchLists(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         IndexResults<Supply> result = RestoranRepo.filterSupplyLists(q, displayStart, displaySize, sortCol, sortDirection);

         for(Supply item : result.results) {
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", item.id);
            o.put("1", "<a href=\"" + VIEW(groupname, item.id) + "\">" + item.title + "</a>");
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            o.put("0", "<a href=\"" + VIEW(groupname, item.id) + "\" class='date'>" + df.format(item.date.getTime()) + "</a>");
            o.put("2", item.creator().toString());
            arr.add(o);
         }

         n.put("iTotalRecords", result.totalCount);
         n.put("iTotalDisplayRecords", result.totalCount);
      } catch(Exception e) {
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
      }

      return ok(n);
   }

   public static Result add(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("title").toString().isEmpty() || form.get("date_submit").toString().isEmpty())
         return redirect( LISTS(groupname) );

      DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

      Calendar date = Calendar.getInstance();
      try {
         date.setTime(df.parse(form.get("date_submit").toString()));
      } catch(Exception e) {
         e.printStackTrace();
         return redirect( LISTS(groupname) );
      }

      Supply item = new Supply();
      item.date = date;
      item.title = form.get("title").toString();
      item.save();

      return redirect( restoran.controllers.routes.Supplies.view(groupname, item.id) );
   }

   public static Result addSupplyItem(String groupname, Long id) {
      Supply supply = BaseRepo.findById(Supply.class, id);
      if(supply == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("item").toString().isEmpty() || form.get("amount").toString().isEmpty())
         return badRequest();

      Long itemId = Long.valueOf(form.get("item").toString());
      Item item = BaseRepo.findById(Item.class, itemId);
      double amount = Double.valueOf(form.get("amount").toString());
      if(item == null)
         return badRequest();

      SupplyItem supplyItem = BaseRepo.find(SupplyItem.class).where().eq("item", itemId).eq("supply", id).findUnique();
      if(supplyItem != null)
         return badRequest();

      supplyItem = new SupplyItem();
      supplyItem.item = itemId;
      supplyItem.amount = amount;// * item.transformRatio;
      supplyItem.date = supply.date;
      supplyItem.supply = id;
      supplyItem.save();

      ObjectNode n = Json.newObject();
      n.put("title", item.title());
      n.put("id", supplyItem.id);
      n.put("amount", amount);
      n.put("measureType", item.measureType().title());

      return ok(n);
   }

   public static Result deleteSupplyItem(String groupname, Long id) {
      SupplyItem item = BaseRepo.findById(SupplyItem.class, id);
      if(item != null)
         item.delete();
      return ok();
   }

   public static Result editSupplyItemAmount(String groupname, Long supplyId, Long supplyItemId) {
      DynamicForm form = Form.form().bindFromRequest();

      SupplyItem item = BaseRepo.findById(SupplyItem.class, supplyItemId);

      if(form.get("value").toString().isEmpty() || item == null || item.supply != supplyId)
         return badRequest();

      item.amount = Double.valueOf( form.get("value").toString() );// * item.item().transformRatio;
      item.update();

      return ok();
   }

   public static Result editSupplyTitle(String groupname, Long supplyId) {
      DynamicForm form = Form.form().bindFromRequest();

      Supply item = BaseRepo.findById(Supply.class, supplyId);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      item.title = form.get("value").toString();
      item.update();

      return ok(item.title);
   }

   public static Result editSupplyDate(String groupname, Long supplyId) {
      Order ord = BaseRepo.find(Order.class).where().eq("supply", supplyId).findUnique();
      if(ord != null)
         return badRequest();

      Supply supply = BaseRepo.findById(Supply.class, supplyId);
      if(supply == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();
      if(form == null || form.get("value") == null || form.get("value").toString().isEmpty())
         return badRequest();

      String dateString = form.get("value").toString();

      DateFormat df = new SimpleDateFormat("dd.MM.yyyy"); 
      Date date = null;
      
      try {
        date = df.parse(dateString);
      } catch (ParseException e) {
         e.printStackTrace();
         return badRequest();
      }

      DateTime supplyDate = new DateTime(date).withTimeAtStartOfDay();
      DateTime now = DateTime.now().withTimeAtStartOfDay();
      if(supplyDate.getMillis() < now.getMillis())
         return badRequest();

      supply.date = Calendar.getInstance();
      supply.date.setTimeInMillis(supplyDate.getMillis());

      supply.update();

      return ok(String.valueOf(supplyDate.getMillis()));
   }

   public static Result finalAmount(String groupname, String time) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Date date = null;
      try {
         date = dateFormat.parse(time);
      } catch(Exception e) {
         e.printStackTrace();
      }

      if(form.get("measureType").toString().isEmpty() || form.get("item").toString().isEmpty() || form.get("sum").toString().isEmpty() || form.get("amount").toString().isEmpty() || date == null)
         return badRequest();

      Long itemId = Long.valueOf(form.get("item").toString());
      Long measureType = Long.valueOf(form.get("measureType").toString());

      ItemMeasureType mt = BaseRepo.findById(ItemMeasureType.class, measureType);
      if(mt == null)
         return badRequest();

      Double finalAmount = Double.valueOf(form.get("amount").toString());
      Double price = Double.valueOf(form.get("price").toString());
      Integer sum = Integer.parseInt(form.get("sum").toString());

      if(finalAmount < 0 || price < 0)
         return badRequest();

      SupplyItemResult item = new SupplyItemResult();
      item.item = itemId;
      item.date = Calendar.getInstance();
      item.date.setTime(date);
      item.sum = sum;
      item.finalAmount = finalAmount * mt.transformRatio;
      item.singlePrice = sum / Double.valueOf(item.finalAmount).intValue();
      item.save();

      try {
         String tostock = form.get("tostock").toString();
         play.Logger.debug(">>>>>>> " + tostock);

         if(tostock != null && tostock.equals("on")) {
            play.Logger.debug(">>>>>>> " + item.finalAmount);
            Stock.addToStock(itemId, item.finalAmount);
            block(itemId, date);
         }

      } catch(NullPointerException e) {

      }

      SupplyItemTemp itemc = itemClick(itemId, time);

      ObjectNode jsonNode = Json.newObject();
      jsonNode.put("result", "success");
      jsonNode.put("id", itemId);
      jsonNode.put("tpl", itemInfo.render(itemc, date).toString());

      return ok(jsonNode);
   }

   public static Result amount(String groupname, String time) {
      List<Supply> list = null;
      try {
         list = BaseRepo.find(Supply.class).where().eq("date", dateFormat.parse(time)).findList();
      } catch (Exception e) {
         e.printStackTrace();
      }

      if(list == null)
         return ok("false");
      return ok(list.size() > 0 ? "true" : "false");
   }

   // public static List<SupplyItem> findListByDate(String time) {
   //    try {
   //       List<SupplyItem> list = BaseRepo.find(SupplyItem.class).where().eq("date", dateFormat.parse(time)).orderBy("item").findList();
   //       return list;
   //    } catch(Exception e) {
   //       e.printStackTrace();
   //    }

   //    return null;
   // }

   public static Result block(String groupname) {
      Form<XEditForm> form = new Form(XEditForm.class).bindFromRequest();
      play.Logger.debug(form.toString());

      XEditForm s = form.get();

      ObjectNode jsonNode = Json.newObject();
      jsonNode.put("result", "success");
      ArrayNode arr = jsonNode.putArray("arr");
      
      for(Long id : s.arr) {
         ObjectNode n = block(id, s.date);
         if(n == null)
            continue;
         arr.add(n);
      }

      return ok(jsonNode);
   }

   private static ObjectNode block(Long id, Date date) {
      play.Logger.debug(">>>> id " + id + " " + date);

      Item i = BaseRepo.findById(Item.class, id);
      List<Object> supps = BaseRepo.find(Supply.class).where().eq("date", date).findIds();

      for(Object o : supps) {
         play.Logger.debug(">>>>> suupsss " + supps.size() + " " + o);            
      }

      List<SupplyItem> items = BaseRepo.find(SupplyItem.class).where().eq("item", id).in("supply", supps).findList();

      if(items == null || i == null)
         return null;

      play.Logger.debug(">>>>> items " + items.size());

      double finalLocked = 0, overall = 0, toBlock = 0, 
         availableAmount = StockItem.availableAmountOf(id);

      if(availableAmount == 0)
         return null;

      for(SupplyItem item : items) {
         play.Logger.debug(">>>>>>>> availableAmount " + availableAmount + " " + item.amount);
         if(availableAmount == 0)
            break;

         double amount = item.amount-item.locked;

         if(availableAmount >= amount) {
            toBlock = amount;
            availableAmount -= toBlock;
         } else if(availableAmount < amount) {
            toBlock = availableAmount;
            availableAmount = 0;
         }

         if(item.locked > 0)
            item.locked += toBlock;
         else
            item.locked = toBlock;

         finalLocked += toBlock;
         overall += amount;
         item.update();
      }

      play.Logger.debug(">>>>>>>>> finalLocked " + finalLocked);

      if(finalLocked > 0)
         StockItem.lock(id, finalLocked);

      ObjectNode n = Json.newObject();
      n.put("id", id);
      n.put("locked", Items.transform(StockItem.lockedOf(id), i.measureType().transformRatio));
      n.put("available", Items.transform(StockItem.availableAmountOf(id), i.measureType().transformRatio));
      n.put("overall", Items.transform(overall-finalLocked, i.measureType().transformRatio));
      return n;
   } 

   public static Result unblock(String groupname) {
      Form<XEditForm> form = new Form(XEditForm.class).bindFromRequest();
      play.Logger.debug(form.toString());

      XEditForm s = form.get();

      ObjectNode jsonNode = Json.newObject();
      jsonNode.put("result", "success");
      ArrayNode arr = jsonNode.putArray("arr");
      
      for(Long id : s.arr) {
         Item i = BaseRepo.findById(Item.class, id);
         List<SupplyItem> items = BaseRepo.find(SupplyItem.class).where().eq("item", id).eq("date", s.date).findList();

         if(items == null || i == null)
            continue;

         play.Logger.debug(">>>>>> " + i.title.ru);

         double finalLocked = 0, overall = 0;
         for(SupplyItem item : items) {
            play.Logger.debug(">>>>>> " + item.locked + " " + item.amount);
            finalLocked += item.locked;
            item.locked = 0;
            item.update();

            overall += item.amount - item.locked;
         }

         play.Logger.debug(">>>>>> " + finalLocked);

         if(finalLocked > 0)
            StockItem.unlock(id, finalLocked);
         
         ObjectNode n = Json.newObject();
         n.put("id", id);
         n.put("locked", Items.transform(StockItem.lockedOf(id), i.measureType().transformRatio));
         n.put("available", Items.transform(StockItem.availableAmountOf(id), i.measureType().transformRatio));
         n.put("overall", Items.transform(overall, i.measureType().transformRatio));
         arr.add(n);
      }

      play.Logger.debug(">>>>>>> " + jsonNode.toString());

      return ok(jsonNode);
   }

   public static Double compareAmount(SupplyItem item, List<SupplyItemTemp> temps) {
      for(SupplyItemTemp temp : temps) {
         if(temp.item == item.item) {

            double amount = Double.valueOf(temp.amount);
            play.Logger.debug(">>>>>>> " + temp.title + " (" + temp.item + ") " + amount + " " + item.amount);
            if(amount != item.amount)
               return amount;
            
            if(amount == item.amount)
               return 0.0;
         }
      }

      return null;
   }

   public static List<BanketSupply> findBanketSupplies(Long banket) {
      return Ebean.find(BanketSupply.class).where().eq("banket", banket).findList();
   }

   public static List<SupplyItem> findSupplyItems(Long supply) {
      return Ebean.find(SupplyItem.class).where().eq("supply", supply).findList();  
   }

   public static List<SupplyItemTemp> findList(Long orderId) {
      StringBuffer findListQueryBuffer = new StringBuffer()
                  .append("select i.id_ as item, i.ru as title, sum(ford.overall_*\ncase\nwhen\nford.to_supply_\n=\ntrue\nthen\nfing.amount_\nelse\n0\nend) as amount, ")
                  .append("si.amount_ as approvedAmount, ")
                  .append("si.id_ as supplyItem ")
                  .append("from res_item i ")
                  .append("join res_food_ingredient fing on i.id_ = fing.ingredient_ ")
                  .append("join res_order_food ford on ford.food_ = fing.food_ ")
                  .append("join res_order ord on ord.id_ = ford.order_ and ord.id_ = ")
                  .append(orderId).append(" ")
                  .append("left outer join res_supply_item si on si.item_ = i.id_ and si.supply_ = ord.supply_ ")
                  .append("where i.disabled_ = false ")
                  .append("group by title, item, approvedAmount, supplyItem ")
                  .append("order by item");

      String sql = findListQueryBuffer.toString();
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      Query<SupplyItemTemp> query = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql);

      return query.findList();
   }

   public static List<SupplyItemTemp> findListForFood(Long foodId, int portions) {
      StringBuffer findListQueryBuffer = new StringBuffer()
         .append("select i.id_ as item, i.ru as title, ")
         .append(portions).append("*fing.amount_/1000 as amount ")
         .append("from res_item i ")
         .append("join res_food_ingredient fing on i.id_ = fing.ingredient_ and fing.food_ = ").append(foodId)
         .append(" group by title, item, amount ")
         .append("order by item");

      String sql = findListQueryBuffer.toString();
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      Query<SupplyItemTemp> query = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql);

      return query.findList();
   }

   public static List<SupplyItemTemp> findListForIngredient(Long ingredientId, String date) {
      StringBuffer findListQueryBuffer = new StringBuffer()
         .append("select f.ru title, sum(fing.amount_*ford.overall_/mt.transform_ratio_) amount, mt.ru description from res_order_food ford ")
         .append("join res_food f on f.id_ = ford.food_ ")
         .append("join res_food_ingredient fing on fing.food_ = f.id_ ")
         .append("join res_item_measure_type mt on mt.id_ = fing.measure_type_ ")
         .append("join res_item i on fing.ingredient_ = i.id_ AND i.id_=").append(ingredientId)
         .append(" join res_order ord on ford.order_=ord.id_ ")
         .append("join res_supply s on s.id_ = ord.supply_ and s.date_ = '").append(date)
         .append("' group by title, description order by title");

      String sql = findListQueryBuffer.toString();
      RawSql rawSql = RawSqlBuilder.parse(sql).create();
      Query<SupplyItemTemp> query = Ebean.find(SupplyItemTemp.class).setRawSql(rawSql);

      return query.findList();
   }

   public static List<SupplyTemp> findSupplies() {
      String sql = "select date_ as date, count(id_) as count from res_supply where disabled_ = false group by date order by date desc";
 
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      Query<SupplyTemp> query = Ebean.find(SupplyTemp.class).setRawSql(rawSql);

      return query.findList();
   }

   public static List<SupplyItemResult> getSupplyResults(Long itemId, Date date) {
      List<SupplyItemResult> list = BaseRepo.find(SupplyItemResult.class).where().eq("item", itemId).eq("date", date).findList();
      // if(list == null)
      //    return new SupplyItemResult();

      return list;
   }

   public static int getSupplyResultSum(Date date) {
      int result = 0;

      List<SupplyItemResult> list = BaseRepo.find(SupplyItemResult.class).where().eq("date", date).findList();

      for(SupplyItemResult item : list) {
         result += item.sum;
      }

      return result;
   }

   public static class XEditForm {
      public List<Long> arr;
      public Long pk;
      @Formats.DateTime(pattern="yyyy/MM/dd")
      public Date date;
      public Map<String, Double> value;
   }
}

package restoran.controllers;

import core.repos.BaseRepo;
import restoran.models.*;

public class OrderRules {
	public static int getPlaces(Order order, Long foodType, Long locationTableItem) {
		OrderTemplatePlace otp = BaseRepo.find(OrderTemplatePlace.class).where().eq("template", order.template).eq("foodType", foodType)
			.eq("locationTableItem", locationTableItem).findUnique();

		if(otp == null)
			return 1;

		return otp.places;
	}

	public static int getPlaces(Long template, Long foodType, Long locationTableItem) {
		OrderTemplatePlace otp = BaseRepo.find(OrderTemplatePlace.class).where().eq("template", template).eq("foodType", foodType)
			.eq("locationTableItem", locationTableItem).findUnique();

		if(otp == null)
			return 1;

		return otp.places;
	}
}
package restoran.controllers;

import play.mvc.Controller;
import play.mvc.Result;
import restoran.views.html.admin.index;
import restoran.models.Banket;

import com.avaje.ebean.Page;

import core.repos.BaseRepo;

import play.mvc.With;

@With(SessionController.class)
public class Admin extends Controller {
	public static Result index(String group, int page, int pageSize) {
		Page<Banket> items = BaseRepo.findPage(Banket.class, page - 1, pageSize);
		return ok(index.render(items, page, pageSize));
	}
}

package restoran.controllers;

import akka.actor.*;
import akka.dispatch.*;
import akka.util.*;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.facet.*;
import org.elasticsearch.search.facet.terms.*;
import org.elasticsearch.search.facet.terms.longs.*;
import org.elasticsearch.search.facet.terms.strings.*;
import org.elasticsearch.search.sort.SortOrder;

import org.joda.time.DateTime;

import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Akka;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.Routes;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.stock.*;

import static java.util.concurrent.TimeUnit.*;
import scala.concurrent.duration.Duration;

@With(SessionController.class)
public class Stock extends Controller {

   public static final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");

   public static Call INDEX(String groupname, String itemType) {
      return restoran.controllers.routes.Stock.index(groupname, itemType, 1, 20);
   }

   public static Call UPDATE(String groupname, String itemType) {
      return restoran.controllers.routes.Stock.update(groupname, itemType);
   }

   public static void startUnlocker() {
      play.Logger.debug("Starting Stock items unlocker");

      Akka.system().scheduler().schedule(
         Duration.create(2, SECONDS),
         Duration.create(12, HOURS),     //Frequency 12 hours
         new Runnable() {
            public void run() {
               List<Object> bankets = BaseRepo.find(Banket.class).where().lt("date", new Date()).findIds();
               for(Object id : bankets) {
                  Order order = BaseRepo.find(Order.class).where().eq("banket", id).findUnique();
                  if(order == null) {
                     play.Logger.debug(">>>>>>> order null " + id);
                     continue;
                  }

                  List<SupplyItem> sitem = BaseRepo.find(SupplyItem.class).where().eq("supply", order.supply).gt("locked", 0).findList();

                  for(SupplyItem s : sitem) {
                     if(s.locked > 0) {
                        play.Logger.debug("[Stock Items Unlocker] Unlocking item " + s.item().title.ru + " " + s.locked);
                        StockItem.minusLock(s.item, s.locked);
                     }
                  }
               }
            }
         },
         Akka.system().dispatcher()
      );

   }

   public static Result index(String groupname, String itemType, int page, int pageSize) {
      return ok(index.render(groupname, itemType, page-1, pageSize));
   }

   public static Result timeline(String groupname, String itemType, int page, int pageSize) {
      QueryBuilder query = QueryBuilders.matchAllQuery();

      TermFilterBuilder subject = FilterBuilders.termFilter("subject.objectType", StockItemTakeOut.objectType);     
      query = QueryBuilders.filteredQuery(query, subject);

      IndexQuery<Activity> indexQuery = Elastic.query(Activity.class);
      indexQuery.setBuilder(query);
      indexQuery.from((page - 1) * pageSize);
      indexQuery.size(pageSize);
      indexQuery.addSort("date", SortOrder.DESC);

      IndexResults<Activity> result = Elastic.search(Activity.class, indexQuery);
      
      return ok(timeline.render(groupname, result.results, page, pageSize));
   }

   public static Result itemClick(String groupname, Long id) {
      Item item = BaseRepo.findById(Item.class, id);
      if(id == null)
         return badRequest();

      // StockItem stock = BaseRepo.find(StockItem.class).where().eq("item", id).findUnique();

      return ok(itemInfo.render(item));
   }

   public static Result search(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q, String itemType) {
      IndexResults<Item> result = FoodsRepo.filterItems(q, itemType, false, displayStart, displaySize, sortCol, sortDirection);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(Item f : result.results) {
         StockItem stock = BaseRepo.find(StockItem.class).where().eq("item", f.id).findUnique();

         ObjectNode o = Json.newObject();
         o.put("DT_RowId", f.id);
         o.put("0", f.title());

         double stockAmount = 0, stockLock = 0;
         ItemMeasureType measureType = f.measureType();
         double mratio = measureType == null ? 1 : measureType.transformRatio;
         String measureTitle = measureType == null ? "" : measureType.title();
         
         stockAmount = stock == null ? 0 : stock.amount / mratio;
         stockLock = stock == null ? 0 : stock.locked / mratio;

         StringBuffer stockAmountString = new StringBuffer()
            .append("<span id='amount").append(f.id).append("'>")
            .append(stockAmount).append("</span> ").append(measureTitle);

         StringBuffer stockLockString = new StringBuffer()
            .append("<span id='lock").append(f.id).append("'>")
            .append(stockLock).append("</span>");

         o.put("1", stockAmountString.toString());
         o.put("2", stockLockString.toString());
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

   public static Result update(String groupname, String itemType) {
      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("pk").toString().isEmpty() || form.get("value").toString().isEmpty())
         return badRequest();

      Long id = Long.valueOf(form.get("pk").toString());
      Double amount = Double.valueOf(form.get("value").toString());
      double transformRatio = 1;

      Item ingr = BaseRepo.findById(Item.class, id);
      if(ingr == null)
         return badRequest();

      if(Item.ItemType.valueOf(itemType.toUpperCase()) == Item.ItemType.INGREDIENT) {
         if(form.get("measureType").toString().isEmpty())
            return badRequest();

         Long measureType = Long.valueOf(form.get("measureType").toString());

         ItemMeasureType mt = BaseRepo.findById(ItemMeasureType.class, measureType);
         if(mt == null)
            return badRequest();

         transformRatio = mt.transformRatio;
      }

      StockItem stock = setToStock(id, amount * transformRatio, false);

      ObjectNode n = Json.newObject();
      n.put("id", id);
      n.put("amount", stock.amount);
      n.put("locked", stock.locked);
      n.put("tpl", itemInfo.render(ingr).toString());

      return ok(n);
   }

   public static StockItem addToStock(Long id, Double amount) {
      return setToStock(id, amount, true);
   }

   public static StockItem setToStock(Long id, Double amount, boolean add) {
      StockItem stock = BaseRepo.find(StockItem.class).where().eq("item", id).findUnique();
      if(stock == null) {
         stock = new StockItem();
         stock.item = id;
         stock.amount = amount;
         stock.save();
      } else {
         if(add)
            stock.amount += amount;
         else
            stock.amount = amount;
         stock.update();
      }

      StockItemTakeOut takeOut = new StockItemTakeOut();
      takeOut.item = id;
      takeOut.amount = amount;
      takeOut.save();

      return stock;
   }

   public static Result takeOuts(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String search, Long id, String item) {
      IndexResults<StockItemTakeOut> result = null;
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      try {
         result = RestoranRepo.filterStockItemTakeOuts(id, displayStart, displaySize, sortCol, sortDirection);
      } catch(Exception e) {
         e.printStackTrace();
         n.put("iTotalRecords", 0);
         n.put("iTotalDisplayRecords", 0);
         return ok(n);
      }

      for(StockItemTakeOut f : result.results) {
         ObjectNode o = Json.newObject();
         o.put("DT_RowId", f.id);
         o.put("0", formatter.format(f.dateCreated));
         o.put("1", f.amount);
         o.put("2", f.creator().toString());
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }

   public static class XEditForm {
      public Long pk;

      public Map<String, Double> value;
   }

}

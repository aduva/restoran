package restoran.controllers;

import restoran.repos.RestoranRepo;
import core.repos.BaseRepo;
import core.models.security.User;
import core.models.security.Group;

import org.codehaus.jackson.node.*;
import com.github.aduva.elasticsearch.*;

import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.i18n.Messages;

import java.util.*;
import java.text.*;
import restoran.models.*;

import restoran.views.html.bankets.foods.*;

import org.joda.time.DateTime;
import com.avaje.ebean.*;
import javax.persistence.Entity;
import com.avaje.ebean.annotation.Sql;
import play.mvc.With;

@With(SessionController.class)
public class Orders extends Controller {

   public static final DecimalFormat df = new DecimalFormat("#.#");

   public static Result view(String groupname, Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      Order order = RestoranRepo.findBanketOrder(banketId);

      if(order == null) {
         return ok(restoran.views.html.bankets.order.render(groupname, item, order));
      }

      return ok(restoran.views.html.bankets.order.render(groupname, item, order));
   }

   private static void calcTables(Banket item, Order order) {

      int peopleAmount = item.peopleAmount;
      if(item.banketType().code == BanketType.Code.WEDDING) {
         addWeddingTable(order, item.location);
         peopleAmount -= 4;
      }

      countTables(item, order, peopleAmount);
   }

   public static Result updateBanketDisabled(String groupname, Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      boolean disabled = !item.isDisabled;
      item.isDisabled = disabled;
      item.update();

      Order order = RestoranRepo.findBanketOrder(banketId);
      if(order == null)
         return redirect( Bankets.VIEW(groupname, banketId) );

      Supply s = order.supply();
      if(s != null) {
         play.Logger.debug(">>>>>> setting disabled supply " + disabled);
         s.isDisabled = disabled;
         s.update();
      }

      BanketInvoice bi = BaseRepo.find(BanketInvoice.class).where().eq("banket", banketId).findUnique();
      if(bi != null && bi.invoice != null) {
         Invoice invoice = bi.invoice();
         if(invoice != null) {
            play.Logger.debug(">>>>>> setting disabled invoice " + disabled);
            invoice.isDisabled = disabled;
            invoice.update();

            List<InvoiceItem> invoices = BaseRepo.find(InvoiceItem.class).where().eq("invoice", invoice.id).findList();
            for(InvoiceItem i : invoices) {
               i.isDisabled = disabled;
               i.update();
            }
         }
      }

      
      return redirect( Bankets.VIEW(groupname, banketId) );
   }

   public static Result start(String groupname, Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("template").toString().isEmpty())
         return redirect( Bankets.VIEW(groupname, banketId) );

      Long templateId = Long.valueOf(form.get("template"));

      try {
         startOrder(item, templateId);
      } catch(Exception e) {
         return redirect( Bankets.VIEW(groupname, banketId) );
      }

      return redirect( restoran.controllers.routes.Orders.view(groupname, banketId) );
   }

   private static void startOrder(Banket item, Long templateId) throws Exception {
      OrderTemplate template = BaseRepo.findById(OrderTemplate.class, templateId);
      if(template == null)
         throw new Exception("no template");

      Order order = Order.create(item, template);
      calcTables(item, order);
   }

   public static List<Order> findBySupplyDate(Date date) {
      return BaseRepo.find(Order.class).where().eq("supplyDate", date).findList();
   }

   public static int overallPersonSum(Long orderId) {
      Order order = BaseRepo.findById(Order.class, orderId);
      if(order == null)
         return 0;

      int foodPayments = overallFoodPayments(orderId);
      int percents = order.servicePercents;

      if(foodPayments == 0)
         return 0;

      int res = (foodPayments * percents / 100 + foodPayments) / order.peopleAmount;

      return res;
   }

   public static int overallPayments(Long orderId) {
      Order order = BaseRepo.findById(Order.class, orderId);
      if(order == null)
         return 0;

      int foodPayments = overallFoodPayments(orderId);
      int percents = order.servicePercents;

      if(foodPayments == 0)
         return 0;

      int res = (foodPayments * percents / 100) + foodPayments;

      return res;
   }

   public static Result overallPersonSumGet(String groupname, Long orderId) {
      Order order = BaseRepo.findById(Order.class, orderId);
      ObjectNode o = Json.newObject();

      o.put("overallPayments", 0);
      o.put("personCost", 0);

      if(order == null) {
         return ok(o);
      }

      int foodPayments = overallFoodPayments(orderId);
      int percents = order.servicePercents;

      if(foodPayments == 0) {
         return ok(o);
      }

      int personCost = (foodPayments * percents / 100 + foodPayments) / order.peopleAmount;

      o.put("overallPayments", personCost + foodPayments);
      o.put("personCost", personCost);

      return ok(o);
   }



   public static int overallFoodPayments(Long orderId) {
      String sql = "select sum(fo.price_*fo.overall_) as overall from res_food f join res_order_food fo on f.id_ = fo.food_ and fo.disabled_ = false and fo.to_supply_=true and fo.order_=" + orderId;
      RawSql rawSql = RawSqlBuilder.parse(sql).create();

      SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
      SqlRow row = sqlQuery.findUnique();
      try {
         return row.getInteger("overall");
      } catch(Exception e) {
      }
      return 0;
   }



   public static Result payments(String groupname, Long banketId, int page, int pageSize) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      return ok(restoran.views.html.bankets.payments.income.render(groupname, item, page-1, pageSize));
   }

   public static Result blanks(String groupname, Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      Order order = RestoranRepo.findBanketOrder(banketId);
      if(order == null)
         return redirect( Bankets.VIEW(groupname, banketId) );

      // OrderBlanksWriter.write(order.id);

      return ok(restoran.views.html.bankets.blanks.index.render(groupname, item, order));  
   }

   public static Result searchInvoices(String groupname, int displayStart, int displaySize, int sortCol, String sortDirection, String q, Long banketId, String invoiceType) {
      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      n.put("iTotalRecords", 0);
      n.put("iTotalDisplayRecords", 0);

      BanketInvoice banketInvoice = RestoranRepo.findBanketInvoice(banketId);
      if(banketInvoice == null) {
         return ok(n);
      }

      try {
         // List<InvoiceItem> result = RestoranRepo.filterBanketInvoices(q, banketInvoice.invoice, invoiceType, displayStart, displaySize, sortCol, sortDirection);
         List<InvoiceItem> result = BaseRepo.find(InvoiceItem.class)
            .where().eq("invoiceType", invoiceType.toUpperCase()).eq("invoice", banketInvoice.invoice)
            .setFirstRow(displayStart).setMaxRows(displaySize).order("id desc").findList();

         int total = BaseRepo.find(InvoiceItem.class)
            .where().eq("invoiceType", invoiceType.toUpperCase()).eq("invoice", banketInvoice.invoice).findRowCount();

         for(InvoiceItem f : result) {
            String inv = new StringBuffer().append("invoices.invoiceType.").append(f.invoiceType.toString()).toString();
            ObjectNode o = Json.newObject();
            o.put("DT_RowId", f.id);
            o.put("0", f.id);
            o.put("1", f.title);
            o.put("2", new StringBuffer().append(f.amount).append(" (").append(Messages.get(inv)).append(")").toString());
            o.put("3", f.creator().toString());
            o.put("4", "<span class='date'>" + (f.dateLastEdited != null ? f.dateLastEdited.getTime() : f.dateCreated.getTime()) + "</span>");
            arr.add(o);
         }

         n.put("iTotalRecords", result.size());
         n.put("iTotalDisplayRecords", total);
      } catch(Exception e) {
         e.printStackTrace();
      }

      return ok(n);
   }

   public static List<InvoiceItem> payments(Banket banket) {
      return RestoranRepo.findBanketInvoices(banket.id);
   }

   public static int paymentsOverall(List<InvoiceItem> payments, String type) {
      if(payments == null)
         return 0;

      int overall = 0;
      for(InvoiceItem invoice : payments) {
         if(type != null && invoice.invoiceType == InvoiceItem.InvoiceType.valueOf(type))
            overall += invoice.amount;
      }
      return overall;
   }

   public static int paymentsBalance(Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return 0;

      List<InvoiceItem> payments = payments(item);
      int invoices = paymentsOverall(payments, "BILL");
      int payment = paymentsOverall(payments, "INCOME");
      int balance = invoices - payment;

      return balance*-1;
   }

   public static Result addPayment(String groupname, Long banketId, String invoiceType) {
      ObjectNode n = Json.newObject();
      n.put("result", "error");

      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null) {
         n.put("message", "There's no Banket with such id " + banketId);
         return badRequest(n);
      }

      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(">>>> " + form.toString());

      if(form.get("amount").toString().isEmpty() || form.get("title").toString().isEmpty()) {
         n.put("message", "Amount or Title values are empty");
         return badRequest(n);
      }

      String basis = form.get("title").toString();
      Integer amount = Integer.parseInt(form.get("amount").toString());

      InvoiceItem invoiceItem = new InvoiceItem();
      BanketInvoice banketInvoice = RestoranRepo.findBanketInvoice(banketId);
      Invoice invoice = null;
      
      if(banketInvoice == null) {
         invoice = createBanketInvoice(item);
         invoiceItem.invoice = invoice.id;

         banketInvoice = new BanketInvoice();
         banketInvoice.banket = banketId;
         banketInvoice.invoice = invoice.id;
         banketInvoice.save();
      } else {
         invoiceItem.invoice = banketInvoice.invoice;
      }

      invoiceItem.title = basis;
      invoiceItem.amount = amount;
      invoiceItem.invoiceType = InvoiceItem.InvoiceType.valueOf(invoiceType.toUpperCase());
      invoiceItem.save();

      String inv = new StringBuffer().append("invoices.invoiceType.").append(invoiceType.toUpperCase()).toString();

      n.put("id", invoiceItem.id);
      n.put("title", invoiceItem.title);
      n.put("amount", new StringBuffer().append(invoiceItem.amount).append(" (").append(Messages.get(inv)).append(")").toString());
      n.put("creator", invoiceItem.creator().toString());
      n.put("dateCreated", "<span class='date'>" + (invoiceItem.dateLastEdited != null ? invoiceItem.dateLastEdited.getTime() : invoiceItem.dateCreated.getTime()) + "</span>");

      n.put("result", "success");

      return ok(n);
   }

   private static Invoice createBanketInvoice(Banket banket) {
      Invoice invoice = new Invoice();
      invoice.title = new StringBuffer().append(Messages.get("invoices.banket")).append(" ").append(banket.shortString()).toString();
      invoice.save();
      return invoice;
   }

   public static Result addFood(String groupname, Long orderId) {
      Order ord = BaseRepo.findById(Order.class, orderId);
      
      if(ord == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("food").toString().isEmpty())
         return badRequest();

      Long foodId = Long.valueOf(form.get("food").toString());
      Food food = BaseRepo.findById(Food.class, foodId);
      if(food == null)
         return badRequest();

      FoodOrder foodOrder = new FoodOrder();
      foodOrder.order = orderId;
      foodOrder.portions = Double.valueOf(form.get("portions"));
      foodOrder.food = foodId;
      foodOrder.foodType = food.foodType;
      foodOrder.price = food.price;
      foodOrder.id = Long.valueOf( (Integer) Ebean.nextId(FoodOrder.class));

      foodOrder = foodOrderByTables(foodOrder);
      foodOrder = createFoodInvoiceItem(foodOrder, food.title(), ord.banket());
      foodOrder.save();

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, orderId, true).toString());

      return ok(jsonNode);
   }

   private static FoodOrder createFoodInvoiceItem(FoodOrder foodOrder, String foodTitle, Banket banket) {
      BanketInvoice banketInvoice = BaseRepo.find(BanketInvoice.class).where().eq("banket", banket.id).findUnique();

      Invoice invoice = null;

      InvoiceItem invoiceItem = new InvoiceItem();
      invoiceItem.title = foodTitle;
      invoiceItem.invoiceType = InvoiceItem.InvoiceType.BILL;
      Double amount = foodOrder.overall * foodOrder.price;
      invoiceItem.amount = amount.intValue();

      if(banketInvoice == null) {
         invoice = createBanketInvoice(banket);

         invoiceItem.invoice = invoice.id;

         banketInvoice = new BanketInvoice();
         banketInvoice.banket = banket.id;
         banketInvoice.invoice = invoice.id;
         banketInvoice.save();
      } else {
         invoiceItem.invoice = banketInvoice.invoice;
      }

      invoiceItem.save();

      foodOrder.invoiceItem = invoiceItem.id;
      // foodOrder.update();

      return foodOrder;
   }

   private static FoodOrder foodOrderByTables(FoodOrder foodOrder) {
      List<OrderTableItem> otis = RestoranRepo.findOrderTables(foodOrder.order);

      for(OrderTableItem oti : otis) {
         FoodOrderTableItem foti = BaseRepo.find(FoodOrderTableItem.class).where()
            .eq("order", foodOrder.order).eq("orderTableItem", oti.id)
            .eq("foodOrder", foodOrder.id).findUnique();

         if(foti == null) {
            foti = new FoodOrderTableItem();
            foti.order = foodOrder.order;
            foti.orderTableItem = oti.id;
            foti.foodOrder = foodOrder.id;
            foti.portions = foodOrder.portions;
            
            foti.places = OrderRules.getPlaces(foodOrder.order(), foodOrder.foodType, oti.locationTableItem);

            if(foodOrder.foodType == 1 || foodOrder.foodType == 4)
               foti.portions = (double) oti.peopleAmount / (double) foti.places;

            foti.overall = foti.places * foti.portions * oti.quantity;
            foti.save();
         } else {
            foti.places = OrderRules.getPlaces(foodOrder.order(), foodOrder.foodType, oti.locationTableItem);

            if(foodOrder.foodType == 1 || foodOrder.foodType == 4)
               foti.portions = (double) oti.peopleAmount / (double) foti.places;

            foti.overall = foti.places * foti.portions * oti.quantity;
            foti.update();
         }
         

         foodOrder.overall += foti.overall;
      }

      // foodOrder.update();//git clone git@bitbucket.org:aduva/restoran.git
      return foodOrder;
   }

   public static int countFoodTypeOverallPortions(Order order, Long foodType) {
      List<OrderTableItem> otis = RestoranRepo.findOrderTables(order.id);

      int overall = 0;
      for(OrderTableItem oti : otis) {
         int places = OrderRules.getPlaces(order.template, foodType, oti.locationTableItem);

         overall += places * oti.quantity;
      }

      return overall;
   }

   public static List<SupplyItemTemp> fruits(Order order) {
      int portions = countFoodTypeOverallPortions(order, 5l);
      return Supplies.findListForFood(40l, portions);
   }

   public static Result foods(String groupname, Long banketId, boolean isWizard) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      Order order = RestoranRepo.findBanketOrder(banketId);

      if(order == null)
         return redirect( Bankets.VIEW(groupname, banketId) );

      return ok(index.render(groupname, item, order, isWizard));
   }

   public static Result supplies(String groupname, Long banketId) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect(Bankets.VIEW(groupname, banketId));

      Order order = RestoranRepo.findBanketOrder(banketId);

      if(order == null)
         return redirect( Bankets.VIEW(groupname, banketId) );

      List<SupplyItemTemp> list = Supplies.findList(order.id);

      return ok(supplies.render(groupname, item, list, order));
   }

   public static Result updateSupply(String groupname, Long orderId) {
      Order order = BaseRepo.findByIdNocache(Order.class, orderId);

      if(order == null)
         return badRequest("no order");

      List<SupplyItemTemp> list = Supplies.findList(order.id);

      for(SupplyItemTemp item : list) {
         if(item.amount == 0 && item.supplyItem != null) {
            item.supplyItem().delete();
         } else if(item.amount != 0 && item.supplyItem == null) {
            SupplyItem si = new SupplyItem();
            si.amount = item.amount;
            si.supply = order.supply;
            si.item = item.item;
            si.date = order.supplyDate;
            si.save();
         } else if(item.amount != 0 && item.supplyItem != null && item.amount != item.approvedAmount) {
            SupplyItem si = item.supplyItem();
            si.amount = item.amount;
            si.update();
         }

      }

      return redirect(restoran.controllers.routes.Orders.supplies(groupname, order.banket));
   }

   public static Result tables(String groupname, Long banketId, boolean isWizard) {
      Banket item = BaseRepo.findById(Banket.class, banketId);
      if(item == null)
         return redirect("/");

      Order order = RestoranRepo.findBanketOrder(banketId);

      if(order == null) {
         return redirect( Bankets.VIEW(groupname, banketId) );
      }
      
      int peopleAmount = item.peopleAmount;
      if(item.banketType().code == BanketType.Code.WEDDING) {
         addWeddingTable(order, item.location);
         peopleAmount -= 4;
      }

      countTables(item, order, peopleAmount);

      return ok(restoran.views.html.bankets.tables.index.render(groupname, item, order, isWizard));
   }

   public static void addWeddingTable(Order order, Long location) {
      TableItem tableItem = TableItem.weddingTable();
      LocationTableItem table = RestoranRepo.findLocationTable(location, tableItem.id);
      if(table == null) {
         play.Logger.debug(">>>>> table not null returning");
         return;
      }

      OrderTableItem item = BaseRepo.find(OrderTableItem.class).where().eq("order", order.id)
         .eq("tableItem", tableItem.id).eq("locationTableItem", table.id).findUnique();
      
      if(item != null) {
         play.Logger.debug(">>>>> item not null returning");
         return;
      }

      item = new OrderTableItem();
      item.locationTableItem = table.id;
      item.tableItem = tableItem.id;
      item.order = order.id;
      item.quantity = 1;
      item.peopleAmount = 4;
      item.save();
   }

   public static void countTables(Banket banket, Order order, int unplacedPeopleAmount) {
      int peopleAmount = unplacedPeopleAmount;
      if(peopleAmount == 0 || peopleAmount < 0) 
            return;

      List<LocationTableItem> locationTables = RestoranRepo.findTablesByLocation(banket.location);

      for(LocationTableItem table : locationTables) {
         int volume = table.tableItem().peopleAmount;
         if(table.joined)
            volume = volume * 2 - 2;
         int usedAmount = 0;

         while(usedAmount < table.quantity) {
            int neededAmount = peopleAmount / volume;

            if(peopleAmount < volume) {
               neededAmount = 1;
               volume = peopleAmount;
            }

            // OrderTableItem t = BaseRepo.find(OrderTableItem.class).where()
            //    .eq("locationTableItem", table.id).eq("order", order.id).eq("tableItem", table.tableItem).findUnique();

            OrderTableItem t = new OrderTableItem();
            t.locationTableItem = table.id;
            t.tableItem = table.tableItem;
            t.order = order.id;
            t.peopleAmount = volume;

            if(table.quantity < neededAmount)
               neededAmount = table.quantity;

            t.quantity = neededAmount;

            t.save();

            usedAmount += neededAmount;
            peopleAmount -= neededAmount * volume;

            if(peopleAmount == 0 || peopleAmount < 0)
               break;
         }

         if(peopleAmount == 0 || peopleAmount < 0) 
            break;
      }

      if(peopleAmount == 0 || peopleAmount < 0)
         order.unplacedPeopleAmount = peopleAmount;

      order.update();
   }
  
   public static Result editServicePercents(String groupname, Long orderId) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(">>>>>> " + form.toString());

      Order ord = BaseRepo.findById(Order.class, orderId);
      if(ord == null || form.get("value").toString().isEmpty())
         return badRequest();

      ord.servicePercents = Integer.valueOf(form.get("value").toString());
      ord.update();

      int foodPayments = overallFoodPayments(orderId);
      int percents = ord.servicePercents;

      int personCost = (foodPayments * percents / 100 + foodPayments) / ord.peopleAmount;

      ObjectNode o = Json.newObject();

      o.put("overallPayments", overallPayments(orderId));
      o.put("personCost", personCost);

      return ok(o);
   }

   public static int servicePercents(Long orderId) {
      Order ord = BaseRepo.findById(Order.class, orderId);
      if(ord == null)
         return 15;
      return ord.servicePercents;
   }
      
   public static Result editSupplyDate(String groupname, Long orderId) {
      Order ord = BaseRepo.findById(Order.class, orderId);
      if(ord == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("value").toString().isEmpty())
         return badRequest();

      String dateString = form.get("value").toString();

      DateFormat df = new SimpleDateFormat("dd.MM.yyyy"); 
      Date date;
      
      try {
        date = df.parse(dateString);
      } catch (ParseException e) {
         e.printStackTrace();
         return badRequest();
      }
      DateTime banketDate = new DateTime(ord.banket().date);
      DateTime supplyDate = new DateTime(date).withTimeAtStartOfDay();
      DateTime now = DateTime.now().withTimeAtStartOfDay();
      if(supplyDate.getMillis() < now.getMillis() || supplyDate.getMillis() > banketDate.getMillis())
         return badRequest();

      Ebean.refresh(ord);

      ord.supplyDate = Calendar.getInstance();
      ord.supplyDate.setTimeInMillis(supplyDate.getMillis());
      ord.update();

      Supply supply = ord.supply();
      if(supply != null) {
         supply.date = Calendar.getInstance();
         supply.date.setTimeInMillis(supplyDate.getMillis());
         supply.update();
      }

      List<SupplyItem> supps = BaseRepo.find(SupplyItem.class).where().eq("supply", ord.supply).findList();
      for(SupplyItem sup : supps) {
         sup.date = supply.date;
         sup.update();
      }

      return ok();
   }

   public static Result approveSupply(String groupname, Long orderId) {
      Order ord = BaseRepo.findByIdNocache(Order.class, orderId);

      if(ord == null)
         return badRequest("no order");

      if(ord.supply != null)
         return badRequest("approved");      

      Supply supply = new Supply();
      supply.title = Messages.get("supplies.list") + " - " + ord.displayName();
      supply.date = ord.supplyDate;
      supply.save();

      List<SupplyItemTemp> list = Supplies.findList(ord.id);
      for(SupplyItemTemp temp : list) {
         try {
            SupplyItem si = new SupplyItem();
            si.item = temp.item;
            si.date = ord.supplyDate;
            si.supply = supply.id;
            si.amount = temp.amount;
            si.save();
         } catch(Exception e) {
            e.printStackTrace();
         }
      }

      ord.approved = true;
      ord.supply = supply.id;
      ord.update();

      BanketSupply bs = new BanketSupply();
      bs.supply = supply.id;
      bs.banket = ord.banket;
      bs.save();

      return redirect(restoran.controllers.routes.Orders.supplies(groupname, ord.banket));
   }

   public static Result supplyFood(String groupname, Long foodOrderId, boolean toSupply) {
      FoodOrder f = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(f == null)
         return badRequest();

      if(f.toSupply != toSupply) {
         f.toSupply = toSupply;
         f.update();

         InvoiceItem i = f.invoiceItem();
         i.isDisabled = !toSupply;
         i.update();

      }

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, f.order, true).toString());

      return ok(jsonNode);
   }

	

   public static void recalc(Banket banket) {
      Order order = RestoranRepo.findBanketOrder(banket.id);
      if(order != null) {
         recalc(banket, order);
      }
   }

   public static void recalc(Banket banket, Order order) {
      RestoranRepo.deleteOrderTables(order.id);
      RestoranRepo.deleteFoodOrderTablesByOrder(order.id);

      Order.update(order, banket);
      play.Logger.debug(">>>>>>> " + order.unplacedPeopleAmount);
      calcTables(banket, order);
      updateFoodOrders(order.id);
   }

   public static Result addGarnish(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Long foodOrderId = Long.valueOf(form.get("pk").toString());

      FoodOrder parentOrder = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(parentOrder == null)
         return badRequest();

      Long foodId = Long.valueOf(form.get("value").toString());
      Food food = BaseRepo.findById(Food.class, foodId);
      if(food == null)
         return badRequest();

      FoodOrder foodOrder = new FoodOrder();
      foodOrder.order = parentOrder.order;
      foodOrder.portions = parentOrder.portions;
      foodOrder.price = food.price;
      foodOrder.overall = parentOrder.overall;
      foodOrder.parent = parentOrder.id;
      foodOrder.food = foodId;
      foodOrder.foodType = food.foodType;
      foodOrder.save();
      
      createFoodInvoiceItem(foodOrder, food.title(), foodOrder.order().banket());

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, foodOrder.order, true).toString());

      return ok(jsonNode);
   }

   public static Result deleteFood(String groupname, Long foodOrderId) {
      FoodOrder foodOrder = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(foodOrder == null) {
         return badRequest("no food order");
      }
      Order ord = BaseRepo.findById(Order.class, foodOrder.order);
      if(ord == null)
         return badRequest("no order");

      if(foodOrder.invoiceItem != null) {
         InvoiceItem it = foodOrder.invoiceItem();
         if(it != null) {
            it.delete();
         }
      }
      

      foodOrder.delete();
      RestoranRepo.deleteFoodOrdersByParent(foodOrderId);
      RestoranRepo.deleteFoodOrderTables(foodOrderId);

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, ord.id, true).toString());

      return ok(jsonNode);
   }

   public static Result changeFood(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Long foodOrderId = Long.valueOf(form.get("pk").toString());

      FoodOrder foodOrder = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(foodOrder == null)
         return badRequest();

      Long foodId = Long.valueOf(form.get("value").toString());
      foodOrder.food = foodId;
      foodOrder.update();

      updateFoodOrderInvoiceItem(foodOrder);

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, foodOrder.order, true).toString());

      return ok(jsonNode);
   }

   public static Result changeFoodOverallPortions(String groupname, Long orderId) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Long foodOrderId = Long.valueOf(form.get("pk").toString());

      FoodOrder foodOrder = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(foodOrder == null)
         return badRequest();
      Order ord = BaseRepo.findById(Order.class, foodOrder.order);
      if(ord == null)
         return badRequest();

      double overall = Double.valueOf(form.get("value").toString());
      double percents = 100 - (overall * 100) / foodOrder.overall;

      List<FoodOrderTableItem> fotis = RestoranRepo.findFoodOrderTableItems(orderId, foodOrderId);

      for(FoodOrderTableItem foti : fotis) {
         foti.portions -= foti.portions * percents / 100;
         foti.portions = Double.valueOf(df.format(foti.portions));
         foti.overall = foti.places * foti.portions * foti.orderTableItem().quantity;
         foti.overall = Double.valueOf(df.format(foti.overall));
         foti.update();
      }

      foodOrder.overall = overall;
      foodOrder.update();

      updateFoodOrderInvoiceItem(foodOrder);

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, orderId, true).toString());

      return ok(jsonNode);
   }

   public static Result changeFoodPlacesOnTable(String groupname, Long orderId) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Long foodOrderId = Long.valueOf(form.get("pk").toString());

      FoodOrderTableItem foodOrderTableItem = BaseRepo.findById(FoodOrderTableItem.class, foodOrderId);
      if(foodOrderTableItem == null)
         return badRequest();
      Order ord = BaseRepo.findById(Order.class, foodOrderTableItem.order);
      if(ord == null)
         return badRequest();

      int places = Integer.parseInt(form.get("value").toString());

      FoodOrder foodOrder = foodOrderTableItem.foodOrder();

      foodOrderTableItem.portions = foodOrder.portions;
      foodOrderTableItem.places = places;
      foodOrderTableItem.overall = places * foodOrderTableItem.portions * foodOrderTableItem.orderTableItem().quantity;
      foodOrderTableItem.overall = Double.valueOf(df.format(foodOrderTableItem.overall));
      foodOrderTableItem.update();

      List<FoodOrderTableItem> fotis = RestoranRepo.findFoodOrderTableItems(orderId, foodOrder.id);

      double overall = 0;

      for(FoodOrderTableItem foti : fotis) {
         overall += foti.overall;
      }

      foodOrder.overall = overall;
      foodOrder.update();

      updateFoodOrderInvoiceItem(foodOrder);

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, orderId, true).toString());

      return ok(jsonNode);
   }

   private static void updateFoodOrderInvoiceItem(FoodOrder foodOrder) {
      InvoiceItem invoiceItem = foodOrder.invoiceItem();
      Double amount = foodOrder.price * foodOrder.overall;
      if(invoiceItem.amount != amount.intValue()) {
         invoiceItem.amount = amount.intValue();
         invoiceItem.update();
      }
   }

   public static Result updateFoodOrderPrice(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      Long foodOrderId = Long.valueOf(form.get("pk").toString());

      FoodOrder foodOrder = BaseRepo.findById(FoodOrder.class, foodOrderId);
      if(foodOrder == null)
         return badRequest();
      Order ord = BaseRepo.findById(Order.class, foodOrder.order);
      if(ord == null)
         return badRequest();

      double price = Double.valueOf(form.get("value").toString());

      foodOrder.price = price;
      foodOrder.update();

      updateFoodOrderInvoiceItem(foodOrder);

      ObjectNode jsonNode = Json.newObject();

      jsonNode.put("result", "success");
      jsonNode.put("tpl", foodList.render(groupname, foodOrder.order, true).toString());

      return ok(jsonNode);
   }

   public static Result addTableItem(String groupname, Long orderId) {
      Order ord = BaseRepo.findById(Order.class, orderId);
      if(ord == null)
         return badRequest();

      DynamicForm form = Form.form().bindFromRequest();

      if(form.get("tableItem").toString().isEmpty())
         return badRequest();

      Long tableItemId = Long.valueOf(form.get("tableItem").toString());
      LocationTableItem table = BaseRepo.findById(LocationTableItem.class, tableItemId);
      if(table == null)
         return badRequest();

      
      int quantity = Integer.valueOf(form.get("quantity").toString());
      int peopleAmount = Integer.valueOf(form.get("peopleAmount").toString());

      int allowedPeopleAmount = table.tableItem().peopleAmount;
      if(table.joined)
         allowedPeopleAmount = allowedPeopleAmount * 2 - 2;

      List<OrderTableItem> otis = RestoranRepo.findOrderTablesByTableItem(orderId, table.tableItem);

      OrderTableItem item = null;
      int usedTablesAmount = 0;
      for(OrderTableItem oti : otis) {
         if(!table.joined && oti.joined())
            usedTablesAmount += oti.quantity * 2;
         else
            usedTablesAmount += oti.quantity;

         if(oti.peopleAmount == peopleAmount && oti.locationTableItem == tableItemId)
            item = oti;
      }

      ObjectNode jsonNode = Json.newObject();

      if(table.quantity < usedTablesAmount + quantity) {
         jsonNode.put("result", "error");
         jsonNode.put("msg", Messages.get("tables.error.no_more_tables_of_this_kind"));
         return ok(jsonNode);
      // } else if(allowedPeopleAmount +1 < peopleAmount) {
      //    jsonNode.put("result", "error");
      //    jsonNode.put("msg", Messages.get("tables.error.too_many_people_for_tabled"));
      //    return ok(jsonNode);
      }

      if(item == null) {
         item = new OrderTableItem();
         item.locationTableItem = tableItemId;
         item.tableItem = table.tableItem;
         item.order = orderId;
         item.quantity = quantity;
         item.peopleAmount = peopleAmount;
         item.save();
      } else {
         item.quantity += quantity;
         item.update();
      }

      ord.unplacedPeopleAmount -= quantity * peopleAmount;
      ord.update();

      updateFoodOrders(ord.id);

      jsonNode.put("result", "success");
      jsonNode.put("tpl", restoran.views.html.bankets.tables.tablesList.render(orderId, null, true).toString());
      jsonNode.put("unplacedPeopleAmount", ord.unplacedPeopleAmount);
      jsonNode.put("foodsTpl", foodList.render(groupname, orderId, true).toString());

      return ok(jsonNode);
   }

   public static Result recalcTables(String groupname, Long orderId) {

      Order order = BaseRepo.findById(Order.class, orderId);

      recalc(order.banket(), order);

      ObjectNode jsonNode = Json.newObject();
      jsonNode.put("result", "success");
      jsonNode.put("tpl", restoran.views.html.bankets.tables.tablesList.render(orderId, null, true).toString());
      jsonNode.put("foodsTpl", foodList.render(groupname, orderId, true).toString());
      jsonNode.put("unplacedPeopleAmount", order.unplacedPeopleAmount);

      return ok(jsonNode);
   }

   public static Result deleteTable(String groupname, Long orderTableId) {
      OrderTableItem item = BaseRepo.findById(OrderTableItem.class, orderTableId);
      if(item == null)
         return badRequest();

      Order ord = BaseRepo.findById(Order.class, item.order);
      if(ord == null)
         return badRequest();

      int unplacedPeopleAmount = ord.unplacedPeopleAmount + item.quantity * item.peopleAmount;

      if(unplacedPeopleAmount <= ord.peopleAmount) {
         ord.unplacedPeopleAmount = unplacedPeopleAmount;
         ord.update();
      }

      RestoranRepo.deleteFoodOrderTablesByOrderAndTableItem(ord.id, item.id);

      item.delete();
      updateFoodOrders(ord.id);

      ObjectNode jsonNode = Json.newObject();
      jsonNode.put("tpl", restoran.views.html.bankets.tables.tablesList.render(ord.id, null, true).toString());
      jsonNode.put("unplacedPeopleAmount", ord.unplacedPeopleAmount);
      jsonNode.put("foodsTpl", foodList.render(groupname, ord.id, true).toString());

      return ok(jsonNode);
   }

   public static void updateFoodOrders(Long order) {
      List<FoodOrder> foodOrders = RestoranRepo.findFoodOrders(order);
      for(FoodOrder foodOrder : foodOrders) {
         foodOrder.overall = 0;
         foodOrderByTables(foodOrder);
         updateFoodOrderInvoiceItem(foodOrder);
      }
   }

   public static List<FoodOrder> foodOrders(Long orderId) {
      return RestoranRepo.findFoodOrders(orderId);
   }

   public static List<FoodOrderTableItem> foodOrderByTables(Long orderId, Long tableItem) {
      return RestoranRepo.findFoodOrderByTables(orderId, tableItem);
   }

   public static int tablesQuantity(Long orderId) {
      List<OrderTableItem> tables = tablesList(orderId);
      int quantity = 0;
      if(tables == null)
         return quantity;

      for(OrderTableItem oti : tables)
         quantity += oti.quantity;
      return quantity;
   }

   public static List<OrderTableItem> tablesList(Long orderId) {
      return RestoranRepo.findOrderTables(orderId);
   }

   public static List<OrderTemplateItem> tmplItems(Long id) {
      return RestoranRepo.findTemplateItems(id);
   }
}

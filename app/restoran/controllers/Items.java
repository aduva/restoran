package restoran.controllers;

import com.github.aduva.elasticsearch.*;

import core.models.activity.*;
import core.models.multilingual.Title;
import core.models.security.Group;
import core.models.security.User;
import core.repos.BaseRepo;
import core.repos.UserRepo;
import core.utils.Page;

import java.text.*;
import java.util.*;

import org.apache.commons.lang3.text.WordUtils;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;

import restoran.models.*;
import restoran.repos.*;
import restoran.views.html.management.items.*;
import play.mvc.With;

@With(SessionController.class)
public class Items extends Controller {

   public static final DecimalFormat df = new DecimalFormat("#.##");

   public static Call VIEW(String groupname, String itemType, Long id) {
      return restoran.controllers.routes.Items.view(groupname, itemType, id);
   }

   public static Result index(String groupname, String itemType, int page, int pageSize) {
      return ok(index.render(groupname, itemType, page-1, pageSize));
   }

   public static Result view(String groupname, String itemType, Long id) {
      Item item = BaseRepo.findById(Item.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Items.index(groupname, itemType, 1, 10));

      List<Activity> activities = ActivityRepo.findAllByObject(item.activityObject().id);

      return ok(view.render(groupname, item, activities));
   }

   public static double transform(double amount, double ratio) {
      double res = amount / ratio;
      return Double.valueOf(df.format(res));
   }

   public static double transform(int amount, double ratio) {
      double res = (double) amount / ratio;
      return Double.valueOf(df.format(res));
   }

   public static Result stock(String groupname, String itemType, Long id) {
      Item item = BaseRepo.findById(Item.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Items.index(groupname, itemType, 1, 10));

      return ok(stock.render(groupname, item));
   }

   public static Result foods(String groupname, String itemType, Long id) {
      Item item = BaseRepo.findById(Item.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Items.index(groupname, itemType, 1, 10));

      IndexResults<FoodIngredient> list = null;

      if(itemType.equals(Item.ItemType.INGREDIENT.toString().toLowerCase())) {
         try {
            list = FoodsRepo.foodIngredientsByIngredient(id);
         } catch(Exception e) {
            return ok(foods.render(groupname, item, null));
         }
      }

      return ok(foods.render(groupname, item, list.results));
   }

   public static Result viewMeasureTypes(String groupname, String itemType, Long id) {
      Item item = BaseRepo.findById(Item.class, id);

      if(item == null)
         return redirect(restoran.controllers.routes.Items.index(groupname, itemType, 1, 10));

      List<ItemMeasureType> list = measureTypes(id);
      return ok(measureTypes.render(groupname, item, list));
   }

   public static Result updateDisabled(String groupname, String itemType, Long id) {
      Item item = BaseRepo.findById(Item.class, id);
      if(item == null)
         return redirect(restoran.controllers.routes.Items.index(groupname, itemType, 1, 10));

      boolean disabled = !item.isDisabled;

      List<FoodIngredient> ingrs = BaseRepo.find(FoodIngredient.class).where().eq("ingredient", id).findList();
      if(ingrs != null) {
         for(FoodIngredient ingr : ingrs) {
            ingr.isDisabled = disabled;
            ingr.update();
         }
      }

      List<SupplyItem> supps = BaseRepo.find(SupplyItem.class).where().eq("item", id).findList();
      if(supps != null) {
         for(SupplyItem supp : supps) {
            supp.isDisabled = disabled;
            supp.update();
         }
      }

      item.isDisabled = disabled;
      item.update();
      return redirect(VIEW(groupname, itemType, id));
   }

   public static Result deleteMeasureType(String groupname, Long id) {
      play.Logger.debug(">>>>> " + id);

      ItemMeasureType item = BaseRepo.findById(ItemMeasureType.class, id);
      if(item == null)
         return badRequest();

      item.delete();
      return ok();
   }

   public static Result addMeasureType(String groupname) {
      DynamicForm form = Form.form().bindFromRequest();

      play.Logger.debug(">>>>> " + form.toString());

      if(form.get("netto").toString().isEmpty() || form.get("title").toString().isEmpty() || form.get("pk").toString().isEmpty())
         return badRequest();

      Long id = Long.valueOf(form.get("pk").toString());

      Item item = BaseRepo.findById(Item.class, id);
      if(item == null)
         return badRequest();

      ItemMeasureType mt = new ItemMeasureType();
      mt.title = new Title();
      mt.title.ru = form.get("title").toString();
      mt.transformRatio = Double.valueOf(form.get("netto").toString());
      mt.item = id;
      mt.save();

      ObjectNode o = Json.newObject();
      o.put("id", mt.id);
      o.put("item", mt.item);
      o.put("transformRatio", mt.transformRatio);
      o.put("title", mt.title());
      o.put("result", "success");
      o.put("idx", 0);

      return ok(o);
   }

   public static List<ItemMeasureType> measureTypes(Long id) {
      return BaseRepo.find(ItemMeasureType.class).where().eq("item", id).findList();
   }

   public static List<ItemMeasureType> measureTypes() {
      return BaseRepo.find(ItemMeasureType.class).where().isNull("item").findList();
   }

   public static Result editMeasureTypeTitle(String groupname, String itemType, Long id) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(">>>>> " + form.toString());

      ItemMeasureType item = BaseRepo.findById(ItemMeasureType.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      String title = WordUtils.capitalizeFully(form.get("value").toString());

      item.title(title);
      item.update();

      return ok();
   }

   public static Result editMeasureTypeRatio(String groupname, String itemType, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      ItemMeasureType item = BaseRepo.findById(ItemMeasureType.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      Double ratio = Double.valueOf(form.get("value").toString());

      item.transformRatio = ratio;
      item.update();

      return ok();
   }

   public static Result editTitle(String groupname, String itemType, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Item item = BaseRepo.findById(Item.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      String title = WordUtils.capitalizeFully(form.get("value").toString());

      item.title(title);
      item.update();

      return ok();
   }

   public static Result editMeasureType(String groupname, String itemType, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Item item = BaseRepo.findById(Item.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      Long mt = Long.valueOf(form.get("value").toString());
      ItemMeasureType imt = BaseRepo.findById(ItemMeasureType.class, mt);
      if(imt == null)
         return badRequest();

      item.measureType = mt;
      item.update();

      return ok();  
   }

   public static Result editTags(String groupname, String itemType, Long id) {
      DynamicForm form = Form.form().bindFromRequest();

      Item item = BaseRepo.findById(Item.class, id);

      if(form.get("value").toString().isEmpty() || item == null)
         return badRequest();

      String tags = WordUtils.uncapitalize(form.get("value").toString());

      item.tags = tags;
      item.update();

      return ok();  
   }

   public static Result add(String groupname, String itemType) {
      DynamicForm form = Form.form().bindFromRequest();
      play.Logger.debug(form.toString());

      if(form.get("ru").toString().isEmpty() || form.get("measureType").toString().isEmpty())
         return badRequest();

      Long mt = Long.valueOf(form.get("measureType").toString());
      ItemMeasureType imt = BaseRepo.findById(ItemMeasureType.class, mt);
      if(imt == null)
         return badRequest();

      Item item = new Item();
      item.title = new Title();
      item.title.ru = WordUtils.capitalizeFully(form.get("ru").toString());
      item.tags = WordUtils.uncapitalize(form.get("tags").toString());
      item.itemType = Item.ItemType.valueOf(itemType.toUpperCase());
      item.measureType = mt;
      item.save();

      ObjectNode result = Json.newObject();
      result.put("result", "success");
      result.put("msg", "hello");

      StockItem stock = BaseRepo.find(StockItem.class).where().eq("item", item.id).findUnique();
      result.put("item", dataTableJson(item, stock, groupname));

      return ok(result);
   }

   public static Result items(String groupname, String itemType, String callback, String q, int pageSize, int page) {
      IndexResults<Item> results = FoodsRepo.filterItems(q, itemType, false, (page - 1) * pageSize, pageSize, 0, "asc");

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("results");

      for(Item f : results.results) {
         ObjectNode o = toJson(f);

         arr.add(o);
      }

      n.put("more", results.pageCurrent < results.pageNb);

      return ok(Jsonp.jsonp(callback, n));
   }

   private static ObjectNode toJson(Item f) {
      ObjectNode o = Json.newObject();
      o.put("id", f.id);
      o.put("title", f.title());
      o.put("tags", f.tags);
      if(f.measureType != null) {
         o.put("measureType.title", f.measureType().title());
         o.put("measureType", f.measureType);
      }

      ArrayNode options = o.putArray("options");
      List<ItemMeasureType> measureTypes = measureTypes();
      if(measureTypes != null) {
         for(ItemMeasureType measureType : measureTypes) {
            ObjectNode option = Json.newObject();
            option.put("title", measureType.title());
            option.put("id", measureType.id);
            option.put("transformRatio", measureType.transformRatio);
            options.add(option);
         }
      }

      List<ItemMeasureType> itemMeasureTypes = measureTypes(f.id);
      if(itemMeasureTypes != null) {
         for(ItemMeasureType measureType : itemMeasureTypes) {
            ObjectNode option = Json.newObject();
            option.put("title", measureType.title());
            option.put("id", measureType.id);
            option.put("transformRatio", measureType.transformRatio);
            options.add(option);
         }
      }

      return o;
   }

   private static ObjectNode dataTableJson(Item f, StockItem stock, String groupname) {
      ObjectNode o = Json.newObject();
      o.put("DT_RowId", f.id);
      o.put("0", "<a href=\"" + VIEW(groupname, f.itemType.toString().toLowerCase(), f.id) + "\">" + f.title() + "</a>");
      o.put("1", f.tags);
      o.put("2", f.measureType == null ? "" : f.measureType().title());
      o.put("3", stock == null ? 0 : stock.amount);
      return o;
   }

   public static Result search(String groupname, String itemType, int displayStart, int displaySize, int sortCol, String sortDirection, String q) {
   IndexResults<Item> result = FoodsRepo.filterItems(q, itemType, true, displayStart, displaySize, sortCol, sortDirection);

      ObjectNode n = Json.newObject();
      ArrayNode arr = n.putArray("aaData");

      for(Item f : result.results) {
         StockItem stock = BaseRepo.find(StockItem.class).where().eq("item", f.id).findUnique();
         ObjectNode o = dataTableJson(f, stock, groupname);
         arr.add(o);
      }

      n.put("iTotalRecords", result.totalCount);
      n.put("iTotalDisplayRecords", result.totalCount);

      return ok(n);
   }
}

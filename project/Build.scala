import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "restoran"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,

    "com.loicdescotte.coffeebean" % "html5tags_2.10" % "1.1.0",
    "jexcelapi" % "jxl" % "2.6"

  )

  val coreProject = RootProject(file("../core"))

  val main = play.Project(appName, appVersion, appDependencies).settings(
    organization := "kz.sazsyrnai",
    
    publishArtifact in(Compile, packageDoc) := false,
    sources in doc in Compile := List(),

    resolvers += Resolver.url("github repo for html5tags", url("http://loicdescotte.github.io/releases/"))(Resolver.ivyStylePatterns)
  ).dependsOn(coreProject)

}
